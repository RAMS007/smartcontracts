<?php

use Illuminate\Http\Request;
use Tymon\JWTAuth\JWTAuth;

Route::group(['prefix' => 'auth'], function () {

    Route::post('login', 'Auth\LoginController@loginMobile');
    Route::post('signup', 'Auth\RegisterController@registerMobile');

 //   Route::post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
//    Route::post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');


});
/*
Route::group(['middleware' => 'jwt.auth'], function () {

    Router::get('protected', function () {
        return response()->json([
            'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
        ]);
    });

});
*/

Route::get('refresh', function (Request $Request, JWTAuth $JWTAuth) {
    $input = $Request->all();
    $token = $input['Token'];

    if (!$token) {
        $Err['status'] = 'error';
        $Err['msg'] = 'There is no token';
        return response()
            ->json($Err, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    }

    try {
        $JWTAuth->setToken($token);
        $token = $JWTAuth->refresh();

    } catch (JWTException $e) {
        $ERR['status'] = 'error';
        $ERR['MSG'] = "the was erorr on you token ";
        return response()
            ->json($ERR, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    }catch(\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
        $Err['status'] = 'error';
        $Err['msg'] = 'Token expired';
        return response()
            ->json($Err, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

    }

    $Sucss['status'] = 'success';
    $Sucss['token'] = $token;
    return response()
        ->json($Sucss, 200, ['Content-type' => 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

});


