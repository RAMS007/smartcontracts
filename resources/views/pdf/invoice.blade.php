<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <style>
        #dyiPDF .inner {
            width: 90%;
            margin: 30px auto;
            padding: 20px 25px;
            background: #fff;
            box-shadow: -1px -1px 18px rgba(0, 0, 0, 0.1);
            overflow: hidden;
        }

        .row {
            margin-left: -10px;
            margin-right: -10px;
        }

        .col-xs-8 {
            width: 66%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .clearfix:before, .clearfix:after, .container:before, .container:after, .container-fluid:before, .container-fluid:after, .row:before, .row:after, .form-horizontal .form-group:before, .form-horizontal .form-group:after, .btn-toolbar:before, .btn-toolbar:after, .btn-group-vertical > .btn-group:before, .btn-group-vertical > .btn-group:after, .nav:before, .nav:after, .navbar:before, .navbar:after, .navbar-header:before, .navbar-header:after, .navbar-collapse:before, .navbar-collapse:after, .pager:before, .pager:after, .panel-body:before, .panel-body:after, .modal-header:before, .modal-header:after, .modal-footer:before, .modal-footer:after {
            content: " ";
            display: table;
        }

        #dyiPDF p {
            font-size: 15px;
        }

        .invoice-app p.lead {
            font-weight: bold;
            margin-bottom: 5px;
            padding: 0;
            background: none;
            text-align: initial;
        }

        .invoice-app .text-sample {
            color: #999;
            font-style: italic;
        }

        .qb-wrapper, .qb-wrapper p {
            line-height: 1.5;
        }

        .invoice-app .text-sample {
            color: #999;
            font-style: italic;
        }

        .qb-wrapper .col-xs-4 {
            width: 29%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #businessLogo {
            max-height: 150px;
            max-width: 150px;
        }

        .invoice-app .logo-holder {
            width: 100px;
            text-align: center;
            border-radius: 50px;
            height: 100px;
            padding-top: 34px;
            font-size: 13px;
            float: right;
        }
        .invoice-app .logo-holder2 {
            width: 150px;
            height: 150px;
            padding-top: 10px;
            float: right;
        }

        .qb-wrapper .col-xs-5 {
            width: 40%;
            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .qb-wrapper .col-xs-7 {
            width: 57%;

            float: left;
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .qb-wrapper .col-xs-push-7 {
            left: 57%;
        }

        .qb-wrapper .col-xs-5 {
            width: 40%;
        }

        #subtotal {
            margin-top: 15px;
            padding-top: 10px;
            border-top: 1px dotted #ccc;
        }

        .qb-wrapper .row {
            margin-left: -10px;
            margin-right: -10px;
        }

        .clearfix:after, .container:after, .container-fluid:after, .row:after, .form-horizontal .form-group:after, .btn-toolbar:after, .btn-group-vertical > .btn-group:after, .nav:after, .navbar:after, .navbar-header:after, .navbar-collapse:after, .pager:after, .panel-body:after, .modal-header:after, .modal-footer:after {
            clear: both;
        }

        .break-lines {
            white-space: pre-line;
        }

        .qb-wrapper .col-xs-2 {
            width: 14%;
        }

        .qb-wrapper .col-xs-1, .qb-wrapper .col-xs-10, .qb-wrapper .col-xs-11, .qb-wrapper .col-xs-12, .qb-wrapper .col-xs-2, .qb-wrapper .col-xs-3, .qb-wrapper .col-xs-4, .qb-wrapper .col-xs-5, .qb-wrapper .col-xs-6, .qb-wrapper .col-xs-7, .qb-wrapper .col-xs-8, .qb-wrapper .col-xs-9 {
            float: left;
        }

        .qb-wrapper .col-lg-1, .qb-wrapper .col-lg-10, .qb-wrapper .col-lg-11, .qb-wrapper .col-lg-12, .qb-wrapper .col-lg-2, .qb-wrapper .col-lg-3, .qb-wrapper .col-lg-4, .qb-wrapper .col-lg-5, .qb-wrapper .col-lg-6, .qb-wrapper .col-lg-7, .qb-wrapper .col-lg-8, .qb-wrapper .col-lg-9, .qb-wrapper .col-md-1, .qb-wrapper .col-md-10, .qb-wrapper .col-md-11, .qb-wrapper .col-md-12, .qb-wrapper .col-md-2, .qb-wrapper .col-md-3, .qb-wrapper .col-md-4, .qb-wrapper .col-md-5, .qb-wrapper .col-md-6, .qb-wrapper .col-md-7, .qb-wrapper .col-md-8, .qb-wrapper .col-md-9, .qb-wrapper .col-sm-1, .qb-wrapper .col-sm-10, .qb-wrapper .col-sm-11, .qb-wrapper .col-sm-12, .qb-wrapper .col-sm-2, .qb-wrapper .col-sm-3, .qb-wrapper .col-sm-4, .qb-wrapper .col-sm-5, .qb-wrapper .col-sm-6, .qb-wrapper .col-sm-7, .qb-wrapper .col-sm-8, .qb-wrapper .col-sm-9, .qb-wrapper .col-xs-1, .qb-wrapper .col-xs-10, .qb-wrapper .col-xs-11, .qb-wrapper .col-xs-12, .qb-wrapper .col-xs-2, .qb-wrapper .col-xs-3, .qb-wrapper .col-xs-4, .qb-wrapper .col-xs-5, .qb-wrapper .col-xs-6, .qb-wrapper .col-xs-7, .qb-wrapper .col-xs-8, .qb-wrapper .col-xs-9 {
            position: relative;
            min-height: 1px;
            padding-left: 10px;
            padding-right: 10px;
        }

        .bgcolor {
            background: #dce9f1;
            color: #7eaecc;
        }

        .full-header {
            padding: 6px 0;
            margin-top: 15px;
        }

        .invoice-label {
            font-weight: bold;
        }

        .dyi-line-item {
            padding-top: 15px;
            padding-bottom: 15px;
        }

        .logo-holder2 img{
            width:100%;
        }

    </style>

</head>

<body>

<div class="qb-wrapper">
    <div class="invoice-app">
        <div id="dyiPDF">
            <div class="tpl-airy ">
                <div class="inner">
                    <div class="row">
                        <div class="col-xs-8">
                            <p class="lead text-sample">{{$invoice->sender->FirstName}} {{$invoice->sender->LastName}} </p>
                            <span class="text-sample">{{$invoice->sender->HomeAdress}}  </span><br>
                            <!--      <span class="text-sample">
                          City
                          <span ng-hide="itt_remove_state" class=" ng-hide">State </span>
                          Zip code
                      </span><br>
                                  -->
                            <span class="text-sample">{{$invoice->sender->PhoneNumber}}   </span><br>
                            <span class="text-sample"> {{$invoice->sender->email}} </span><br>
                            <!--               <span class="text-sample">Website</span><br>  -->
                        </div>
                        <div id="businessLogoWrap" class="col-xs-4 text-right">
                            <div class="logo-holder2 " >
                                <img src="/var/www/smartcontracts/public/img/invoiceLogo.jpg">

                            </div>

                            <!--
                            @if($invoice->logo=='')
                            <div class="logo-holder " style="color:#4c8fbd; background-color:#cae1ed">
                                YOUR LOGO HERE
                            </div>
                                @else
                                <div class="logo-holder2 " >
                                 <img src="{{$invoice->logo}}">

                                </div>
                            @endif
                            -->

                        </div>
                    </div>

                    <div>
                        <h2 class="primary-color " style="color:#4c8fbd">Invoice</h2>
                        <div class="row font-adjust">
                            <div class="col-xs-4">
                                <span class="invoice-label ">BILL TO</span>
                                <!-- <p class="break-lines text-sample "> -->
                                <p class="lead text-sample">{{$invoice->receiver->FirstName}} {{$invoice->receiver->LastName}} </p>
                            <!--     <span class="text-sample">{{$invoice->receiver->HomeAdress}}  </span><br>
                                    <span class="text-sample">
                              City
                              <span ng-hide="itt_remove_state" class=" ng-hide">State </span>
                              Zip code
                          </span><br>

                                <span class="text-sample">{{$invoice->receiver->PhoneNumber}}   </span><br> -->
                                <span class="text-sample"> {{$invoice->receiver->email}} </span><br>
                            </div>
                            <div class="col-xs-4">
                                <span class="invoice-label ">SHIP TO</span>
                                <p class="lead text-sample">{{$invoice->sender->FirstName}} {{$invoice->sender->LastName}} </p>
                            <!--     <span class="text-sample">{{$invoice->receiver->HomeAdress}}  </span><br>
                                    <span class="text-sample">
                              City
                              <span ng-hide="itt_remove_state" class=" ng-hide">State </span>
                              Zip code
                          </span><br>

                                <span class="text-sample">{{$invoice->receiver->PhoneNumber}}   </span><br> -->
                                <span class="text-sample"> {{$invoice->sender->email}} </span><br>
                            </div>
                            <div class="col-xs-2 pr-pr invoice-label " style="text-align:right">
                                INVOICE: <br>

                                DATE: <br>
                                <br>
                                DUE DATE: <br>
                            </div>
                            <div class="col-xs-2 pr-pr">
                                <span class="text-sample">{{ $invoice->id }}</span><br>
                                <span class=" customInvoiceDateResult">{{ $invoice->created_at }}</span><br>
                                <span class=" customDueDateResult"> {{$invoice->PaymentDate}}</span>
                            </div>
                        </div>
                    </div>

                    <!-- second heading -->
                    <div class="row full-header bgcolor" style="color:#4c8fbd; background-color:#cae1ed">
                        <div class="col-xs-2 cell-date invoice-label ">DATE</div>
                        <div class="col-xs-4 cell-activity invoice-label ">ACTIVITY</div>
                        <div class="col-xs-2 cell-amount text-right pr25 invoice-label ">AMOUNT</div>
                    </div>

                    <!-- begin line items -->
                    <div>
                        @foreach ($invoice->lines as $line)
                            <div ng-repeat="item in invoiceInputs" class="">
                                <div class="row dyi-line-item font-adjust">
                                    <div class="col-xs-2 cell-date ">{{ $invoice->created_at }}</div>
                                    <div class="col-xs-4 cell-activity ">{{ $line->description }}</div>
                                    <div class="col-xs-2 cell-amount text-right ">${{ $line->amount }}</div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <!-- subtotal -->
                    <div id="subtotal" class="row font-adjust ">
                        <div class="col-xs-5 col-xs-push-7 pr-pr" style="
    float: right;
">
                            <div>
              <!--                  SUBTOTAL ${{$invoice->SubTotal }}<br>  -->
                                Late Fee’s ${{$invoice->LateFee}}<br>
                                Processing Fee ${{ $invoice->serviceFee }} +  Gas fee ${{ $invoice->gasPrice }}<br>
                                TOTAL ${{$invoice->total }}</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>

