<!DOCTYPE html>
<html>
<head>

</head>
<body>

@if ($userRegistered==false)

    <p> Congratulations!</p>
    <p>{{$whoInvited->role}} {{$whoInvited->name}} / {{$whoInvited->email}}/ has added you to sign a contract {{$leaseId}} with a registered
        address at {{$propertyAdress}}.</p>
    <p> Please, follow the link below to sign away. </p>
    <p><a href="http://52.234.147.185/show_form/{{$leaseId}}">http://52.234.147.185/show_form/{{$leaseId}} </a></p>

    <p>Cheers, </p>
    <p>Vertag. </p>

@else

    <p>Congratulations!</p>
    <p> {{$whoInvited->role}}  {{$whoInvited->name}} / {{$whoInvited->email}} / has added you to sign a contract {{$leaseId}} with a registered address at {{$propertyAdress}}.
    </p>
    <p> Please, follow the link below to register as a {{$userRegistered->role}} with the email {{$userRegistered->email}} and sign
        the contract {{$leaseId}} in your profile.  </p>
    <p><a href="http://52.234.147.185/register">http://52.234.147.185/register </a></p>
    <p>Cheers, </p>
    <p>Vertag. </p>


@endif


</body>

