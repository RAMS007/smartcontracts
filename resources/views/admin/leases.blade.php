@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Leases list</h1>
            </div>
            <div class="wrapper-md">
          <!--      <button class="btn btn-info "><a
                            href="/admin/invoice/new"> New Invoice </a></button>
                -->

                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables
                    </div>
                    <div class="table-responsive">
                        <table ui-jq="dataTable" class="table table-striped b-t b-b">
                            <thead>
                            <tr>
                                <th> Id</th>
                                <th>landlord_user_id</th>
                                <th>status</th>
                                <th>address1 </th>
                                <th>address2</th>

                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($leases as $lease )
                                <tr>
                                    <td> {{$lease->id}} </td>
                                    <td>{{$lease->landlord_user_id}}</td>
                                    <td> {{$lease->status}}  </td>
                                    <td> {{$lease->address1}}  </td>
                                    <td> {{$lease->address1}}  </td>


                                    <td>
                                        <button class="btn btn-info "><a
                                                    href="/admin/lease/{{$lease->id}}"> Edit </a></button>

                                        <button class="btn btn-danger deleteUser"
                                                data-uid="{{$lease->id}}"><i class="fa fa-trash-o"></i>Delete
                                        </button>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>

        $('.deleteUser').click(function () {

            console.log($(this).data('uid'));
            var postData = {
                'id': $(this).data('uid'),
                'action': 'delete'
            };
            $.ajax({
                method: "POST",
                url: "/admin/leases/delete",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })

   /*
        $('#RunGenerator').click(function () {

            $.ajax({
                method: "GET",
                url: "/api/invoice"
            })
                    .done(function (msg) {
                        window.location.href="/admin/invoices";
                    });


        })
*/

    </script>

@endsection