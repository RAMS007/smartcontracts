@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Invoice list</h1>
            </div>
            <div class="wrapper-md">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Create invoice
                    </div>
                    <div class="table-responsive">
                        <form id="newInvoice">

                            {{csrf_field()}}
                            <div class="form-group">
                                <label>Who generate invoice </label>
                                <input type="email" class="form-control" placeholder="Who generate invoice"
                                       value="{{$user->email}}" name="WhoGenerate">
                            </div>

                            <div class="form-group">
                                <label>Invoice for </label>
                                <input type="email" class="form-control" placeholder="invoice for" name="InvoiceFor">
                            </div>

                            <div class="form-group">
                                <label>Note </label>
                                <input type="text" class="form-control" placeholder="note" name="note">
                            </div>

                            <div class="form-group">
                                <label>Paid Until </label>
                                <input type="date" class="form-control" placeholder="paid Until" name="paidUntil">
                            </div>


                            <!-- https://editor.datatables.net/examples/inline-editing/simple -->

                            <h2>Add records</h2>
                            <div class="form-group">
                                <label> Description </label>
                                <input type="text" class="form-control" placeholder="Description" id="Description">
                            </div>
                            <div class="form-group">
                                <label>Amount </label>
                                <input type="text" class="form-control" placeholder="Amount" id="Amount">
                            </div>
                            <div class="form-group">
                                <label>Tax </label>
                                <input type="text" class="form-control" placeholder="Tax" id="Tax">
                            </div>

                            <button id="AddRow" type="button">Add record</button>


                            <table class="table table-striped b-t b-b " id="InvoiceList">
                                <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Tax</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Total:</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>


                            <button id="Save" type="button">Save Invoice</button>
                        </form>
                    </div>
                </div>
            </div>


            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>


        $('.deleteUser').click(function () {

            console.log($(this).data('uid'));
            var postData = {
                'uid': $(this).data('uid'),
                'action': 'delete'
            };
            $.ajax({
                method: "POST",
                url: "/api/invoice/delete",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })


    </script>


    <script>


        $(document).ready(function () {

            var table = $('#InvoiceList').DataTable(
                    {

                        columns: [
                            {data: 'description'},
                            {data: 'amount'},
                            {data: 'tax'},
                            {data: 'Action'},
                        ],
                        "footerCallback": function (row, data, start, end, display) {
                            var api = this.api(), data;

                            // Remove the formatting to get integer data for summation
                            var intVal = function (i) {
                                return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '') * 1 :
                                        typeof i === 'number' ?
                                                i : 0;
                            };


                            // Total over this page
                            TotalAmount = api
                                    .column(1, {page: 'current'})
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);

                            TotalTax = api
                                    .column(2, {page: 'current'})
                                    .data()
                                    .reduce(function (a, b) {
                                        return intVal(a) + intVal(b);
                                    }, 0);


                            // Update footer
                            $(api.column(1).footer()).html(
                                    '$' + TotalAmount
                            );

                            $(api.column(2).footer()).html(
                                    '$' + TotalTax
                            );

                        }


                    }
            );

            /*       table.row.add({
             "Description": "Tiger Nixon",
             "Amount": "14",
             "Tax": "$3,120",
             "Action": "<a class='DeleteRow'>delete</a>",

             }).draw();
             */

            $('#InvoiceList').on('click', 'tbody tr .DeleteRow', function () {
                table.row($(this).parent()).remove().draw(false)
            });


            $('#Save').click(function () {
                var data = table
                        .rows()
                        .data().toArray();
                ;
                console.log(data);

                $.ajax({
                    method: "POST",
                    url: "/admin/invoice/new",
                    data: {
                        'data': JSON.stringify(data),
                        'form': $('#newInvoice').serialize()
                    }
                })
                        .done(function (msg) {

                            if (msg.error == false) {
                                toastr.success(msg.msg);
                            } else {
                                toastr.warning(msg.msg);
                            }


                        });

            })


            $('#AddRow').click(function () {

                if ($('#Description').val() == '' || $('#Amount').val() == '' || $('#Tax').val() == '') {
                    return false;
                }
                table.row.add({
                    "description": $('#Description').val(),
                    "amount": $('#Amount').val(),
                    "tax": $('#Tax').val(),
                    "Action": "<a class='DeleteRow'>delete</a>",

                }).draw();


            })


        });


    </script>

@endsection