@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Payouts list</h1>
            </div>
            <div class="wrapper-md">
          <!--      <button class="btn btn-info "><a
                            href="/admin/invoice/new"> New Invoice </a></button>
                -->

                <div class="panel panel-default">
                    <div class="panel-heading">

                    </div>
                    <div class="table-responsive">
                        <table ui-jq="dataTable" class="table table-striped b-t b-b">
                            <thead>
                            <tr>
                                <th> Id</th>
                                <th>payoutId</th>
                                <th>paid_at</th>
                                <th>paypalAccount </th>
                                <th>paymentAmount</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payouts as $payout )
                                <tr>
                                    <td> {{$payout['id']}} </td>
                                    <td>{{$payout['payoutId']}}</td>
                                    <td> {{$payout['paid_at']}}  </td>
                                    <td> {{$payout['paypalAccount']}}  </td>
                                    <td> {{$payout['paymentAmount']}}  </td>

                                    <td>
                                        @if($payout['payoutId']=='')
                                        <button class="btn m-b-xs btn-sm btn-danger btn-addon makePayout"  data-payuotId="{{$payout['id']}}"> Make payout</button>
                                    @else
                                          Payout already done at {{$payout['payuot_paid_at']}}

                                        @endif

                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>

        $('.makePayout').click(function () {

            console.log($(this).data('payuotid'));

            var postData = {
                'id': $(this).data('payuotid')
            };
            $.ajax({
                method: "POST",
                url: "/admin/payouts/make",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })

   /*
        $('#RunGenerator').click(function () {

            $.ajax({
                method: "GET",
                url: "/api/invoice"
            })
                    .done(function (msg) {
                        window.location.href="/admin/invoices";
                    });


        })
*/

    </script>

@endsection