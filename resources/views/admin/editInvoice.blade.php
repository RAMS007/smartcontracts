@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Invoice</h1>
            </div>


            <form role="form" method="POST" >
                {{csrf_field()}}
                <div class="form-group">
                    <label>Who generate invoice </label>
                    <input type="email" class="form-control" placeholder="Who generate invoice"
                           value="{{$invoice->user_generated}}" name="WhoGenerate">
                </div>

                <div class="form-group">
                    <label>Invoice for </label>
                    <input type="email" class="form-control" placeholder="invoice for" name="InvoiceFor" value="{{$invoice->user_who_pay}}">
                </div>

                <div class="form-group">
                    <label>Note </label>
                    <input type="text" class="form-control" placeholder="note" name="note" value="{{$invoice->note}}">
                </div>

                <div class="form-group">
                    <label>Paid Until </label>
                    <input type="date" class="form-control" placeholder="paid Until" name="paidUntil" value="{{$invoice->paidUntil}}">
                </div>

                <div class="form-group">
                    <label>Status</label>
                    <select  class="form-control" name="status" >
                        <option value="new" @if($invoice->status=='new') selected  @endif >new</option>
                        <option value="paid" @if($invoice->status=='paid') selected  @endif >paid</option>
                        <option value="notPaid"  @if($invoice->status=='notPaid') selected  @endif>notPaid</option>


                    </select>
                </div>


                <button type="submit" class="btn btn-sm btn-primary">Save</button>
            </form>



            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')


@endsection