@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">User</h1>
            </div>


            <form role="form" method="POST" >
                {{csrf_field()}}
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" placeholder="Enter email" value="{{$user->email}}" name="email">
                </div>
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" placeholder="name" value="{{$user->name}}" name="name">
                </div>

                <div class="form-group">
                    <label>Role</label>
                    <select  class="form-control" name="role" >
                        <option value="user" @if($user->role=='landlord') selected  @endif >landlord</option>
                        <option value="user" @if($user->role=='tenant') selected  @endif >tenant</option>
                        <option value="admin"  @if($user->role=='admin') selected  @endif>admin</option>


                    </select>
                </div>


                <button type="submit" class="btn btn-sm btn-primary">Save</button>
            </form>



            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>
        $('.deleteUser').click(function () {

            console.log($(this).data('uid'));
            var postData = {
                'uid': $(this).data('uid'),
                'action': 'delete'
            };
            $.ajax({
                method: "POST",
                url: "/api/users/delete",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })


    </script>

@endsection