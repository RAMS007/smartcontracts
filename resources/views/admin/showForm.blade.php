@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3"> Lease</h1>
            </div>


            <div class="wrapper-md">
                <!-- stats -->
                <div class="row">
                    <div class="col-sm-6">
                        Property Adress1
                    </div>
                    <div class="col-sm-6">
                        {{$property->address1}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        Property Adress
                    </div>
                    <div class="col-sm-6">
                        {{$property->address2}}
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        City
                    </div>
                    <div class="col-sm-6">
                        {{$property->city}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        State
                    </div>
                    <div class="col-sm-6">
                        {{$property->state}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        ZIP
                    </div>
                    <div class="col-sm-6">
                        {{$property->zip}}
                    </div>
                </div>

                <h1> Lease content</h1>
                <h2> Starting and ending dates </h2>
                <div class="row">
                    <div class="col-sm-6">
                        Move In Date
                    </div>
                    <div class="col-sm-6">
                        {{$form->moveInDate}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        Move Out Date
                    </div>
                    <div class="col-sm-6">
                        {{$form->MoveOutDate}}
                    </div>
                </div>

                <div ng-if="$form->AdditionalOccupants==1">
                    <h2> Additional Occupants of Property </h2>
                    <div class="row">
                        <div class="col-sm-6">
                            Number of Additional Occupants
                        </div>
                        <div class="col-sm-6">
                            {{$form->AdditionalOccupants_number}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Additional Occupants Name
                        </div>
                        <div class="col-sm-3">
                            {{$form->AdditionalOccupants_FirstName}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->AdditionalOccupants_LastName}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Additional Occupants Email
                        </div>
                        <div class="col-sm-6">
                            {{$form->AdditionalOccupants_email}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Additional Occupants Phone Number
                        </div>
                        <div class="col-sm-3">
                            {{$form->AdditionalOccupants_AreaCode}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->AdditionalOccupants_PhoneNumber}}
                        </div>
                    </div>
                </div>

                <div>
                    <h1>Next of Kin\Emergency Contact</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            Emergency Contact Full Name
                        </div>
                        <div class="col-sm-3">
                            {{$form->Emergency_FirstName}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->Emergency_LastName}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Emergency Contact Relationship
                        </div>
                        <div class="col-sm-6">
                            {{$form->Emergency_relationship}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Emergency Contact Email
                        </div>
                        <div class="col-sm-6">
                            {{$form->Emergency_email}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Emergency Contact Phone Number
                        </div>
                        <div class="col-sm-3">
                            {{$form->Emergency_AreaCode}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->Emergency_PhoneNumber}}
                        </div>
                    </div>
                </div>

                <div ng-if="$form->PropertyManager==1">
                    <h1>Property Manager Information</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            Property Manager Name
                        </div>
                        <div class="col-sm-3">
                            {{$form->PropertyManager_FirstName}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->PropertyManager_LastName}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Property Manager Email
                        </div>
                        <div class="col-sm-6">
                            {{$form->PropertyManager_email}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Property Manager Address
                        </div>
                        <div class="col-sm-6">
                            {{$form->PropertyManager_address}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Property Manager Phone Number
                        </div>
                        <div class="col-sm-3">
                            {{$form->PropertyManager_AreaCode}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->PropertyManager_PhoneNumber}}
                        </div>
                    </div>

                </div>


                <div ng-if="$form->RealEstate==1">
                    <h1>Real Estate Broker Information</h1>
                    <div class="row">
                        <div class="col-sm-6">
                            Which Jurisdiction Is The Broker Licensed To Conduct Business In?
                        </div>
                        <div class="col-sm-6">
                            {{$form->RealEstate_Jurisdiction}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Real Estate Broker Name
                        </div>
                        <div class="col-sm-3">
                            {{$form->RealEstate_FirstName}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->RealEstate_LastName}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Real Estate Email
                        </div>
                        <div class="col-sm-6">
                            {{$form->RealEstate_email}}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            Real Estate Broker Phone Number
                        </div>
                        <div class="col-sm-3">
                            {{$form->RealEstate_AreaCode}}
                        </div>
                        <div class="col-sm-3">
                            {{$form->RealEstate_PhoneNumber}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            Real Estate Broker Address
                        </div>
                        <div class="col-sm-6">
                            {{$form->RealEstate_address}}
                        </div>
                    </div>
                    <!--
                                        <div class="row">
                                            <div class="col-sm-6">
                                                Real Estate Broker Relationship to
                                            </div>
                                            <div class="col-sm-6">
                                                <span ng-if="$form->RealEstate_LLrelationship==1">Landlord</span> <span
                                                        ng-if="$form->RealEstate_Trelationship==1">Tenant</span>
                                            </div>
                                        </div>
                    -->

                    <div ng-if="$form->RealEstate_Additional==1">
                        <h2> Additional Brokers Involved In Transaction </h2>
                        <div class="row">
                            <div class="col-sm-6">
                                Which Jurisdiction Is The Broker Licensed To Conduct Business In?
                            </div>
                            <div class="col-sm-6">
                                {{$form->RealEstate_Additional_Jurisdiction}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                Additional Broker Name
                            </div>
                            <div class="col-sm-3">
                                {{$form->RealEstate_Additional_FirstName}}
                            </div>
                            <div class="col-sm-3">
                                {{$form->RealEstate_Additional_LastName}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                Additional Broker Email
                            </div>
                            <div class="col-sm-6">
                                {{$form->RealEstate_Additional_email}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                Additional Broker Phone Number
                            </div>
                            <div class="col-sm-3">
                                {{$form->RealEstate_Additional_AreaCode}}
                            </div>
                            <div class="col-sm-3">
                                {{$form->RealEstate_Additional_PhoneNumber}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                Additional Broker Address
                            </div>
                            <div class="col-sm-6">
                                {{$form->RealEstate_Additional_address}}
                            </div>
                        </div>


                    </div>
                </div>

                <h1>Terms and Conditions</h1>

                @if($form->GoverningLawEnabled==1 )
                    <div>
                        <p><strong>GOVERNING LAW:</strong></p>

                        @if($form->GoverningLawEdit==0)
                            <div>
                                <p>This Lease shall be governed by and construed in accordance with the laws of the
                                    State
                                    of $form->Lease_state.</p>
                            </div>
                        @endif
                        @if($form->GoverningLawEdit==1)
                            <div>
                                {{$form->GoverningLawCustom}}
                            </div>
                        @endif
                    </div>

                @endif

                @if($form->LeasePaymentsEnabled==1)
                    <div>
                        <p><strong>LEASE PAYMENTS: </strong></p>

                        @if($form->LeasePaymentsEdit==0)
                            <div>
                                <p>Tenant agrees to pay Landlord for use of the Premises in the amount of
                                    {{ $form->Lease_monthAmount }} each month in
                                    advance on
                                    the {{$form->Lease_payday }}
                                    day of with
                                    payment to be made by {{$form->Lease_payBy}}.</p>
                                <p>If the Lease Term does not start on the
                                    {{ $form->Lease_startDay}}
                                    day of the month or end on the last day of a month, the first and last
                                    month’s
                                    rent
                                    will be prorated accordingly.</p>
                            </div>
                        @endif
                        @if($form->LeasePaymentsEdit==1)
                            <div>
                                {{$form->LeasePaymentsCustom}}
                            </div>
                        @endif

                    </div>
                @endif

                @if($form->InsufficientFundsEnabled==1)
                    <div>
                        <p><strong>INSUFFICIENT FUNDS: </strong></p>
                        @if($form->InsufficientFundsEdit==0)
                            <div>
                                <p>Tenant agrees to pay a charge of
                                    {{$form->Lease_insufficientFunds }} for
                                    each check given by Tenant to Landlord that is returned to Landlord for
                                    lack of
                                    sufficient funds (NSF).</p>
                            </div>
                        @else
                            <div>
                                {{$form->InsufficientFundsCustom}}
                            </div>
                        @endif
                    </div>
                @endif

                @if($form->SecurityDepositEnabled==1)
                    <div>
                        <p><strong>SECURITY DEPOSIT: </strong></p>
                        @if($form->SecurityDepositEdit==0)
                            <div>
                                <p>At the signing of this Lease, Tenant shall deposit with Landlord, in trust, a
                                    security
                                    deposit of {{ $form->Lease_SecurityDeposit}} as security for
                                    the performance by Tenant of the terms under this Lease and for any damages caused
                                    by
                                    Tenant, Tenant’s family, agents and visitors to the Premises during the term of this
                                    Lease. Landlord may use part or all of the security deposit to repair any damage to
                                    the
                                    Premises caused by Tenant, Tenant’s family, agents and visitors to the Premises.
                                </p>
                                <p>However, Landlord is not limited to the security deposit amount and
                                    Tenant remains liable
                                    for any balance. Tenant shall not apply or deduct any portion of any
                                    security deposit from the last or any month’s rent. Tenant shall not use or apply
                                    any
                                    such security deposit at any time in lieu of payment of rent. If Tenant breaches any
                                    terms or conditions of this Lease, Tenant shall forfeit any deposit, as permitted
                                    by law.</p>

                            </div>
                        @else
                            <div>
                                {{$form->SecurityDepositCustom}}
                            </div>
                        @endif
                    </div>
                @endif

                @if($form->DefaultsEnabled==1)
                    <div>
                        <p><strong>DEFAULTS: </strong></p>
                        @if($form->DefaultsEdit==0)
                            <div>
                                <p>If Tenant fails to perform or fulfill any obligation under this Lease, Tenant
                                    shall be in default of this Lease. Subject to any statute ordinance or law to the
                                    contrary, Tenant shall have the State minimum requirement or seven (7) days,
                                    whichever is
                                    less, from the date of notice of default by Landlord to cure the default. In the
                                    event
                                    Tenant does not cure a default, Landlord may at Landlord’s option:</p>
                                <p><strong>a)</strong> cure such default and the cost of such action may be added to
                                    Tenant’s financial obligations under this Lease; or </p>
                                <p><strong>b)</strong> declare Tenant in default of the Lease. In the event
                                    of default, Landlord may also, as permitted by law, re-enter the Premises and
                                    re-take possession of the Premises.</p>
                                <p> Landlord may, at its option, hold Tenant liable for any difference
                                    between the rent that would have been payable under this Lease during the balance of
                                    the
                                    unexpired term, if this Lease had continued in force and any rent paid by any
                                    successive Tenant if the Premises are re-let. In the event Landlord is unable to
                                    re-let
                                    the Premises during any remaining term of this Lease, after default by Tenant,
                                    Landlord may at its
                                    option hold Tenant liable for the balance of the unpaid rent under this
                                    Lease if this Lease had continued in force. The failure of Tenants or their guests
                                    or
                                    invitees to comply with any term of this Agreement is grounds for termination of the
                                    tenancy, with appropriate notice to Tenants and procedures as required by
                                    law.</p>
                            </div>
                        @else
                            <div>
                                {{$form->DefaultsCustom}}
                            </div>
                        @endif
                    </div>
                @endif


                @if($form->QuietEnjoymentEnabled==1)
                    <div>
                        <p><strong>QUIET ENJOYMENT: </strong></p>
                        @if($form->QuietEnjoymentEdit==0)
                            <div>
                                <p>Tenant shall be entitled to quiet enjoyment of the Premises and Landlord
                                    will not interfere with that right, as long as Tenant pays the rent in a timely
                                    manner
                                    and performs all other obligations under this Lease.</p>

                            </div>
                        @else
                            <div>
                                {{$form->QuietEnjoymentCustom}}

                            </div>
                        @endif
                    </div>
                @endif

                @if($form->PosessionEnabled==1)
                    <div ng-if="">
                        <p><strong>POSSESSION AND SURRENDER OF PREMISES: </strong></p>
                        @if($form->PosessionEdit==0)
                            <div ng-if="">
                                <p>Tenant shall be entitled to possession of the Premises on
                                    the {{$form->Lease_Posession_day }}
                                    day
                                    of the Lease Term. At the expiration of the Lease, Tenant shall peaceably surrender
                                    the Premises to the Landlord or Landlord’s agent in good condition, as it was at the
                                    commencement of the Lease, reasonable wear and tear excepted.</p>
                            </div>
                        @else
                            <div>
                                {{$form->PosessionCustom}}
                            </div>
                        @endif
                    </div>
                @endif

                @if($form->PremisessEnabled==1)
                    <div ng-if="">
                        <p><strong>USE OF PREMISES: </strong></p>
                        @if($form->PremisessEdit==0)
                            <div>
                                <p>Tenant shall only use the Premises as a residence. The Premises shall not be used
                                    to carry on any type of business or trade without prior written consent of the
                                    Landlord. Tenant will comply with all laws, rules, ordinances, statutes and orders
                                    regarding the use of the Premises.</p>

                            </div>
                        @else
                            <div>
                                {{$form->PremisessCustom}}

                            </div>
                        @endif
                    </div>
                @endif

                @if($form->OccupantsEnabled==1)
                    <div>
                        <p><strong>OCCUPANTS: </strong></p>
                        @if($form->OccupantsEdit==0)
                            <div>
                                <p>Tenant agrees that no more than {{$form->Lease_OccupantsAllowed }} persons may reside
                                    on the
                                    Premises without prior written consent of the Landlord.</p>
                            </div>
                        @else
                            <div>
                                {{$form->OccupantsCustom}}
                            </div>
                        @endif
                    </div>
                @endif

                @if($form->VisitorsEnabled==1)
                    <div ng-if="">
                        <p><strong>VISITORS: </strong></p>
                        @if($form->VisitorsEdit==0)
                            <div ng-if="">
                                <p>Tenant shall be allowed to have Visitors on the Premises for a period of
                                    {{$form->Lease_VisitorsPeriodAllowed }} days without prior written consent of the
                                    Landlord.</p>
                            </div>
                        @else
                            <div>
                                {{$form->VisitorsCustom}}
                            </div>
                        @endif
                    </div>
                @endif

                @if($form->ConditionOfPremissesEnabled==1)
                    <div ng-if="">
                        <p><strong> CONDITION OF PREMISES: </strong></p>
                        @if($form->ConditionOfPremissesEdit==0)
                            <div ng-if="">
                                <p>Tenant or Tenant’s agent have inspected the Premises, the fixtures, the
                                    grounds, building and improvements and acknowledges that the Premises are in good
                                    and
                                    acceptable condition and are habitable. If at any time during the term of this
                                    Lease, in
                                    Tenant’s opinion, the conditions change, Tenant shall promptly provide reasonable
                                    notice
                                    to Landlord.</p>

                            </div>
                        @else
                            <div>
                                {{$form->ConditionOfPremissesCustom}}

                            </div>
                        @endif
                    </div>
                @endif

                @if($form->AssigmentEnabled==1)
                    <div ng-if="">
                        <p><strong> ASSIGNMENT AND SUBLEASE: </strong></p>
                        @if($form->AssigmentEdit==0)
                            <div ng-if="">
                                <p>Tenant shall not assign or sublease any interest in this Lease without prior written
                                    consent
                                    of
                                    the Landlord, which consent shall not be unreasonably
                                    withheld.</p>
                                <p>Any assignment or sublease without Landlord’s written prior consent shall, at
                                    Landlord’s
                                    option,
                                    terminate this Lease.</p>

                            </div>
                        @else
                            <div>
                                {{$form->AssigmentCustom}}

                            </div>
                        @endif
                    </div>
                @endif
                <div ng-if="$form->DangerousMaterialsEnabled==1">
                    <p><strong> DANGEROUS MATERIALS: </strong></p>
                    <div ng-if="$form->DangerousMaterialsEdit==0">
                        <p>Tenant shall not keep or have on or around the Premises any item of a dangerous, flammable or
                            explosive nature that might unreasonably increase the risk
                            of fire or explosion on or around the Premises or that might be considered
                            hazardous by any responsible insurance company. </p>

                    </div>
                    <div ng-if="$form->DangerousMaterialsEdit==1" {{$form->DangerousMaterialsCustom}} >
                    </div>
                </div>

                <div ng-if="$form->UtilitesEnabled==1">
                    <p><strong> UTILITIES AND SERVICES: </strong></p>
                    <div ng-if="$form->UtilitesEdit==0">
                        <p>The following utilities and services will be the responsibility of the
                            following:</p>
                        <p>Electricity - {{$form->Lease_Service_Electricity }}<br>
                            Telephone Service - {{$form->Lease_Service_Telephone}} <br>
                            Cable (TV) - {{ $form->Lease_Service_TV}} <br>
                            Heat - {{ $form->Lease_Service_Heat }} <br>
                            Hot Water - {{ $form->Lease_Service_HotWater }} <br>
                            Water - {{ $form->Lease_Service_Water}} <br>
                            Garbage/Trash - {{ $form->Lease_Service_Garbage}} <br>
                            Snow Removal - {{ $form->Lease_Service_SnowRemoval}} <br>
                            Landscaping - {{$form->Lease_Service_Landscaping }} <br>
                        </p>

                    </div>
                    <div ng-if="$form->UtilitesEdit==1" {{$form->UtilitesCustom}} >
                    </div>
                </div>

                <div ng-if="$form->ApplianceIncludedEnabled==1">
                    <p><strong>APPLIANCES INCLUDED: </strong></p>
                    <div ng-if="$form->ApplianceIncludedEdit==0">
                        <p>The following appliances will be included with the rental of the property, enter yes or no
                            for
                            each item included:</p>
                        <p>Landlord is responsible for repair s to appliances listed above unless otherwise
                            stated here : Tenant is responsible for the 1st 100.00 of any appliance repair</p>
                        <p>Stove: {{$form->Lease_Included_Stove }}</p>
                        <p>Refrigerator : {{$form->Lease_Included_Refrigerator}}</p>
                        <p>Dishwasher : {{$form->Lease_Included_Dishwasher}}</p>
                        <p>Washing Machine: {{ $form->Lease_Included_WachingMachine}}</p>
                        <p>Dryer: {{$form->Lease_Included_Dryer }}</p>
                        <p>Garbage Disposal : {{ $form->Lease_Included_GarbageDisposal}}</p>
                        <p>Microwave: {{$form->Lease_Included_Microwawe }}</p>
                        <p>Toaster : {{$form->Lease_Included_Toaster }}</p>
                        <p>Air Conditioning Units : {{ $form->Lease_Included_AirConditioner}}</p>
                    </div>
                    <div ng-if="$form->ApplianceIncludedEdit==1" {{$form->ApplianceIncludedCustom}} >
                    </div>
                </div>

                <div ng-if="$form->PetsEnabled==1">
                    <p><strong>PETS: </strong></p>
                    <div ng-if="$form->PetsEdit==0">

                        <p>The Landlord agrees that the Tenant:</p>

                        <div ng-if="$form->Lease_Pets_allowed==1">
                            <strong>shall be allowed to keep</strong> a maximum of {{$form->Lease_Pets_maxAllowed }}
                            pets on
                            the property. The type of pets
                            allowed shall consist of {{$form->Lease_Pets_Allowedtypes }} In addition to the security
                            deposit,
                            there shall be a pet deposit of {{$form->Lease_Pets_SecurityDeposit}} that will be
                            {{$form->Lease_Pets_deposit_type}}
                            <p ng-if="$form->Lease_Pets_deposit_typ=='refundable' ">If refundable, the deposit will be
                                returned at the end of the
                                lease term if there is no damage due to the pet(s).</p>
                        </div>
                        <div ng-if="$form->Lease_Pets_allowed==0">
                            <strong> not allow</strong>
                        </div>
                    </div>
                    <div ng-if="$form->PetsEdit==1" {{$form->PetsCustom}} >
                    </div>
                </div>

                <div ng-if="$form->AlterationsEnabled==1">
                    <p><strong>ALTERATIONS AND IMPROVEMENTS: </strong></p>
                    <div ng-if="$form->AlterationsEdit==0">
                        <p>Tenant agrees not to make any improvements or alterations to the Premises
                            without prior written consent of the Landlord. If any alterations, improvement or
                            changes are made
                            to or built on or around the Premises, with the exception of fixtures
                            and personal property
                            that can be removed without damage to the Premises, they shall become the
                            property of Landlord and shall remain at the expiration of the Lease, unless
                            otherwise agreed in writing.</p>
                    </div>
                    <div ng-if="$form->AlterationsEdit==1" {{$form->AlterationsCustom}} >
                    </div>
                </div>

                <div ng-if="$form->DamagesEnabled==1">
                    <p><strong>DAMAGE TO PREMISES: </strong></p>
                    <div ng-if="$form->DamagesEdit==0">
                        <p>If the Premises or part of the Premises are damaged or destroyed by fire or
                            other casualty not due to Tenant’s negligence, the rent will be abated during the time that
                            the Premises are uninhabitable. If Landlord decides not to repair or rebuild
                            the Premises, then this Lease shall terminate and the rent shall be prorated up to the
                            time of the damage. Any unearned rent paid in advance shall be refunded to
                            Tenant.</p>
                    </div>
                    <div ng-if="$form->DamagesEdit==1" {{$form->DamagesCustom}} >
                    </div>
                </div>

                <div ng-if="$form->MaintanceAndRepairEnabled==1">
                    <p><strong>MAINTENANCE AND REPAIR: </strong></p>
                    <div ng-if="$form->MaintanceAndRepairEdit==0">
                        <p>Tenant will, at Tenant’s sole expense, keep and maintain the Premises in
                            good, clean and sanitary condition and repair during the term of this Lease and any
                            renewal thereof. Tenant shall be responsible to make all repairs to the Premises,
                            fixtures, appliances and equipment therein that may have been damaged by Tenant’s misuse,
                            waste or neglect, or that of the Tenant’s family, agents or visitors. Tenant agrees
                            that no painting will be done on or about the Premises without the prior written consent
                            of Landlord. Tenant shall promptly notify Landlord of any damage, defect or destruction
                            of the Premises or in the event of the failure of any of the appliances or equipment.
                            Landlord will use its best efforts to repair or replace any such damaged or defective
                            areas, appliances or equipment.</p>

                    </div>
                    <div ng-if="$form->MaintanceAndRepairEdit==1" {{$form->MaintanceAndRepairCustom}} >
                    </div>
                </div>

                <div ng-if="$form->RightOfInspectionEnabled==1">
                    <p><strong>RIGHT OF INSPECTION: </strong></p>
                    <div ng-if="$form->RightOfInspectionEdit==0">
                        <p>Tenant agrees to make the Premises available to Landlord or Landlord’s agents for
                            the purposes of inspection, making repairs or improvements, or to supply agreed
                            services or show the premises to prospective buyers or tenants, or in case of
                            emergency. Except in case of emergency, Landlord shall give Tenant reasonable notice of
                            intent to enter. For these purposes, the minimum statutory notice allowed or <strong>twenty-four
                                (24) hours</strong>, whichever is less, shall be deemed reasonable.
                        </p>
                        <p>Tenant shall not, without Landlord’s prior written consent, add, alter or
                            re-key any locks to the Premises. At all times Landlord shall be provided with
                            a key or keys capable of unlocking all such locks and gaining entry. Tenant
                            further agrees to notify Landlord in writing if Tenant installs any burglar alarm system,
                            including instructions on how to disarm it in case of emergency entry.</p>

                    </div>
                    <div ng-if="$form->RightOfInspectionEdit==1" {{$form->RightOfInspectionCustom}} >
                    </div>
                </div>

                <div ng-if="$form->AbandonmentEnabled==1">
                    <p><strong>ABANDONMENT: </strong></p>
                    <div ng-if="$form->AbandonmentEdit==0">
                        <p>If Tenant abandons the Premises of any personal property during the term of
                            this Lease, Landlord may, at their option, enter the Premises by any legal means
                            without liability to Tenant and may at Landlord’s option terminate the Lease.
                            Abandonment is defined as
                            absence of the Tenant from the Premises for at least {{ $form->Lease_Abandons_days}}
                            consecutive days without notice to Landlord.</p>
                        <p>If Tenant abandons the Premises while the rent is outstanding for more than 15 days
                            and there is not reasonable evidence, other than the presence of the
                            Tenants’ personal property, that the Tenant is occupying the unit, Landlord may at
                            Landlord’s option terminate this Lease Agreement and regain possession in the manner
                            prescribed by law. Landlord will dispose of all abandoned personal property on the
                            Premises in any manner allowed by law.</p>

                    </div>
                    <div ng-if="$form->AbandonmentEdit==1" {{$form->AbandonmentCustom}} >
                    </div>
                </div>

                <div ng-if="$form->ExtendedAbsensesEnabled==1">
                    <p><strong>EXTENDED ABSENCES: </strong></p>
                    <div ng-if="$form->ExtendedAbsensesEdit==0">
                        <p>In the event Tenant will be away from the Premises for more than the ExtendedAbsenses
                            period, Tenant agrees to notify Landlord in writing of such absence. During such
                            absence, Landlord may enter the premises at times reasonable necessary to maintain
                            the property and inspect for damages and needed repairs.</p>
                    </div>
                    <div ng-if="$form->ExtendedAbsensesEdit==1" {{$form->ExtendedAbsensesCustom}} >
                    </div>
                </div>


                <div ng-if="$form->SecurityEnabled==1">
                    <p><strong>SECURITY: </strong></p>
                    <div ng-if="$form->SecurityEdit==0">
                        <p>Tenant understands that Landlord does not provide any security alarm system
                            or other security for Tenant or the Premises. In the event any alarm system
                            is provided, Tenant understands that such alarm system is not warranted to be complete
                            in all respects or to
                            be sufficient to protect Tenant on the Premises. Tenant releases Landlord
                            from any loss, damage, claim or injury resulting from the failure of any alarm
                            system, security or from the lack of any alarm system or security.</p>

                    </div>
                    <div ng-if="$form->SecurityEdit==1" {{$form->SecurityCustom}} >
                    </div>
                </div>

                <div ng-if="$form->SeverabilityEnabled==1">
                    <p><strong>SEVERABILITY: </strong></p>
                    <div ng-if="$form->SeverabilityEdit==0">
                        <p>If any part of this Lease shall be held unenforceable for any reason, the
                            remainder of this Agreement shall continue in full force and effect. If any court
                            of competent jurisdiction deems any provision of this Lease invalid or
                            unenforceable, and if limiting
                            such provision would make the provision valid, then such provision shall be
                            deemed to be construed as so limited. </p>

                    </div>
                    <div ng-if="$form->SeverabilityEdit==1" {{$form->SeverabilityCustom}} >
                    </div>
                </div>


                <div ng-if="$form->InsuranceEnabled==1">
                    <p><strong>INSURANCE: </strong></p>
                    <div ng-if="$form->InsuranceEdit==0">
                        <p>Landlord and Tenant shall each be responsible to maintain appropriate insurance
                            for their respective interests in the Premises and property located on the Premises. Tenant
                            understands that Landlord will not provide any insurance
                            coverage for Tenant’s property. Landlord will not be responsible for any loss of Tenant’s
                            property, whether by theft, fire, riots, strikes, acts of God or otherwise. Landlord
                            encourages Tenant to obtain renter’s insurance or other similar coverage to protect
                            against risk of loss.</p>

                    </div>
                    <div ng-if="$form->InsuranceEdit==1" {{$form->InsuranceCustom}} >
                    </div>
                </div>

                <div ng-if="$form->BindingEffectEnabled==1">
                    <p><strong>BINDING EFFECT: </strong></p>
                    <div ng-if="$form->BindingEffectEdit==0">
                        <p>The covenants and conditions contained in the Lease shall apply to the Parties and the
                            heirs, legal representatives, successors and permitted assigns of the Parties.</p>
                    </div>
                    <div ng-if="$form->BindingEffectEdit==1" {{$form->BindingEffectCustom}} >
                    </div>
                </div>


                <div ng-if="$form->EntireAgrementEnabled==1">
                    <p><strong>ENTIRE AGREEMENT: </strong></p>
                    <div ng-if="$form->EntireAgrementEdit==0">
                        <p>This Lease constitutes the entire Agreement between the Parties and supersedes
                            any prior understanding or representation of any kind preceding the date of
                            this Agreement. There
                            are no other promises, conditions, understandings or other Agreements,
                            whether oral or written, relating to the subject matter of this Lease.</p>
                        <p>This Lease may be modified in writing and must be signed by both Landlord and Tenant.</p>

                    </div>
                    <div ng-if="$form->EntireAgrementEdit==1" {{$form->EntireAgrementCustom}} >
                    </div>
                </div>

                <div ng-if="$form->NoticeEnabled==1">
                    <p><strong>NOTICE: </strong></p>
                    <div ng-if="$form->NoticeEdit==0">
                        <p>Any notice required or otherwise given pursuant to this Lease shall be in
                            writing and mailed certified return receipt requested, postage prepaid, or
                            delivered by overnight delivery service, if to Tenant, the address of the Premises, and if
                            to Landlord, the following address {{ $form->Lease_Notice_address }} City of {{
                        $form->Lease_Notice_city}} ,
                            State of {{ $form->Lease_Notice_state}} , Zip Code {{ $form->Lease_Notice_Zip}}.
                            Either party may change such addresses from time to time by providing notice as set forth
                            above.</p>

                    </div>
                    <div ng-if="$form->NoticeEdit==1" {{$form->NoticeCustom}} >
                    </div>
                </div>


                <div ng-if="$form->CumulativeRightsEnabled==1">
                    <p><strong>CUMULATIVE RIGHTS: </strong></p>
                    <div ng-if="$form->CumulativeRightsEdit==0">
                        <p>Landlord’s and Tenant’s rights under this Lease are cumulative and shall not
                            be construed as exclusive of each other unless otherwise required by law.</p>

                    </div>
                    <div ng-if="$form->CumulativeRightsEdit==1" {{$form->CumulativeRightsCustom}} >
                    </div>
                </div>

                <div ng-if="$form->WaiverEnabled==1">
                    <p><strong>WAIVER: </strong></p>
                    <div ng-if="$form->WaiverEdit==0">
                        <p>The failure of either Party to enforce any provisions of the Lease shall not be
                            deemed a waiver of limitation of that Party’s right to subsequently enforce and
                            compel strict compliance with every provision of this Lease. The acceptance of
                            rent by Landlord does not waive Landlord’s right to enforce any provisions of this
                            Lease.</p>
                    </div>
                    <div ng-if="$form->WaiverEdit==1" {{$form->WaiverCustom}} >
                    </div>
                </div>


                <div ng-if="$form->IndemnificationEnabled==1">
                    <p><strong>INDEMNIFICATION: </strong></p>
                    <div ng-if="$form->IndemnificationEdit==0">
                        <p>To the extent permitted by law, Tenant will indemnify and hold Landlord and Landlord’s
                            property,
                            including the Premises, free and harmless from any liability for
                            losses, claims, injury to or death of any person, including Tenant, or for damage to
                            property arising from Tenant using and occupying the Premises or from the acts or
                            omissions of any person or persons, including Tenant, in or about the Premises
                            with Tenant’s express or implied consent except Landlord’s act or negligence. </p>

                    </div>
                    <div ng-if="$form->IndemnificationEdit==1" {{$form->IndemnificationCustom}} >
                    </div>
                </div>

                <div ng-if="$form->AttorneyFeesEnabled==1">
                    <p><strong>ATTORNEY FEES: </strong></p>
                    <div ng-if="$form->AttorneyFeesEdit==0">
                        <p>In the event that the Tenant violates the terms of the Lease or defaults in
                            the performance of any covenants in the Lease and the Landlord engages an
                            attorney or institutes a legal action, counterclaim, or summary proceeding
                            against Tenant based upon
                            such violation or default, Tenant shall be liable to Landlord for the costs
                            and expenses incurred in enforcing this Lease, including reasonable attorney fees
                            and costs. In the event the Tenant brings any action against the Landlord pursuant to
                            this Lease and the Landlord prevails, Tenant shall be liable to Landlord for costs and
                            expenses of defending such action, including reasonable attorney fees and costs. </p>

                    </div>
                    <div ng-if="$form->AttorneyFeesEdit==1" {{$form->AttorneyFeesCustom}} >
                    </div>
                </div>


                <div ng-if="$form->DisplaySignsEnabled==1">
                    <p><strong>DISPLAY OF SIGNS: </strong></p>
                    <div ng-if="$form->DisplaySignsEdit==0">
                        <p>Landlord or Landlord’s agent may display “For Sale” or “For Rent” or “Vacancy” or similar
                            signs on or about the Premises and enter to show the Premises to prospective tenants
                            during the last sixty (60) days of this Lease. Tenant agrees that no signs shall
                            be placed on the Premises without the prior written consent of the Landlord.</p>

                    </div>
                    <div ng-if="$form->DisplaySignsEdit==1" {{$form->DisplaySignsCustom}} >
                    </div>
                </div>

                <div ng-if="$form->NoiceEnabled==1">
                    <p><strong>NOISE: </strong></p>
                    <div ng-if="$form->NoiceEdit==0">
                        <p>Tenant shall not cause or allow any unreasonably loud noise or activity in the Premises that
                            might disturb the rights, comforts and conveniences of other
                            persons. No lounging or visiting will be allowed in the common areas. Furniture delivery and
                            removal will take place between 8:00 a.m. and 8:00 p.m.</p>

                    </div>
                    <div ng-if="$form->NoiceEdit==1" {{$form->NoiceCustom}} >
                    </div>
                </div>


                <div ng-if="$form->ParkingEnabled==1">
                    <p><strong>PARKING: </strong></p>
                    <div ng-if="$form->ParkingEdit==0">
                        <p>The Landlord agrees:</p>
                        <div ng-if="$form->Lease_Parking_allowed==1">
                            <strong>to provide the tenant parking</strong> with {{$form->Lease_Parking_space }}

                            spaces.

                            There shall be
                            <div ng-if="$form->Lease_Parking_fees==1">
                                a fee of {{$form->Lease_Parking_fees_amount }} paid

                                <span ng-if="$form->Lease_Parking_fees_period=='month' ">    per month </span>
                                <span ng-if="$form->Lease_Parking_fees_period=='oneTime' "> one (1) time basis for each space.  </span>


                            </div>
                            <div ng-if="$form->Lease_Parking_fees==0">
                                no fee
                            </div>


                        </div>
                        <div ng-if="$form->Lease_Parking_allowed==0">
                            <strong>not to provide the tenant parking</strong>

                        </div>

                    </div>
                    <div ng-if="$form->ParkingEdit==1" {{$form->ParkingCustom}} >
                    </div>
                </div>


                <div ng-if="$form->BalconesEnabled==1">
                    <p><strong>BALCONIES: </strong></p>
                    <div ng-if="$form->BalconesEdit==0">
                        <p>If a balcony is located on the Premises the Tenant shall not use for the purpose of storage,
                            drying clothes, cleaning rugs or grilling.</p>

                    </div>
                    <div ng-if="$form->BalconesEdit==1" {{$form->BalconesCustom}} >
                    </div>
                </div>


                <div ng-if="$form->DwellingEnabled==1">
                    <p><strong>DWELLING: </strong></p>
                    <div ng-if="$form->DwellingEdit==0">
                        <p>Tenant is only entitled to occupy the dwelling listed above. This Lease does not entitle the
                            Tenant to use of any area outside of the dwelling including, but not limited to, the attic,
                            basement or the garage without written permission from the Landlord. Tenant is not to paint
                            any
                            part of the apartment without prior written permission from the Landlord. </p>

                    </div>
                    <div ng-if="$form->DwellingEdit==1" {{$form->DwellingCustom}} >
                    </div>
                </div>


                <div ng-if="$form->Lease_AdditionalTerms.length > 0">
                    <p><strong>ADDITIONAL TERMS AND CONDITIONS: </strong></p>

                    <div {{$form->Lease_AdditionalTerms}} >
                    </div>
                </div>
            <!--
            <div ng-if="$form->LL_sign.length > 0">
                <p><strong>Landlord sign ( {{$form->LLSigned_date}} ): </strong></p>
                <img ng-src="{{ $form->LL_sign }}">
            </div>

            <div ng-if="$form->T_sign.length > 0">
                <p><strong>Tenant sign ( {{$form->TSigned_date }} ): </strong></p>
                <img ng-src="{{ $form->T_sign }}">
            </div>


            <div ng-if="$form->B_sign.length > 0">
                <p><strong>Broker sign ( {{$form->BSigned_date}}): </strong></p>
                <img ng-src="{{ $form->B_sign }}">
            </div>


            <div ng-if="$form->B_A_sign.length > 0">
                <p><strong>Additional broker sign ({{$form->B_ASigned_date}} ): </strong></p>
                <img ng-src="{{ $form->B_A_sign }}">
            </div>

            <div ng-if="$form->PM_sign.length > 0">
                <p><strong>Property manager sign ({{$form->PMSigned_date}}): </strong></p>
                <img ng-src="{{ $form->PM_sign }}">
            </div>

-->

                <h2> Signs </h2>

                <div class="row" style="    border-bottom: 1px solid black;">
                    <div class="col-sm-2">
                        Role
                    </div>
                    <div class="col-sm-3">
                        Name
                    </div>
                    <div class="col-sm-2">
                        Sign date
                    </div>
                    <div class="col-sm-5">
                        Sign
                    </div>


                </div>
                @foreach($AllSigns as $sign )
                    <div class="row" style="    border-bottom: 1px solid black;">
                        <div class="col-sm-2">
                            {{$sign['role']}}
                        </div>
                        <div class="col-sm-3">
                            {{$sign['Name']}}
                        </div>
                        <div class="col-sm-2">
                            {{$sign['date']}}
                        </div>
                        <div class="col-sm-5">
                            <img ng-src="{{$sign['sign']}}">
                        </div>


                    </div>

                @endforeach


            </div>
        </div>


        <!-- / main -->
    </div>
    </div>

@endsection



@section('afterScripts')


@endsection