@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Users list</h1>
            </div>
            <div class="wrapper-md">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables
                    </div>
                    <div class="table-responsive">
                        <table ui-jq="dataTable" class="table table-striped b-t b-b">
                            <thead>
                            <tr>
                                <th> Id</th>
                                <th>user</th>
                                <th>email</th>
                                <th>role</th>
                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user )
                                <tr>
                                    <td> {{$user->id}} </td>
                                    <td>{{$user->name}}</td>
                                    <td> {{$user->email}}  </td>
                                    <td> {{$user->role}}  </td>
                                    <td>
                                        <button class="btn btn-info btn-addon"><i class="fa fa-play"></i><a href="/admin/user/{{$user->id}}"> Edit </a></button>


                                        <button class="btn m-b-xs btn-sm btn-danger btn-addon deleteUser"
                                                data-uid="{{$user->id}}"><i class="fa fa-trash-o"></i>Delete
                                        </button>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>
        $('.deleteUser').click(function () {

            console.log($(this).data('uid'));
            var postData = {
                'uid': $(this).data('uid'),
                'action': 'delete'
            };
            $.ajax({
                method: "POST",
                url: "/api/users/delete",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })


    </script>

@endsection