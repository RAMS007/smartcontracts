@extends('layouts.admin')

@section('content')
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->


            <div class="bg-light lter b-b wrapper-md">
                <h1 class="m-n font-thin h3">Invoice list</h1>
            </div>
            <div class="wrapper-md">
          <!--      <button class="btn btn-info "><a
                            href="/admin/invoice/new"> New Invoice </a></button>

                <button class="btn btn-info " id="RunGenerator">Run Invoice Generator </button>
                -->

                <form class="form" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>Gas Price</label>
                        <input type="text" class="form-control" placeholder="Enter GAS price. If empty - we will calculate GAS price from API" value="{{$gasPrice}}" name="gasPrice">
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">Save</button>

                </form>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables
                    </div>
                    <div class="table-responsive">
                        <table ui-jq="dataTable" class="table table-striped b-t b-b">
                            <thead>
                            <tr>
                                <th> Id</th>
                                <th>Who generete invoice</th>
                                <th>Who will pay</th>
                                <th>Lease Id</th>
                                <th>PaidUntil</th>
                                <th>status</th>
                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($invoices as $invoice )
                                <tr>
                                    <td> {{$invoice->id}} </td>
                                    <td>{{$invoice->WhoGenerate}}</td>
                                    <td> {{$invoice->WhoPay}}  </td>
                                    <td> {{$invoice->leaseId}}  </td>
                                    <td> {{$invoice->paidUntil}}  </td>
                                    <td> {{$invoice->status}}  </td>

                                    <td>
                                        <button class="btn btn-info "><a
                                                    href="/admin/invoice/{{$invoice->id}}"> Edit </a></button>
                                        <button class="btn btn-info "><a
                                                    href="/invoice/{{$invoice->id}}" target="_blank"> Download PDF </a></button>

                                        <button class="btn btn-danger deleteUser"
                                                data-uid="{{$invoice->id}}"><i class="fa fa-trash-o"></i>Delete
                                        </button>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <!-- / main -->
        </div>
    </div>

@endsection



@section('afterScripts')
    <script>
        $('.deleteUser').click(function () {

            console.log($(this).data('uid'));
            var postData = {
                'id': $(this).data('uid'),
                'action': 'delete'
            };
            $.ajax({
                method: "POST",
                url: "/admin/invoice/delete",
                data: postData
            })
                    .done(function (msg) {
                        if (msg.error == false) {
                            toastr.success(msg.msg);
                        } else {
                            toastr.warning(msg.msg);
                        }
                    });
        })


        $('#RunGenerator').click(function () {

            $.ajax({
                method: "GET",
                url: "/api/invoice"
            })
                    .done(function (msg) {
                        window.location.href="/admin/invoices";
                    });


        })


    </script>

@endsection