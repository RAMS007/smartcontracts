// config

var app =  
angular.module('app')
  .config(
    [        '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
        
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive  = $compileProvider.directive;
        app.filter     = $filterProvider.register;
        app.factory    = $provide.factory;
        app.service    = $provide.service;
        app.constant   = $provide.constant;
        app.value      = $provide.value;
    }
  ])
  .config(['$translateProvider', function($translateProvider){
    // Register a loader for the static files
    // So, the module will search missing translation tables under the specified urls.
    // Those urls are [prefix][langKey][suffix].
    $translateProvider.useStaticFilesLoader({
      prefix: '/l10n/',
      suffix: '.js'
    });
    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
  }]);



// I define an asynchronous wrapper to the native prompt() method. It returns a
// promise that will be "resolved" if the user submits the prompt; or will be
// "rejected" if the user cancels the prompt.
app.factory(
    "prompt",
    function( $window, $q ) {
        // Define promise-based prompt() method.
        function prompt( message, defaultValue ) {
            var defer = $q.defer();
            // The native prompt will return null or a string.
            var response = $window.prompt( message, defaultValue );
            if ( response === null ) {
                defer.reject();
            } else {
                defer.resolve( response );
            }
            return( defer.promise );
        }
        return( prompt );
    }
);
