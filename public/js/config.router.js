'use strict';

/**
 * Config for the router
 */


var xhReq = new XMLHttpRequest();
xhReq.open("GET", "//" + window.location.hostname + "/api/csrf", false);
xhReq.send(null);

angular.module('app').constant("CSRF_TOKEN", xhReq.responseText);






angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams', 'AuthService', 'AUTH_EVENTS','Session','CSRF_TOKEN', '$http', '$location',
            function ($rootScope, $state, $stateParams, AuthService, AUTH_EVENTS, Session, CSRF_TOKEN , $http,$location ) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
                $http.defaults.headers.common['X-CSRF-TOKEN'] = CSRF_TOKEN;

                AuthService.profile().then(function (user, $state) {
                    console.log('promise ok first');
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                    //   $scope.setCurrentUser(user);
                }, function (r) {
                    //	console.log('fail promise');
                    //	console.log(r);
                    //	$scope.error = r;
                    $rootScope.postLogInRoute = $location.path();
                    console.log($state.is('RegisterFail'));
                    if ($state.is('RegisterFail') || $state.is('Register')) {

                    } else {
                        $state.go('Login');
                    }
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                });


                // change page title based on state
                $rootScope.$on('$stateChangeSuccess', function (event, toState) {
                    $rootScope.LoadCompleted = true;
                });

                $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
                    console.log('login success broadcast'); // ‘Some data’
                    console.log('check if we need redirect');
                    console.log($state.is('Login'));
                    console.log(angular.isDefined($rootScope.firstName));
                    if ($state.is('Login') && angular.isDefined($rootScope.firstName)) {
                        $state.go('Dashboard');
                    }
//	$state.go('Dashboard');
                });


                $rootScope.$on('$stateChangeStart', function (event, next) {
                    if (!angular.isDefined(Session.userRole)) {
                        AuthService.profile().then(function () {
                            console.log('promise ok check');
                            var authorizedRoles = next.data.authorizedRoles;
                            console.log(next);
                            console.log(authorizedRoles);
                            if (!AuthService.isAuthorized(authorizedRoles)) {
                                $rootScope.postLogInRoute = $location.path();
                                event.preventDefault();
                                if (AuthService.isAuthenticated()) {
                                    console.log('user is not allowed');
                                    $state.go('Dashboard');
                                    $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                                } else {
                                    console.log('user is not logged in');
                                    $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                                    $state.go('Login');
                                }
                            } else {
                                console.log(next.name);
                                $rootScope.postLogInRoute = null;
                            }

                        });


                    } else {

                        var authorizedRoles = next.data.authorizedRoles;
                        console.log(authorizedRoles);
                        console.log(next);
                        if (!AuthService.isAuthorized(authorizedRoles)) {
                            $rootScope.postLogInRoute = $location.path();
                            event.preventDefault();
                            if (AuthService.isAuthenticated()) {
                                console.log('user is not allowed');
                                $state.go('Dashboard');
                                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                            } else {
                                console.log('user is not logged in');
                                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
                                $state.go('Login');
                            }
                        }else{
                            $rootScope.postLogInRoute = null;
                        }
                    }
                });


            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', 'MODULE_CONFIG', '$locationProvider', 'USER_ROLES',
            function ($stateProvider, $urlRouterProvider, JQ_CONFIG, MODULE_CONFIG, $locationProvider, USER_ROLES) {

                $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                });

                var layout = "tpl/app.html";


                $urlRouterProvider
                    .otherwise('/dashboard');


                $stateProvider
                    .state('app', {
                        abstract: true,
                        url: '/app',
                        templateUrl: layout,
                        data: {
                            authorizedRoles: [ USER_ROLES.all]
                        }
                    })
                    .state('Dashboard', {
                        url: '/dashboard',
                        templateUrl: '/tpl/app_dashboard_v1.html',
                        controller:'DashboardController',
                        //                 resolve: load(['js/controllers/chart.js']),
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })
                    .state('Login', {
                        url: '/login',
                        templateUrl: '/tpl/page_signin.html',

                        data: {
                            authorizedRoles: [ USER_ROLES.all]
                        }
                    })
                    .state('Register', {
                        url: '/register',
                        templateUrl: '/tpl/page_signup.html',

                        data: {
                            authorizedRoles: [USER_ROLES.all]
                        }
                    })
                    .state('Logout', {
                        url: '/logout',
                        controller: 'LogoutCtrl as logout',
                        data: {
                            authorizedRoles: [ USER_ROLES.all]
                        }
                    })
                    .state('ForgotPwd', {
                        url: '/forgot_pwd',
                        templateUrl: '/tpl/page_forgotpwd.html',

                        data: {
                            authorizedRoles: [USER_ROLES.all]
                        }
                    })
                    .state('NewForm', {
                        url: '/new_form',
                        templateUrl: '/tpl/page_newForm.html',
                        controller:'NewFormController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })
                    .state('EditForm', {
                        url: '/edit_form/{formId}',
                        templateUrl: '/tpl/page_newForm.html',
                        controller:'NewFormController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })
                    .state('ShowForm', {
                        url: '/show_form/{formId}',
                        templateUrl: '/tpl/page_ShowForm.html',
                        controller:'ShowFormController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })

                    .state('ListForms', {
                        url: '/list_forms',
                        templateUrl: '/tpl/page_ListForms.html',
                        controller:'ListFormsCaontroller',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })

                    .state('AccountSettings', {
                        url: '/settings',
                        templateUrl: '/tpl/page_AccountSettings.html',
                        controller:'AccountSettingsController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })

                    .state('Invoices', {
                        url: '/invoices/:LeaseId?',
                        templateUrl: '/tpl/page_Invoices.html',
                        controller:'InvoicesController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })
                    .state('PaymentError', {
                        url: '/paymentError',
                        templateUrl: '/tpl/page_PaymentError.html',
                        controller:'PaymentErrorController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })


                    .state('ContactUs', {
                        url: '/ContactUs',
                        templateUrl: '/tpl/page_contactUs.html',
                        controller:'ContactUsController',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })



                    .state('PaymentPage', {
                        url: '/PaymentPage/{invoiceId}',
                        controller: 'PaymentPageController',
                        templateUrl: '/tpl/PaymentPage.html',
                        data: {
                            authorizedRoles: [USER_ROLES.users, USER_ROLES.admin]
                        }
                    })





                ;

                function load(srcs, callback) {
                    return {
                        deps: ['$ocLazyLoad', '$q',
                            function ($ocLazyLoad, $q) {
                                var deferred = $q.defer();
                                var promise = false;
                                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                if (!promise) {
                                    promise = deferred.promise;
                                }
                                angular.forEach(srcs, function (src) {
                                    promise = promise.then(function () {
                                        if (JQ_CONFIG[src]) {
                                            return $ocLazyLoad.load(JQ_CONFIG[src]);
                                        }
                                        angular.forEach(MODULE_CONFIG, function (module) {
                                            if (module.name == src) {
                                                name = module.name;
                                            } else {
                                                name = src;
                                            }
                                        });
                                        return $ocLazyLoad.load(name);
                                    });
                                });
                                deferred.resolve();
                                return callback ? promise.then(function () {
                                    return callback();
                                }) : promise;
                            }]
                    }
                }


            }
        ]
    );
