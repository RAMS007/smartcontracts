app.directive('dateInput', function(){
    return {
        restrict : 'A',
        scope : {
            ngModel : '='
        },
        link: function (scope) {
            console.log('|||||');
            if (scope.ngModel) scope.ngModel = new Date(scope.ngModel);
        }
    }
});