'use strict';


angular.module('app')
    .service('AuthService', ['$http', 'Session', 'USER_ROLES', '$q', '$rootScope', '$state', '$timeout', function ($http, Session, USER_ROLES, $q, $rootScope, $state, $timeout) {


        this.login = function (credentials) {
            return $http
                .post('/api/login', credentials)
                .then(function (res) {
                    console.log('auth ok');
                    if (res.data.status == 'fail') {
                        return $q.reject(res.data.msg);
                    } else {
                        Session.create(res.data.id, res.data.user.id,
                            res.data.user.role);
                        return res.data.user;
                    }
                }, function errorCallback(response) {
                    console.log('auth err');
                    console.log(response);
                    return $q.reject('Error  response code:' + response.status + response.statusText);

                });
        };

        this.register = function (credentials) {
            return $http
                .post('/api/register', credentials)
                .then(function (res) {
                    console.log('reg ok');
                    if (res.data.status == 'fail') {
                        return $q.reject(res.data.msg);
                    } else {
                        return res.data;
                    }
                }, function errorCallback(response) {
                    console.log('reg err');
                    console.log(response);
                    return $q.reject('Error  response code:' + response.status + response.statusText);

                });
        };

        this.Recover = function (email) {
            var data = {'email': email};
            return $http
                .post('password/email', data)
                .then(function (res) {
                    console.log('reg ok');
                    console.log(res);
                    if (res.data.status == 'fail') {
                        return $q.reject(res.data.msg);
                    } else {
                        return res.data.url;
                    }
                }, function errorCallback(response) {
                    console.log('reg err');
                    console.log(response);
                    return $q.reject('Error  response code:' + response.status + response.statusText);

                });
        };


        this.profile = function () {
            return $http
                .get('/api/profile')
                .then(function (res) {
                    console.log('first ok');
                    if (res.data.user == undefined) {
                        return $q.reject('not logined');
                    }
                    if (res.data.user.id == 0) {
                        return $q.reject('not logined');
                    } else {
                        console.log(res.data.user.id);
                        Session.create(res.data.id, res.data.user.id,
                            res.data.user.role);
                        $rootScope.user = {};
                        $rootScope.firstName = res.data.user.firstName;
                        $rootScope.lastName = res.data.user.lastName;
                        $rootScope.email = res.data.user.email;
                        $rootScope.user = res.data.user;
                        $rootScope.countWarnings = res.data.countWarnings;
                        $rootScope.role = res.data.user.role;
                        $rootScope.AllRoles=res.data.AllRoles;
                        $rootScope.newRole =res.data.user.role;
                        if (res.data.user.role == 'admin') {
                            $rootScope.admin = true;
                        } else {
                            $rootScope.admin = false;
                        }
                        return res.data.user;
                    }

                }, function errorCallback(response) {
                    return $q.reject('fail');
                });
        };


        this.isAuthenticated = function () {
            return !!Session.userId;
        };

        this.isAuthorized = function (authorizedRoles) {
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }
            var roleAllowed = false;
            for (var i = 0; i < authorizedRoles.length; i++) {

                console.log(authorizedRoles[i]);

                if (angular.isArray(authorizedRoles[i])) {

                    if (authorizedRoles[i].indexOf(Session.userRole) !== -1) {
                        roleAllowed = true;
                    }
                } else {
                    if (authorizedRoles[i] == Session.userRole) {
                        roleAllowed = true;
                    }
                }


            }

            console.log(authorizedRoles);
            console.log(roleAllowed);
            return ((this.isAuthenticated() &&
            roleAllowed) || authorizedRoles.indexOf(USER_ROLES.all) !== -1);
        };


    }]);