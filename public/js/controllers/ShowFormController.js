'use strict';

/* Controllers */
// signin controller
app.controller('ShowFormController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', '$stateParams', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $stateParams) {



    if ($rootScope.user == undefined) {
        AuthService.profile().then(function () {
            if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
                toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
                $state.go('AccountSettings');
            }
        }, function (r) {

        })
    } else {
        if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
            toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
            $state.go('AccountSettings');
        }
    }













    $scope.formId = $stateParams.formId;
$scope.isformNotSigned=true;
$scope.leadTenant={};
    $scope.AllTenants=[];
    $scope.AllBrokers=[];


    $scope.getOneForm = function (formId) {
        var url = '/api/Forms/getOne';
        var postData = {'formId': formId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = true;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            $rootScope.LoaderEnabled = false;
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);

                $scope.form = data.form;
                $scope.FormInitiator = data.FormInitiator;

                $scope.leadTenant=data.leadTenant;
                $scope.AllTenants=data.AllTenants;
                $scope.AllBrokers=data.AllBrokers;

                $scope.AllComments = data.AllComments;
                $scope.userCommented=data.userCommented;
                $scope.signedByMe=data.signedByMe;


                if ($rootScope.user.role == 'landlord') {
                    $scope.form.signature = {};
                    $scope.form.signature.dataUrl = data.form.LL_sign;
                }

                $scope.form.Id = formId;


                if ($scope.form.customLease_file == null || $scope.form.customLease_file == undefined) {

                    $scope.form.UploadLease = 0;
                } else {
                    $scope.form.UploadLease = 1;

                }


                $scope.form.LLSigned_date  = new Date(data.form.LLSigned_date) ;
                $scope.NotSigned();
                /*      if($scope.form.LLSigned_date=undefined){

                 $scope.form.LLSigned_date = $filter('date')(Date.now(), 'yyyy-MM-dd');
                 }
                 */

            } else {
                toastr.warning(data.msg);
            }
        });


    }


    if ($scope.formId != undefined) {
        $scope.getOneForm($scope.formId);
    }


    $scope.rejectForm = function (form) {

        var url = '/api/Forms/reject';
        var postData = {'formId': form.id};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    };


    $scope.approveForm = function (form) {


        //    $scope.form.signature = accept();

        console.log($scope.form.signature);
        if ($scope.form.signature == undefined || $scope.form.signature.dataUrl == undefined) {
            toastr.warning('Please sign the document');
            return false;
        }


        var url = '/api/Forms/approve';
        var postData = {'form': form};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    }

    $scope.SaveForm = function (form) {

        var url = '/api/Forms/saveWithComment';
        var postData = {'form': form};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    }


    $scope.NotSigned = function () {

console.log('check signed');
        if( ($scope.form != undefined)    &&    ($scope.form.AllSigns != undefined)) {


            if ($scope.form.AllSigns.length > 0) {


                angular.forEach($scope.form.AllSigns, function (value) {

                    switch ($rootScope.user.role) {

                        case 'landlord':
                            if(value.sign !=null) {
                                if (value.role == 'Landlord' && value.sign.length > 10) {
                                    $scope.isformNotSigned = false;

                                }
                            }
                            break;


                        case 'tenant':
                            if(value.sign !=null) {
                                if (value.role == 'Lead Tenant' && value.sign.length > 10) {
                                    $scope.isformNotSigned = false;

                                }
                                if (value.role == 'Tenant' && value.sign.length > 10) {
                                    $scope.isformNotSigned = false;

                                }

                            }
                            break;

                        case 'admin':
                            $scope.isformNotSigned= true;
                            break;

                        case 'broker':
                            if(value.sign !=null) {
                                if (value.role == 'Broker' && value.sign.length > 10) {
                                    $scope.isformNotSigned = false;

                                }
                            }
                            break;

                        case 'property_manager':
                            if(value.sign !=null) {
                                if (value.role == 'Property Manager' && value.sign.length > 10) {
                                    $scope.isformNotSigned = false;

                                }
                            }
                            break;


                    }


                })


             //   return true;

            }
        }

    }


}]);
