'use strict';

/* Controllers */
// signin controller
app.controller('NewFormController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', '$stateParams', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $stateParams) {



    if ($rootScope.user == undefined) {
        AuthService.profile().then(function () {
            if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
                toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
                $state.go('AccountSettings');
            }
        }, function (r) {

        })
    } else {
        if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
            toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
            $state.go('AccountSettings');
        }
    }










    $scope.formId = $stateParams.formId;
    $scope.AvaliableTemplates = [];
    $scope.CreatedLeaseId = 0;
    $scope.ShowPropertyAddress = false;
    $scope.AdditionalOccupantsArray = [];
    $scope.AdditionalBrokersArray = [];
    $scope.AdditionalBrokers_number = 0;
    $scope.occupant = {};
    $scope.form = {
        Lease_Parking_allowed: 1,
        AdditionalOccupants: 0,
        Lease_Parking_fees: 1,
        Lease_Parking_fees_period: 'month',
        PropertyManager: 0,
        RealEstate: 0,
        GoverningLawEnabled: 1,
        GoverningLawEdit: 0,
        LeasePaymentsEnabled: 1,
        LeasePaymentsEdit: 0,
        LateChargesEnabled: 1,
        LateChargesEdit: 0,
        InsufficientFundsEnabled: 1,
        InsufficientFundsEdit: 0,
        SecurityDepositEnabled: 1,
        SecurityDepositEdit: 0,
        DefaultsEnabled: 1,
        DefaultsEdit: 0,
        QuietEnjoymentEnabled: 1,
        QuietEnjoymentEdit: 0,
        PosessionEnabled: 1,
        PosessionEdit: 0,
        PremisessEnabled: 1,
        PremisessEdit: 0,
        OccupantsEnabled: 1,
        OccupantsEdit: 0,
        VisitorsEnabled: 1,
        VisitorsEdit: 0,
        ConditionOfPremissesEnabled: 1,
        ConditionOfPremissesEdit: 0,
        AssigmentEnabled: 1,
        AssigmentEdit: 0,
        DangerousMaterialsEnabled: 1,
        DangerousMaterialsEdit: 0,
        UtilitesEnabled: 1,
        UtilitesEdit: 0,
        ApplianceIncludedEnabled: 1,
        ApplianceIncludedEdit: 0,
        PetsEnabled: 1,
        PetsEdit: 0,
        AlterationsEnabled: 1,
        AlterationsEdit: 0,
        DamagesEnabled: 1,
        DamagesEdit: 0,
        MaintanceAndRepairEnabled: 1,
        MaintanceAndRepairEdit: 0,
        RightOfInspectionEnabled: 1,
        RightOfInspectionEdit: 0,
        AbandonmentEnabled: 1,
        AbandonmentEdit: 0,
        ExtendedAbsensesEnabled: 1,
        ExtendedAbsensesEdit: 0,
        SecurityEnabled: 1,
        SecurityEdit: 0,
        SeverabilityEnabled: 1,
        SeverabilityEdit: 0,
        InsuranceEnabled: 1,
        InsuranceEdit: 0,
        BindingEffectEnabled: 1,
        BindingEffectEdit: 0,
        EntireAgrementEnabled: 1,
        EntireAgrementEdit: 0,
        NoticeEnabled: 1,
        NoticeEdit: 0,
        CumulativeRightsEnabled: 1,
        CumulativeRightsEdit: 0,
        WaiverEnabled: 1,
        WaiverEdit: 0,
        IndemnificationEnabled: 1,
        IndemnificationEdit: 0,
        AttorneyFeesEnabled: 1,
        AttorneyFeesEdit: 0,
        DisplaySignsEnabled: 1,
        DisplaySignsEdit: 0,
        NoiceEnabled: 1,
        NoiceEdit: 0,
        ParkingEnabled: 1,
        ParkingEdit: 0,
        BalconesEnabled: 1,
        BalconesEdit: 0,
        DwellingEnabled: 1,
        DwellingEdit: 0,
        Lease_Service_Electricity:"Tenant",
        Lease_Service_Telephone:"Tenant",
        Lease_Service_TV:"Tenant",
        Lease_Service_Heat:"Tenant",
        Lease_Service_HotWater:"Tenant",
        Lease_Service_Water:"Tenant",
        Lease_Service_Garbage:"Tenant",
        Lease_Service_SnowRemoval:"Tenant",
        Lease_Service_Landscaping:"Tenant",
        Lease_Included_Stove:"Yes",
        Lease_Included_Refrigerator:"Yes",
        Lease_Included_Dishwasher:"Yes",
        Lease_Included_WachingMachine:"Yes",
        Lease_Included_Dryer:"Yes",
        Lease_Included_GarbageDisposal:"Yes",
        Lease_Included_Microwawe:"Yes",
        Lease_Included_Toaster:"Yes",
        Lease_Included_AirConditioner:"Yes"

    };

    $scope.form.GoverningLawCustom = 'This Lease shall be governed by and construed in accordance with the laws of the    State    of _____ .';
    $scope.form.LateChargesCustom = ' Rent is due on the ___ of each month.    If any or all of the rent is not received by the    ___ of the month,        $ ___ per day will be charged as late    fees    until full rental payment is received. If rent is not received by the    __ of the    month, Tenant will be considered    in breach of the Lease Agreement and eviction proceedings will be    initiated.';
    ;
    $scope.form.InsufficientFundsCustom = '   Tenant agrees to pay a charge of    $ ___ for        each check given by Tenant to Landlord that is returned to Landlord for        lack of        sufficient funds (NSF).';
    $scope.form.PosessionCustom = '  Tenant shall be entitled to possession of the Premises on the    _____    day of the Lease Term. At the expiration of the Lease, Tenant shall    peaceably    surrender    the Premises to the Landlord or Landlord’s agent in good condition, as    it was at    the    commencement of the Lease, reasonable wear and tear excepted.';
    $scope.form.PremisessCustom = '    Tenant shall only use the Premises as a residence. The Premises shall not    be used    to    carry on any type of business or trade without prior written consent of    the    Landlord.        Tenant will comply with all laws, rules, ordinances, statutes and orders    regarding    the    use of the Premises.';
    $scope.form.OccupantsCustom = 'Tenant agrees that no more than ______    persons    may reside on the Premises without prior written consent of the    Landlord.';
    $scope.form.VisitorsCustom = '     Tenant shall be allowed to have Visitors on the Premises for a period of    ______ days    without    prior    written consent of the Landlord.';
    $scope.form.ConditionOfPremissesCustom = '    Tenant or Tenant’s agent have inspected the Premises, the fixtures, the    grounds,        building    and improvements and acknowledges that the Premises are in good and    acceptable    condition    and are habitable. If at any time during the term of this Lease, in    Tenant’s    opinion,        the conditions change, Tenant shall promptly provide reasonable notice    to    Landlord.';
    $scope.form.AssigmentCustom = '                  Tenant shall not assign or sublease any interest in this Lease without    prior    written    consent of the Landlord, which consent shall not be unreasonably    withheld.        Any assignment or sublease without Landlord’s written prior consent    shall, at    Landlord’s    option, terminate this Lease.    ';
    $scope.form.DangerousMaterialsCustom = '       Tenant shall not keep or have on or around the Premises any item of a    dangerous,        flammable or explosive nature that might unreasonably increase the risk    of fire    or    explosion on or around the Premises or that might be considered    hazardous by any    responsible insurance company.';
    $scope.form.UtilitesCustom = '  The following utilities and services will be the responsibility of the    following:            Electricity - _________    Telephone Service - ________    Cable (TV) - ________    Heat - ________    Hot Water - ____________    Water - _________    Garbage/Trash -_________    Snow Removal -_________    Landscaping -________';
    $scope.form.ApplianceIncludedCustom = '    The following appliances will be included with the rental of the    property, enter    yes    or    no for each item included:        Landlord is responsible for repair s to appliances listed above unless    otherwise    stated    here : Tenant is responsible for the 1st $100.00 of any appliance    repair    Stove:___    Refrigerator :___    Dishwasher :____    Washing Machine:_____    Dryer:_____    Garbage Disposal :_____    Microwave:_____    Toaster :______    Air Conditioning Units :______    ';
    $scope.form.PetsCustom = '  The Landlord agrees that the Tenant:    shall be allowed to keep a maximum of ____    pets on the property. The type of pets    allowed shall consist of ______    In addition to the security deposit,        there shall be a pet deposit of $_____    that will be refundable    If refundable, the deposit will be returned at the end of the lease term    if        there        is no damage due to the pet(s).';
    $scope.form.AlterationsCustom = '  Tenant agrees not to make any improvements or alterations to the    Premises    without    prior    written consent of the Landlord. If any alterations, improvement or    changes    are    made    to    or built on or around the Premises, with the exception of fixtures    and    personal    property    that can be removed without damage to the Premises, they shall    become the    property    of    Landlord and shall remain at the expiration of the Lease, unless    otherwise    agreed in    writing.';
    $scope.form.DamagesCustom = ' If the Premises or part of the Premises are damaged or destroyed by    fire or    other    casualty not due to Tenant’s negligence, the rent will be abated    during the    time    that    the Premises are uninhabitable. If Landlord decides not to repair or    rebuild    the    Premises, then this Lease shall terminate and the rent shall be    prorated up    to    the    time    of the damage. Any unearned rent paid in advance shall be refunded    to    Tenant.';
    $scope.form.MaintanceAndRepairCustom = '  Tenant will, at Tenant’s sole expense, keep and maintain the Premises            in            good,            clean    and    sanitary condition and repair during the term of this Lease and any    renewal    thereof.        Tenant shall be responsible to make all repairs to the Premises,        fixtures,        appliances    and equipment therein that may have been damaged by Tenant’s misuse,        waste    or    neglect,        or that of the Tenant’s family, agents or visitors. Tenant agrees    that no    painting    will    be done on or about the Premises without the prior written consent    of    Landlord.        Tenant    shall promptly notify Landlord of any damage, defect or destruction    of the    Premises    or    in the event of the failure of any of the appliances or equipment.        Landlord    will    use    its    best efforts to repair or replace any such damaged or defective    areas,        appliances or    equipment.';
    $scope.form.RightOfInspectionCustom = '  Tenant agrees to make the Premises available to Landlord or    Landlord’s agents    for        the        purposes of inspection, making repairs or improvements, or to supply    agreed    services    or    show the premises to prospective buyers or tenants, or in case of    emergency.        Except    in    case of emergency, Landlord shall give Tenant reasonable notice of    intent to    enter.        For    these purposes, the minimum statutory notice allowed or <strong>twenty-four    (24)    hours</strong>, whichever is less, shall be deemed reasonable.                Tenant shall not, without Landlord’s prior written consent, add,        alter or    re-key    any    locks to the Premises. At all times Landlord shall be provided with        a key or    keys    capable of unlocking all such locks and gaining entry. Tenant    further agrees    to    notify    Landlord in writing if Tenant installs any burglar alarm system,        including    instructions    on how to disarm it in case of emergency entry.';
    $scope.form.AbandonmentCustom = '      If Tenant abandons the Premises of any personal property during the    term of    this    Lease,        Landlord may, at their option, enter the Premises by any legal means    without    liability    to Tenant and may at Landlord’s option terminate the Lease.        Abandonment is    defined    as    absence of the Tenant from the Premises for at least    ___    consecutive days without    notice to Landlord.        If Tenant abandons the Premises while the rent is outstanding for        more than    15    days    and    there is not reasonable evidence, other than the presence of the    Tenants’                                        personal    property, that the Tenant is occupying the unit, Landlord may at    Landlord’s    option    terminate this Lease Agreement and regain possession in the manner    prescribed by    law.        Landlord will dispose of all abandoned personal property on the    Premises in    any    manner    allowed by law.';
    $scope.form.ExtendedAbsensesCustom = '      In the event Tenant will be away from the Premises for more than the    abandonment    period,        Tenant agrees to notify Landlord in writing of such absence. During    such    absence,        Landlord may enter the premises at times reasonable necessary to    maintain    the    property    and inspect for damages and needed repairs.';
    $scope.form.SeverabilityCustom = '     If any part of this Lease shall be held unenforceable for any reason,    the    remainder    of    this Agreement shall continue in full force and effect. If any court    of    competent    jurisdiction deems any provision of this Lease invalid or    unenforceable, and    if        limiting        such provision would make the provision valid, then such provision    shall be    deemed    to be    construed as so limited.';
    $scope.form.InsuranceCustom = '        Landlord and Tenant shall each be responsible to maintain appropriate    insurance    for        their respective interests in the Premises and property located on    the    Premises.        Tenant understands that Landlord will not provide any insurance    coverage for        Tenant’s    property. Landlord will not be responsible for any loss of Tenant’s    property,        whether by    theft, fire, riots, strikes, acts of God or otherwise. Landlord    encourages    Tenant to    obtain renter’s insurance or other similar coverage to protect    against risk    of    loss.';
    $scope.form.BindingEffectCustom = '   The covenants and conditions contained in the Lease shall apply to    the    Parties    and    the    heirs, legal representatives, successors and permitted assigns of    the    Parties.';
    $scope.form.EntireAgrementCustom = '       This Lease constitutes the entire Agreement between the Parties and    supersedes    any    prior    understanding or representation of any kind preceding the date of    this    Agreement.        There    are no other promises, conditions, understandings or other    Agreements,        whether    oral    or    written, relating to the subject matter of this Lease.        This Lease may be modified in writing and must be signed by both    Landlord and    Tenant.';
    $scope.form.NoticeCustom = '   Any notice required or otherwise given pursuant to this Lease shall    be in    writing    and    mailed certified return receipt requested, postage prepaid, or    delivered by    overnight    delivery service, if to Tenant, the address of the Premises, and if        to        Landlord,            the    following address ____ City of    _____ ,        State of    _______,        Zip Code ______.        Either party may change such addresses    from time to time by providing notice as set forth above.';
    $scope.form.CumulativeRightsCustom = '   Landlord’s and Tenant’s rights under this Lease are cumulative and    shall not    be    construed    as exclusive of each other unless otherwise required by law.';
    $scope.form.WaiverCustom = '    The failure of either Party to enforce any provisions of the Lease    shall not    be    deemed a    waiver of limitation of that Party’s right to subsequently enforce    and    compel    strict    compliance with every provision of this Lease. The acceptance of    rent by    Landlord    does    not waive Landlord’s right to enforce any provisions of this    Lease.';
    $scope.form.IndemnificationCustom = ' To the extent permitted by law, Tenant will indemnify and hold    Landlord and    Landlord’s    property, including the Premises, free and harmless from any    liability for        losses,            claims, injury to or death of any person, including Tenant, or for        damage to    property    arising from Tenant using and occupying the Premises or from the    acts or    omissions    of    any person or persons, including Tenant, in or about the Premises    with        Tenant’s    express    or implied consent except Landlord’s act or negligence.';
    $scope.form.AttorneyFeesCustom = '    In the event that the Tenant violates the terms of the Lease or    defaults in    the    performance of any covenants in the Lease and the Landlord engages    an    attorney    or    institutes a legal action, counterclaim, or summary proceeding    against    Tenant    based    upon    such violation or default, Tenant shall be liable to Landlord for        the costs    and    expenses    incurred in enforcing this Lease, including reasonable attorney fees    and    costs.        In    the    event the Tenant brings any action against the Landlord pursuant to    this    Lease    and    the    Landlord prevails, Tenant shall be liable to Landlord for costs and    expenses    of    defending such action, including reasonable attorney fees and    costs.';
    $scope.form.DisplaySignsCustom = ' Landlord or Landlord’s agent may display “For Sale” or “For Rent” or                                        “Vacancy” or    similar    signs on or about the Premises and enter to show the Premises to    prospective    tenants    during the last sixty (60) days of this Lease. Tenant agrees that no    signs    shall    be    placed on the Premises without the prior written consent of the    Landlord.';
    $scope.form.NoiceCustom = '   Tenant shall not cause or allow any unreasonably loud noise or    activity in    the    Premises    that might disturb the rights, comforts and conveniences of other    persons.        No    lounging    or visiting will be allowed in the common areas. Furniture delivery    and    removal    will    take place between 8:00 a.m. and 8:00 p.m.';
    $scope.form.ParkingCustom = '   The Landlord agrees:    to provide the tenant parking with ___ spaces.        There shall be    a fee of $____ per month';
    $scope.form.BalconesCustom = '   If a balcony is located on the Premises the Tenant shall not use    for the        purpose    of    storage, drying clothes, cleaning rugs or grilling.';
    $scope.form.DwellingCustom = '   Tenant is only entitled to occupy the dwelling listed above. This    Lease    does    not    entitle    the Tenant to use of any area outside of the dwelling including,        but not    limited    to,        the    attic, basement or the garage without written permission from    the    Landlord.        Tenant    is    not to paint any part of the apartment without prior written    permission    from    the    Landlord.';
    $scope.form.LeasePaymentsCustom = 'Tenant agrees to pay Landlord for use of the Premises in the amount of    $ ____ each month in advance on the    _____ day of with        payment to be made by _____.    If the Lease Term does not start on the    _____    day of the month or end on the last day of a month, the first and last    month’s rent    will be prorated accordingly.';
    $scope.form.SecurityDepositCustom = '          At the signing of this Lease, Tenant shall deposit with Landlord, in    trust, a    security    deposit of $ ___ as security    for        the performance by Tenant of the terms under this Lease and for any        damages    caused    by    Tenant, Tenant’s family, agents and visitors to the Premises during the    term of    this    Lease. Landlord may use part or all of the security deposit to repair    any damage    to    the    Premises caused by Tenant, Tenant’s family, agents and visitors to the    Premises.        However, Landlord is not limited to the security deposit amount and    Tenant    remains    liable    for any balance. Tenant shall not apply or deduct any portion of any    security    deposit    from the last or any month’s rent. Tenant shall not use or apply any    such    security    deposit at any time in lieu of payment of rent. If Tenant breaches any    terms or    conditions of this Lease, Tenant shall forfeit any deposit, as permitted    by    law.';
    $scope.form.DefaultsCustom = ' If Tenant fails to perform or fulfill any obligation under this Lease,    Tenant    shall    be in    default of this Lease. Subject to any statute ordinance or law to the    contrary,        Tenant    shall have the State minimum requirement or seven (7) days, whichever is    less,        from    the    date of notice of default by Landlord to cure the default. In the event    Tenant    does    not    cure a default, Landlord may at Landlord’s option:        a) cure such default and the cost of such action may be    added to    Tenant’s financial obligations under this Lease; or    b) declare Tenant in default of the Lease. In the event    of    default,    Landlord may also, as permitted by law, re-enter the Premises and    re-take    possession    of    the Premises.        Landlord may, at its option, hold Tenant liable for any difference    between the    rent    that would have been payable under this Lease during the balance of the    unexpired    term, if this Lease had continued in force and any rent paid by any    successive    Tenant    if the Premises are re-let. In the event Landlord is unable to re-let    the    Premises    during any remaining term of this Lease, after default by Tenant,        Landlord may    at    its    option hold Tenant liable for the balance of the unpaid rent under this    Lease if        this        Lease had continued in force. The failure of Tenants or their guests or    invitees    to    comply with any term of this Agreement is grounds for termination of the        tenancy,    with appropriate notice to Tenants and procedures as required by    law.';

    $scope.form.QuietEnjoymentCustom = '   Tenant shall be entitled to quiet enjoyment of the Premises and Landlord    will not    interfere with that right, as long as Tenant pays the rent in a timely    manner    and    performs all other obligations under this Lease.';
    $scope.form.SecurityCustom = ' Tenant understands that Landlord does not provide any security alarm    system    or    other    security for Tenant or the Premises. In the event any alarm system    is    provided,        Tenant    understands that such alarm system is not warranted to be complete    in all    respects    or to    be sufficient to protect Tenant on the Premises. Tenant releases    Landlord    from    any    loss,        damage, claim or injury resulting from the failure of any alarm    system,        security    or    from    the lack of any alarm system or security.';

    $scope.form.AdditionalOccupants_number = "0";
    $scope.form.property = {};
    $scope.form.property.country = 'US';

    $scope.getAvaliableTemplates = function () {


        $scope.error = false;
        var url = '/api/getTemplates';

        $http.get(url).then(function (res) {

            return res.data;
        }, function errorCallback() {

            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);

            if (data.error == false) {

                $scope.AvaliableTemplates = data.AvaliableTemplates;


            } else {
                toastr.warning(data.msg);
            }


        });


    }
    $scope.getAvaliableTemplates();


    $scope.SaveForm = function (form) {

        if (form.signature == undefined || form.signature.dataUrl == undefined) {
            toastr.warning('Please Sign this form');
            return false;
        }

        if($scope.AdditionalOccupantsArray.length ==0){
            toastr.warning('Please add at least 1 tenant');
            return false;
        }


        $rootScope.LoaderEnabled = true;
        $scope.error = false;
        var url = '/api/newForm';

        var foundLead = false;
        angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
            console.log(value);
            if (value.isLead == true) {
                foundLead = true;
            }
        });
        console.log(foundLead);
        if (!foundLead) {
            $scope.AdditionalOccupantsArray[0].isLead = true;
        }
        console.log( $scope.AdditionalOccupantsArray);



        form.AdditionalOccupantsArray = $scope.AdditionalOccupantsArray;
        form.AdditionalBrokersArray = $scope.AdditionalBrokersArray;

        $http.post(url, form).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);

            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }


        });
    };

    $scope.SelectFromTemplate = function (form) {
        $rootScope.LoaderEnabled = true;
        $scope.error = false;
        form.TemplateId = $scope.SelectedTemplate;
        var url = '/api/newForm/fromTemplate';

        if($scope.AdditionalOccupantsArray.length ==0){
            toastr.warning('Please add at least 1 tenant');
            $rootScope.LoaderEnabled = false;

            return false;
        }


        $rootScope.LoaderEnabled = true;
        $scope.error = false;


        var foundLead = false;
        angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
            console.log(value);
            if (value.isLead == true) {
                foundLead = true;
            }
        });
        console.log(foundLead);
        if (!foundLead) {
            $scope.AdditionalOccupantsArray[0].isLead = true;
        }
        console.log( $scope.AdditionalOccupantsArray);



        form.AdditionalOccupantsArray = $scope.AdditionalOccupantsArray;
        form.AdditionalBrokersArray = $scope.AdditionalBrokersArray;



        $http.post(url, form).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);

            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }


        });
    };


    $scope.getOneForm = function (formId) {
        var url = '/api/Forms/getOne';
        var postData = {'formId': formId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = true;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            $rootScope.LoaderEnabled = false;
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);

                $scope.form = data.form;

                if ($rootScope.user.role == 'landlord') {
                    $scope.form.signature = {};
                    $scope.form.signature.dataUrl = data.form.LL_sign;
                }

                //               $scope.form.endDate = new Date($scope.form.endDate);
                //               $scope.form.endTime = new Date($scope.form.endTime);
                //               $scope.form.startDate = new Date($scope.form.startDate);
                //         $scope.form.startTime = new Date($scope.form.startTime);

                //           $scope.form.dated = new Date($scope.form.dated);


                $scope.form.Id = formId;

            } else {
                toastr.warning(data.msg);
            }
        });


    }


    $scope.UploadLease = function (form) {
        console.log('click');
        $rootScope.LoaderEnabled = true;
        var file = $rootScope.CustomLease;

        console.log('file is ');
        console.dir(file);

        var uploadUrl = "/api/newForm";

        var fd = new FormData();
        fd.append('file', file);
        fd.append('UploadLease', form.UploadLease);
        //    fd.append('form', JSON.stringify(form));
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (res) {
            return res.data;
        }, function errorCallback(response) {
            console.log('err');
            $scope.error = 'error with your request';
            return $q.reject('Error');
        }).then(function (params) {
            console.log('params');
            console.log(params);
            if (params.error == true) {
                toastr.warning(params.msg);
                $rootScope.LoaderEnabled = false;
            } else {
                toastr.success('File procesed succesfull');
                toastr.success(params.msg);
                $scope.CreatedLeaseId = params.leaseId;
                $scope.ShowPropertyAddress = true;
                $rootScope.LoaderEnabled = false;
            }
        })
    }


    if ($scope.formId != undefined) {

        $scope.getOneForm($scope.formId);

    }


    $scope.$watch('SelectedTemplate', function ($val) {
        console.log($val);
        if ($val > 0) {
            $scope.ShowPropertyAddress = true;

        }
    });



    $scope.$watch('form.UploadLease', function ($val) {
        console.log($val);
        if ($val == 0) {
            $scope.ShowPropertyAddress = true;

        } else {
            if ($val == 1) {
                if ($scope.CreatedLeaseId == 0) {
                    $scope.ShowPropertyAddress = false;
                } else {
                    $scope.ShowPropertyAddress = true;
                }
            }

            if ($val == 2) {

                if ($scope.SelectedTemplate == undefined) {
                    $scope.ShowPropertyAddress = false;
                } else {
                    $scope.ShowPropertyAddress = true;
                }


            }


        }
    });

    /**
     * append property address to uploaded lease
     */
    $scope.savePropertyToLease = function () {

        $scope.error = false;
        var url = '/api/newForm/toUploaded';
        if ($scope.form.property == undefined) {
            toastr.warning('Please Fill Out All The Property Fields');
            return false;
        }
        if ($scope.AdditionalOccupantsArray[0] == undefined) {
            toastr.warning('Please add at least 1 occupant');
            return false;
        }
        $rootScope.LoaderEnabled = true;
        var foundLead = false;
        angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
            console.log(value);
            if (value.isLead == true) {
                foundLead = true;
            }
        });
console.log(foundLead);
        if (!foundLead) {
            $scope.AdditionalOccupantsArray[0].isLead = true;
        }
        console.log( $scope.AdditionalOccupantsArray);
        $scope.form.AdditionalOccupantsArray = $scope.AdditionalOccupantsArray;
        $scope.form.AdditionalBrokersArray = $scope.AdditionalBrokersArray;
        var postData = {
            'property': $scope.form.property,
            'leaseId': $scope.CreatedLeaseId,
            'moveInDate': $scope.form.moveInDate,
            'monthAmount': $scope.form.Lease_monthAmount,
            'LateFee': $scope.form.Lease_payFees,
            'form': $scope.form
        }
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);

            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });
    }

    $scope.AddOccupant = function () {
        if ($scope.occupant.AdditionalOccupants_email == undefined) {
            toastr.warning('Email Required');
            return false;
        }
        if ($scope.AdditionalOccupantsArray.length == 0) {
            $scope.AdditionalOccupantsArray.push({
                'AdditionalOccupants_FirstName': $scope.occupant.AdditionalOccupants_FirstName,
                'AdditionalOccupants_LastName': $scope.occupant.AdditionalOccupants_LastName,
                'AdditionalOccupants_email': $scope.occupant.AdditionalOccupants_email,
                'AdditionalOccupants_PhoneNumber': $scope.occupant.AdditionalOccupants_PhoneNumber,
                'isLead': true
            });
        } else {
            $scope.AdditionalOccupantsArray.push({
                'AdditionalOccupants_FirstName': $scope.occupant.AdditionalOccupants_FirstName,

                'AdditionalOccupants_LastName': $scope.occupant.AdditionalOccupants_LastName,
                'AdditionalOccupants_email': $scope.occupant.AdditionalOccupants_email,
                'AdditionalOccupants_PhoneNumber': $scope.occupant.AdditionalOccupants_PhoneNumber


            });
        }


    }


    $scope.DeleteOccupant = function (occupant) {
        angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
            if (value.AdditionalOccupants_email == occupant.AdditionalOccupants_email) {
                $scope.AdditionalOccupantsArray.splice(key, 1);
            }
        });

        $scope.AdditionalOccupants_number = $scope.AdditionalOccupantsArray.length;

    }


    $scope.UpdatedLead = function (Occupant) {

        console.log(Occupant);
        if (Occupant.isLead == true) {
            angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
                if (value.isLead == true && value.AdditionalOccupants_email != Occupant.AdditionalOccupants_email) {
                    $scope.AdditionalOccupantsArray[key].isLead = false;
                }
            });

        } else {
            var countLeads = 0;
            angular.forEach($scope.AdditionalOccupantsArray, function (value, key) {
                if (value.isLead == true) {
                    countLeads++;
                }
            });

            if (countLeads == 0) {
                Occupant.isLead = true;
                toastr.warning('There must be at least 1 lead');
            }


        }


    }


    $scope.$watch('form.AdditionalOccupants_number', function ($val) {
        console.log($val);
        var length = $val - $scope.AdditionalOccupantsArray.length;
        if ($val > $scope.AdditionalOccupantsArray.length) {
            for (var i = 0; i < length; i++) {
                $scope.AdditionalOccupantsArray.push({
                    'AdditionalOccupants_FirstName': '',
                    'AdditionalOccupants_LastName': '',
                    'AdditionalOccupants_email': '',
                    'AdditionalOccupants_PhoneNumber': ''
                });
            }


        } else {
            length = Math.abs(length);
            console.log('length' + length);
            for (var i = 0; i < length; i++) {
                console.log('pop');
                $scope.AdditionalOccupantsArray.pop();
            }
        }

    });


    $scope.$watch('AdditionalBrokers_number', function ($val) {
        console.log($val);
        if ($val > $scope.AdditionalBrokersArray.length) {
            $scope.AdditionalBrokersArray.push({
                'RealEstate_Additional_Jurisdiction': '',
                'RealEstate_Additional_FirstName': '',
                'RealEstate_Additional_LastName': '',
                'RealEstate_Additional_email': '',
                'RealEstate_Additional_PhoneNumber': '',
                'RealEstate_Additional_address': '',
                'relationToLandlord': false,
                'relationToTenant': false,
            });
        } else {
            $scope.AdditionalBrokersArray.pop();
        }
    });
}]);
