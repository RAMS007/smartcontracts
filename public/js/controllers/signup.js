'use strict';

// signup controller
app.controller('SignupFormController', ['$scope', '$http', '$state','AuthService','AUTH_EVENTS','$rootScope','toastr', function($scope, $http, $state,AuthService,AUTH_EVENTS,$rootScope,toastr) {
    $scope.user = {};
    $scope.authError = null;



    $scope.Doregister = function (credentials) {
        $rootScope.LoaderEnabled=true;
        AuthService.register(credentials).then(function (data) {
            console.log('done reg check');
            $rootScope.LoaderEnabled=false;
                if (data.userCreated == true) {
                    //conversion google Google Code for Fremium_signup Conversion Page ///

                    AuthService.profile().then(function (user) {
                        console.log('promise ok first');
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                        $state.go('Dashboard');


                    }, function (r) {
                        console.log($state.is('RegisterFail'));
                        if ($state.is('RegisterFail') || $state.is('Register')) {
                        } else {
                            $state.go('Login');
                        }
                        $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
                    });
                } else {
                    toastr.warning( 'Unknow error while registering');
                }

        }, function (r) {
            $rootScope.LoaderEnabled=false;
            toastr.warning( r);
            console.log('fail reg check');
            $scope.error = r;
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };

    $scope.Recover = function (email) {
        AuthService.Recover(email).then(function (url) {
            console.log('done recover');
            toastr.success( 'Letter with recover link was sent to your email');
        }, function (r) {
            toastr.warning( r);
            console.log('fail reg check');
        });
    };




    /*
    $scope.signup = function() {
      $scope.authError = null;
      // Try to create
      $http.post('api/register', {name: $scope.user.name, email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {
        if ( !response.data.user ) {
          $scope.authError = response;
        }else{
          $state.go('app.dashboard-v1');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };
    */
  }])
 ;