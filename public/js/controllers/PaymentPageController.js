'use strict';

/* Controllers */
// signin controller
app.controller('PaymentPageController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', '$window', 'prompt', '$stateParams', '$location', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $window, prompt, $stateParams, $location) {

    $scope.invoiceId = $stateParams.invoiceId;


    console.log($scope.invoiceId);


    $scope.Send2FactorCode = function (code) {
        var url = '/api/invoices/2fa';
        $rootScope.LoaderEnabled = true;
        var postData = {'twoFactorToken': code};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });


    }


    $scope.PayInvoiceCoinBase = function () {


        var url = '/api/invoices/pay/Coinbase';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoice': $scope.invoiceId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                if (data.twoFactor == true) {

                    // Use new, injected prompt.
                    prompt("Please enter verification code", "").then(
                        function (response) {
                            console.log("Prompt accomplished with", response);
                            $scope.Send2FactorCode(response);

                        },
                        function () {
                            console.log("Prompt failed :(");
                            toastr.warning('You cant  pay without this code.');
                        }
                    );


                } else {

                    toastr.success(data.msg);
                }
            } else {
                toastr.warning(data.msg);
            }
        });


    }

    $scope.PayInvoicePaypal = function () {


        var url = '/api/invoices/pay/Paypal';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoiceId': $scope.invoiceId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {

                console.log(data);
                $window.location.href = data.link;

            } else {
                toastr.warning(data.msg);
            }
        });


    }


    $scope.PayInvoiceStripe = function () {
        var url = '/api/invoices/pay/Stripe';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoiceId': $scope.invoiceId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {

                console.log(data);
                toastr.success(data.msg);

            } else {
                toastr.warning(data.msg);
            }
        });


    }


}])
;
