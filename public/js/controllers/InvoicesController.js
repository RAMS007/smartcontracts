'use strict';

/* Controllers */
// signin controller
app.controller('InvoicesController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', '$window', 'prompt', '$stateParams','$location', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $window, prompt, $stateParams, $location) {

    $scope.LeaseId = $stateParams.LeaseId;


    console.log($scope.LeaseId);
    $scope.GetInvoices = function () {
        if ($scope.LeaseId > 0) {
            var url = '/api/invoices/' + $scope.LeaseId;
        } else {
            var url = '/api/invoices';
        }


        $http.get(url).then(function (res) {
            return res.data;
        }, function errorCallback() {
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            $scope.Invoices = data.invoices;

        });
    };


    $scope.GetInvoices();

    $scope.Send2FactorCode = function (code) {
        var url = '/api/invoices/2fa';
        $rootScope.LoaderEnabled = true;
        var postData = {'twoFactorToken': code};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });


    }


/*
    $scope.PayInvoice = function (invoice) {


        var url = '/api/invoices/pay/Coinbase';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoice': invoice};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                if (data.twoFactor == true) {

                    // Use new, injected prompt.
                    prompt("Please enter verification code", "").then(
                        function (response) {
                            console.log("Prompt accomplished with", response);
                            $scope.Send2FactorCode(response);

                        },
                        function () {
                            console.log("Prompt failed :(");
                            toastr.warning('You cant  pay without this code.');
                        }
                    );


                } else {

                    toastr.success(data.msg);
                }
            } else {
                toastr.warning(data.msg);
            }
        });


    }

    $scope.PayInvoicePaypal = function (invoice) {


        var url = '/api/invoices/pay/Paypal';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoiceId': invoice.id};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {

                console.log(data);
                $window.location.href =data.link;

            } else {
                toastr.warning(data.msg);
            }
        });


    }

*/

    $scope.PayInvoice = function(invoice){

        $state.go('PaymentPage',{'invoiceId':invoice.id});
    }

 /*   $scope.PayInvoiceStripe = function (invoice) {


        var url = '/api/invoices/pay/Stripe';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'invoiceId': invoice.id};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {

                console.log(data);
                toastr.success(data.msg);

            } else {
                toastr.warning(data.msg);
            }
        });


    }
*/

    /*
     $scope.SaveProfile= function(){
     var url = '/api/profile/save';
     $rootScope.LoaderEnabled = true;
     var postData = {'user': $rootScope.user};
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }
     });


     }


     $scope.UnlinkCoinbase=function(){

     var url = '/api/profile/unlinkCoinbase';
     $rootScope.LoaderEnabled = true;
     var postData = {'user': $rootScope.user};
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }
     });


     }

     $scope.UploadBallance=function(){
     var url = '/api/profile/uploadBallance';
     $rootScope.LoaderEnabled = true;
     var postData = {'user': $rootScope.user, 'UploadAmount':$scope.UploadAmount};
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     window.location.href = data.paymentUrl;
     } else {
     toastr.warning(data.msg);
     }
     });
     }




     $scope.UploadLogo = function () {
     console.log('click');
     $rootScope.LoaderEnabled = true;
     var file = $rootScope.CustomLogo;

     console.log('file is ');
     console.dir(file);

     var uploadUrl = "/api/profile/CustomLogo";

     var fd = new FormData();
     fd.append('file', file);
     //    fd.append('form', JSON.stringify(form));
     $http.post(uploadUrl, fd, {
     transformRequest: angular.identity,
     headers: {'Content-Type': undefined}
     }).then(function (res) {
     return res.data;
     }, function errorCallback(response) {
     console.log('err');
     $scope.error = 'error with your request';
     return $q.reject('Error');
     }).then(function (params) {
     console.log('params');
     console.log(params);
     if (params.error == true) {
     toastr.warning(params.msg);
     $rootScope.LoaderEnabled = false;
     } else {
     toastr.success('File procesed succesfull');
     toastr.success(params.msg);
     $rootScope.LoaderEnabled = false;
     }
     })
     }
     */


    /*
     $scope.Forms = [];



     $scope.deleteForm = function (formId) {
     var url = '/api/Forms/delete';
     $rootScope.LoaderEnabled = true;
     var postData = {'formId': formId};
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     }

     $scope.invitePeople = function (form) {

     if (form.email == undefined) {

     toastr.warning('Please enter correct email');
     return false;

     }


     var url = '/api/Forms/invite';
     var postData = {'formId': form.Id, 'email': form.email};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }
     //      $scope.Forms=data.forms;

     });


     }


     $scope.rejectForm = function (formId) {

     var url = '/api/Forms/reject';
     var postData = {'formId': formId};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     };


     $scope.approveForm = function (formId) {

     var url = '/api/Forms/approve';
     var postData = {'formId': formId};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     }

     */
}])
;
