'use strict';

/* Controllers */
// signin controller
app.controller('AccountSettingsController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q','$window', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $window) {



    $scope.SelectRole= function(){
        var url = '/api/profile/role';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'role':$rootScope.newRole};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
                $window.location.reload();
            } else {
                toastr.warning(data.msg);
            }
        });


    }




$scope.submitStripe = function(){
    console.log('submit stripe');

    window.stripe.createToken(card).then(function(result) {
        console.log(result);
        if (result.error) {
            // Inform the user if there was an error.
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            // Send the token to your server.
       //     stripeTokenHandler(result.token);
            $scope.saveStripeToken(result.token.id);
        }
    });


}



$scope.saveStripeToken = function(token){


    var url = '/api/profile/saveStripe';
    $rootScope.LoaderEnabled = true;
    var postData = {'user': $rootScope.user, 'token':token};
    $http.post(url, postData).then(function (res) {
        $rootScope.LoaderEnabled = false;
        return res.data;
    }, function errorCallback() {
        $rootScope.LoaderEnabled = false;
        console.log('err');
        return $q.reject('Error');
    }).then(function (data) {
        console.log(data);
        if (data.error == false) {
            toastr.success(data.msg);
        } else {
            toastr.warning(data.msg);
        }
    });

}


    $scope.SaveProfile= function(){
        var url = '/api/profile/save';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });
    }


    $scope.SavePayPal= function(){
        var url = '/api/profile/savePaypal';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });
    }



    $scope.UnlinkCoinbase=function(){

        var url = '/api/profile/unlinkCoinbase';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
        });


    }

    $scope.UploadBallance=function(){
        var url = '/api/profile/uploadBallance';
        $rootScope.LoaderEnabled = true;
        var postData = {'user': $rootScope.user, 'UploadAmount':$scope.UploadAmount};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
                window.location.href = data.paymentUrl;
            } else {
                toastr.warning(data.msg);
            }
        });
    }




    $scope.UploadLogo = function () {
        console.log('click');
        $rootScope.LoaderEnabled = true;
        var file = $rootScope.CustomLogo;

        console.log('file is ');
        console.dir(file);

        var uploadUrl = "/api/profile/CustomLogo";

        var fd = new FormData();
        fd.append('file', file);
        //    fd.append('form', JSON.stringify(form));
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (res) {
            return res.data;
        }, function errorCallback(response) {
            console.log('err');
            $scope.error = 'error with your request';
            return $q.reject('Error');
        }).then(function (params) {
            console.log('params');
            console.log(params);
            if (params.error == true) {
                toastr.warning(params.msg);
                $rootScope.LoaderEnabled = false;
            } else {
         //       toastr.success('File procesed succesfull');
                toastr.success(params.msg);
                $rootScope.LoaderEnabled = false;
            }
        })
    }




    $scope.UploadPicture = function () {
        console.log('click');
        $rootScope.LoaderEnabled = true;
        var file = $rootScope.ProfilePicture;

        console.log('file is ');
        console.dir(file);

        var uploadUrl = "/api/profile/ProfilePicture";

        var fd = new FormData();
        fd.append('file', file);
        //    fd.append('form', JSON.stringify(form));
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (res) {
            return res.data;
        }, function errorCallback(response) {
            console.log('err');
            $scope.error = 'error with your request';
            return $q.reject('Error');
        }).then(function (params) {
            console.log('params');
            console.log(params);
            if (params.error == true) {
                toastr.warning(params.msg);
                $rootScope.LoaderEnabled = false;
            } else {
   //             toastr.success('File procesed succesfull');
                toastr.success(params.msg);
                $rootScope.LoaderEnabled = false;
                $rootScope.user.ProfilePicture =params.fileName+'?t='+Date.now();
            }
        })
    }




    /*
    $scope.Forms = [];

    $scope.GetForms = function () {

        var url = '/api/Forms';

        $http.get(url).then(function (res) {
            return res.data;
        }, function errorCallback() {
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            $scope.Forms = data.Forms;

        });
    };


    $scope.GetForms();


    $scope.deleteForm = function (formId) {
        var url = '/api/Forms/delete';
        $rootScope.LoaderEnabled = true;
        var postData = {'formId': formId};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    }

    $scope.invitePeople = function (form) {

        if (form.email == undefined) {

            toastr.warning('Please enter correct email');
            return false;

        }


        var url = '/api/Forms/invite';
        var postData = {'formId': form.Id, 'email': form.email};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }
            //      $scope.Forms=data.forms;

        });


    }


    $scope.rejectForm = function (formId) {

        var url = '/api/Forms/reject';
        var postData = {'formId': formId};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    };


    $scope.approveForm = function (formId) {

        var url = '/api/Forms/approve';
        var postData = {'formId': formId};
        $rootScope.LoaderEnabled = true;
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = false;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);
            } else {
                toastr.warning(data.msg);
            }

        });
    }

*/
}])
;
