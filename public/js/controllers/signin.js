'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http', '$state','AuthService', 'toastr','AUTH_EVENTS','$rootScope','$window','$location', function($scope, $http, $state,AuthService, toastr, AUTH_EVENTS, $rootScope,$window,$location) {
    $scope.user = {};
    $scope.authError = null;


$scope.phone={};
    $scope.phone1={};
    $scope.phone2={};
    $scope.phone3={};
    $scope.phone4={};
    $scope.country1={};



    $http.get('/api/logout')
        .then(function (res) {
        }, function errorCallback(response) {
        }).then(function (plans) {

    });

    $http.get('/api/csrf')
        .then(function (res) {
        }, function errorCallback(response) {
        }).then(function (plans) {
        $http.defaults.headers.common['X-CSRF-TOKEN'] = plans;
    });


/*

    $scope.login = function() {
      $scope.authError = null;
      // Try to login
      $http.post('api/login', {email: $scope.user.email, password: $scope.user.password})
      .then(function(response) {
        if ( !response.data.user ) {
          $scope.authError = 'Email or Password not right';
        }else{
          $state.go('app.dashboard-v1');
        }
      }, function(x) {
        $scope.authError = 'Server Error';
      });
    };
*/

    $scope.Dologin = function (credentials) {
        $scope.error = false;
        $rootScope.LoaderEnabled=true;
        AuthService.login(credentials).then(function (credentials) {
            $rootScope.LoaderEnabled=false;
            console.log('done login');
            toastr.success('Logged in');

            console.log(credentials);
            if(credentials.email=='admin@admin.com'){

                $window.location.href = '/admin';
            }else{
                AuthService.profile();
                $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                if($rootScope.postLogInRoute== undefined || $rootScope.postLogInRoute== null){
                    $state.go('Dashboard');
                }else{
                    $location.path($rootScope.postLogInRoute).replace();
                    $rootScope.postLogInRoute = null;
                }

            }


        }, function (r) {
            $rootScope.LoaderEnabled=false;
            console.log('fail login');
            console.log(r);
            toastr.warning( r)
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        });
    };


  }])
;
