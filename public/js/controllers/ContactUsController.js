'use strict';

/* Controllers */
// signin controller
app.controller('ContactUsController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', '$stateParams', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q, $stateParams) {

$scope.message={
    salutation:'Mr'
};


    $scope.SenMessage = function (message) {
        var url = '/api/contactUs';
        var postData = {'message': message};
        $http.post(url, postData).then(function (res) {
            $rootScope.LoaderEnabled = true;
            return res.data;
        }, function errorCallback() {
            $rootScope.LoaderEnabled = false;
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
            $rootScope.LoaderEnabled = false;
            console.log(data);
            if (data.error == false) {
                toastr.success(data.msg);

              } else {
                toastr.warning(data.msg);
            }
        });


    }

    $scope.$watch('user', function (newVal, oldVal) {

        $scope.message.name=newVal.name;
        $scope.message.email=newVal.email;

    });


}]);
