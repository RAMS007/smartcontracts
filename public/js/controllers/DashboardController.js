'use strict';

/* Controllers */
// signin controller
app.controller('DashboardController', ['$scope', '$http', '$state', 'AuthService', 'toastr', 'AUTH_EVENTS', '$rootScope', '$q', function ($scope, $http, $state, AuthService, toastr, AUTH_EVENTS, $rootScope, $q) {


    if ($rootScope.user == undefined) {
        AuthService.profile().then(function () {
            if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
                toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
                $state.go('AccountSettings');
            }
        }, function (r) {

        })
    } else {
        if ($rootScope.user.FirstName == undefined || $rootScope.user.FirstName == null) {
            toastr.warning('To Access Contract Admin, Profile Needs To Be Filled Out');
            $state.go('AccountSettings');
        }
    }



    $scope.getApartmentList=function(){
        var url = '/api/PropertyList';
        $http.get(url).then(function (res) {
            return res.data;
        }, function errorCallback() {
            console.log('err');
            return $q.reject('Error');
        }).then(function (data) {
             $scope.ApartmentList = data.ApartmentList;

        });




    }
    $scope.getApartmentList();



    /*
     $scope.Forms = [];

     $scope.GetForms = function () {

     var url = '/api/Forms';

     $http.get(url).then(function (res) {
     return res.data;
     }, function errorCallback() {
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     $scope.Forms = data.Forms;

     });
     };


     $scope.GetForms();


     $scope.deleteForm = function (formId) {
     var url = '/api/Forms/delete';
     $rootScope.LoaderEnabled = true;
     var postData = {'formId': formId};
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     }

     $scope.invitePeople = function (form) {

     if (form.email == undefined) {

     toastr.warning('Please enter correct email');
     return false;

     }


     var url = '/api/Forms/invite';
     var postData = {'formId': form.Id, 'email': form.email};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }
     //      $scope.Forms=data.forms;

     });


     }


     $scope.rejectForm = function (formId) {

     var url = '/api/Forms/reject';
     var postData = {'formId': formId};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     };


     $scope.approveForm = function (formId) {

     var url = '/api/Forms/approve';
     var postData = {'formId': formId};
     $rootScope.LoaderEnabled = true;
     $http.post(url, postData).then(function (res) {
     $rootScope.LoaderEnabled = false;
     return res.data;
     }, function errorCallback() {
     $rootScope.LoaderEnabled = false;
     console.log('err');
     return $q.reject('Error');
     }).then(function (data) {
     console.log(data);
     if (data.error == false) {
     toastr.success(data.msg);
     } else {
     toastr.warning(data.msg);
     }

     });
     }

     */
}])
;
