{
"header" : {
  "navbar" : {
    "menu":"Menu",
            "new":"New Agreement",
            "list":"Agreements list"
    }
},
    "newAgreement":{
    "title": "Create new contract",
     "upload":"Upload contract",
        "createLease":"Create contract",
        "fromTemplate":"Select previous template",
        "propertyAdress":"Property Address",
        "unit":"Unit",
        "municipalityOf":"Municipality of",
        "country":"country",
        "SchoolDistrict":"School district",
        "SaveProperty":"SaveProperty",
        "address1":"Address line 1",
        "address2":"Address line 2",
        "city":"City",
        "state":" State/Province/Region",
        "zip":"ZIP",
        "country":"Country"

}
}
