{
  "header" : {
  "navbar" : {
    "menu":"Speisekarte",
        "new":"Neue Vereinbarung",
        "list":"Vereinbarungen Liste"
  }
},
  "newAgreement":{
  "title": "Erstellen Sie ein neues Formular",
      "upload":"Laden Sie das Leasing hoch",
      "createLease":"Mietvertrag erstellen",
      "fromTemplate":"Wählen Sie die vorherige Vorlage",
      "propertyAdress":"Immobilien-Adresse",
      "unit":"Einheit",
      "municipalityOf":"Gemeinde von",
      "country":"Land",
      "SchoolDistrict":"Schulbezirk",
      "SaveProperty":"Eigenschaft speichern"
}
}
