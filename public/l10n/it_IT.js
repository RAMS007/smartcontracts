{
  "header":{
  "navbar":{
    "menu":"Menu",
        "new":"Nuovo accordo",
        "list":"Lista di accordi"
  }
},
  "newAgreement":{
  "title":"Crea un nuovo modulo",
      "upload":"Carica leasing",
      "createLease":"Crea contratto di locazione",
      "fromTemplate":"Seleziona il modello precedente",
      "propertyAdress":"Indirizzo della proprietà",
      "unit":"Unità",
      "municipalityOf":"Comune di",
      "country":"nazione",
      "SchoolDistrict":"Distretto scolastico",
      "SaveProperty":"Salva proprietà"
}
}