<?php

namespace App\Console\Commands;

use App\AdditionalOccupants;
use App\Leases;
use App\Settings;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PDF;
use App\InvoiceElements;
use App\Invoices;
use Illuminate\Support\Facades\Mail;
use App\Mail\Sendnvoice;
use GuzzleHttp\Client as GuzzleClient;

class GenerateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GenerateInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * https://www.etherchain.org/api/gasPriceOracle
     * https://ethgasstation.info/json/ethgasAPI.json
     *
     * ETH price
     * https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&api_key=f61e3b0dc333e0fad9c0de1f411d67691ba4d085e7d6e1dce6227563749a1b85
     *
     *
     * @return mixed
     */
    public function handle()
    {
        //find leases to work


        //first with no invoices at all
        $sql = "SELECT leases.id AS leaseId, additional_occupants.email AS tenant_email, leases.moveInDate, leases.Lease_monthAmount, leases.Lease_payFees,invoices.`status`, leases.Lease_payday2
FROM leases
LEFT JOIN invoices ON leases.id = invoices.leaseId
LEFT JOIN additional_occupants ON additional_occupants.lease_id=leases.id
WHERE invoices.id IS NULL AND additional_occupants.email !='' AND leases.Lease_payday2 IS NOT NULL      AND leases.deleted_at IS NULL         ";

        $res = DB::select($sql);
        $countEmpty = 0;
        $countRegenerated = 0;
        foreach ($res as $invoice) {
            //check payment date if  left 5 days generate invoice
            //if left 2 days and not paid - send reminder
            //if now  more then paydey - apply late fee

            if (empty($invoice->Lease_payday2)) {
                continue;
            }
            if (empty($invoice->tenant_email)) {
                continue;
            }

            $payDate = date('Y-m-').$invoice->Lease_payday2;
            $payDateCarbon = new Carbon($payDate);
            $curDate = new Carbon();


            if ($payDateCarbon->gt($curDate)) {
                echo $payDate . ' in future ' . "\n";
                //so this is in future  check how many days left
                $daysToPayment = $payDateCarbon->diffInDays($curDate);
                if ($daysToPayment < 14) {
                    //so generate invoice
                    echo $payDate . ' in future we will generate invoice' . "\n";
                    $this->generateInvoice($invoice->leaseId, $invoice->tenant_email, $payDateCarbon->toDateString());
                } else {
                    continue;
                }
            } else {
                //else this is past
                echo $payDate . ' in past ' . "\n";
                $daysToPayment = $payDateCarbon->diffInDays($curDate);
                $this->generateInvoice($invoice->leaseId, $invoice->tenant_email, $payDateCarbon->toDateString(), $daysToPayment);
            }

            $countEmpty++;
        }

// then invoices with late fees
        $sql = "SELECT leases.id AS leaseId, additional_occupants.email AS tenant_email, leases.moveInDate, invoices.`status`, invoices.paidUntil, invoices.id AS InvoiceId, leases.Lease_payday2
FROM leases
LEFT JOIN invoices ON leases.id = invoices.leaseId
LEFT JOIN additional_occupants ON additional_occupants.lease_id =leases.id
WHERE invoices.`status` <> 'paid' AND invoices.paidUntil < '" . date('Y-m-d') . "' AND  additional_occupants.email <> ''
  AND leases.deleted_at IS NULL ";


        $res = DB::select($sql);
        foreach ($res as $invoice) {
            if (empty($invoice->Lease_payday2)) {
                continue;
            }
            if (empty($invoice->tenant_email)) {
                continue;
            }
//            $payDate = date('Y-m-').$invoice->Lease_payday2;
            $MoveDateCarbon = new Carbon($invoice->paidUntil);
            $curDate = new Carbon();
            $daysToPayment = $MoveDateCarbon->diffInDays($curDate);
            $this->generateInvoice($invoice->leaseId, $invoice->tenant_email, $MoveDateCarbon->toDateString(), $daysToPayment, $invoice->InvoiceId);
            $countRegenerated++;

        }

        /*
                //finally we found leases  which need  invoice for this month
                $sql="SELECT leases.id AS leaseId, additional_occupants.email AS tenant_email, leases.moveInDate, invoices.`status`, invoices.paidUntil, invoices.id AS InvoiceId, MONTH(invoices.paidUntil) AS PaidMonth
        FROM leases
        LEFT JOIN invoices ON leases.id = invoices.leaseId
        LEFT JOIN additional_occupants ON additional_occupants.lease_id=leases.id
        WHERE  invoices.`status` = 'paid' AND additional_occupants.email <> ''
        HAVING
         PaidMonth <> 11";

                $res = DB::select($sql);
                foreach ($res as $invoice) {
                    if (empty($invoice->moveInDate)) {
                        continue;
                    }
                    if (empty($invoice->tenant_email)) {
                        continue;
                    }


                    $NewInvoiceDate = new Carbon($invoice->paidUntil);
                    $NewInvoiceDate=$NewInvoiceDate->addMonth(1);
                    $curDate = new Carbon();


                    if ($NewInvoiceDate->gt($curDate)) {
                        echo$NewInvoiceDate->toDateString(). ' in future ' . "\n";
                        //so this is in future  check how many days left
                        $daysToPayment = $NewInvoiceDate->diffInDays($curDate);
                        if ($daysToPayment <= 5) {
                            //so generate invoice
                            echo $invoice->moveInDate . ' in future we will generate invoice' . "\n";
                            $this->generateInvoice($invoice->leaseId,$invoice->tenant_email, $NewInvoiceDate->toDateString());
                        } else {
                            continue;
                        }
                    } else {
                        //else this is past
                        echo $NewInvoiceDate->toDateString() . ' in past ' . "\n";
                        $daysToPayment = $NewInvoiceDate->diffInDays($curDate);
                        $this->generateInvoice($invoice->leaseId,$invoice->tenant_email, $NewInvoiceDate->toDateString(), $daysToPayment);
                    }

                }

        */

//send invoice 1 week and on due date
        $sql = "SELECT leases.id AS leaseId, additional_occupants.email AS tenant_email, leases.moveInDate, leases.Lease_monthAmount, leases.Lease_payFees,invoices.`status`,  invoices.id AS  InvoiceId, leases.Lease_payday2
FROM leases
LEFT JOIN invoices ON leases.id = invoices.leaseId
LEFT JOIN additional_occupants ON additional_occupants.lease_id=leases.id
WHERE  additional_occupants.email !='' AND leases.Lease_payday2 IS NOT NULL      AND  ( invoices.paidUntil ='" . date('Y-m-d') . "'  OR  invoices.paidUntil ='" . date('Y-m-d', time() - 60 * 60 * 24 * 7) . "'   )       AND leases.deleted_at IS NULL   ";

        $res = DB::select($sql);
        foreach ($res as $invoice) {

            if (empty($invoice->Lease_payday2)) {
                continue;
            }
            if (empty($invoice->tenant_email)) {
                continue;
            }
            $payDate = date('Y-m-').$invoice->Lease_payday2;
            $payDateCarbon = new Carbon($payDate);

            $this->sendInvoiceReminder($invoice->leaseId, $invoice->tenant_email, $payDateCarbon->toDateString(), $invoice->InvoiceId);

        }


        echo 'We finish and process new records:' . $countEmpty . '  updated records:' . $countRegenerated . "\n";

    }

    /**
     * @param $leaseId
     * @param $paidDate
     * @param int $lateDays
     * @param int $invoiceId
     * @return bool|\Illuminate\Http\JsonResponse
     */
    private function generateInvoice($leaseId, $tenantEmail, $paidDate, $lateDays = 0, $invoiceId = 0)
    {
        $lease = Leases::find($leaseId);
        if (empty($lease)) {
            return false;

        }

        $landlordUser = User::find($lease->landlord_user_id);
        if (empty($landlordUser)) {
            Log::error('For lease Id =' . $leaseId . '  we dont found landlord');
            return false;
        }

        $tenantUser = AdditionalOccupants::where('email', $tenantEmail)->first();
        if (empty($tenantUser)) {
            $tenantUser = new \stdClass();;
            $tenantUser->FirstName = 'User not registered';
            $tenantUser->LastName = $lease->tenant_email;
            $tenantUser->HomeAdress = 'User not registered';
            $tenantUser->PhoneNumber = 'User not registered';
            $tenantUser->email = $lease->tenant_email;
        } else {
            $tenantUser->HomeAdress = $tenantUser->address;
        }

        if ($invoiceId == 0) {
            $newInvoice = Invoices::create([
                'user_generated' => $landlordUser->email,
                'user_who_pay' => $tenantEmail,
                'note' => 'note',
                'status' => 'new',
                'paidUntil' => $paidDate,
                'leaseId' => $leaseId

            ]);
        } else {

            $newInvoice = Invoices::find($invoiceId);
            $newInvoice->touch();

        }

        if (!empty($newInvoice)) {
            $total = 0;

            if (empty($lease->Lease_monthAmount)) {
                $lease->Lease_monthAmount = 0;
            }

            $data = new \stdClass();
            $data->invoice_id = $newInvoice->id;
            $data->description = 'Rent';
            $data->amount = $lease->Lease_monthAmount;
            $data->tax = 0;
            if ($invoiceId == 0) {
                InvoiceElements::create((array)$data);
            }
            $total = $SubTotal = $lease->Lease_monthAmount;


            if ($lateDays > 0) {
                $lateFees = $lease->Lease_payFees * $lateDays;
            } else {
                $lateFees = 0;
            }


            $total += $lateFees;
            $serviceFee = 2.75;
            $total += $serviceFee;
            $gasPrice = $this->calculateGas();
            $total += $gasPrice;
            ////////////////////////////
            $newInvoice->lateFees = $lateFees;
            $newInvoice->TotalAmount = $total;
            $newInvoice->gasPrice = $gasPrice;


            $invoice = new \stdClass();
            $invoice->sender = $landlordUser;
            if (!empty($landlordUser->CustomLogo)) {
                $invoice->logo = 'uploads/customLogo/' . $landlordUser->CustomLogo;
            } else {
                $invoice->logo = '';
            }
            $invoice->receiver = $tenantUser;
            $invoice->created_at = date('Y-m-d H:i:s');
            $invoice->PaymentDate = $paidDate;
            $invoice->lines = [$data];
            $invoice->SubTotal = $SubTotal;
            $invoice->total = $total;
            $invoice->gasPrice = $gasPrice;
            $invoice->LateFee = $lateFees;
            $invoice->serviceFee = $serviceFee;

            $invoice->id = $newInvoice->id;

            try {
                PDF::loadView('pdf.invoice', ['invoice' => $invoice])->save(storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'));
            } catch (\Exception $e) {
                Log::error('PDF');
                Log::error($e->getMessage());
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }

            $newInvoice->hash = hash_file('sha256', storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'));
            $newInvoice->serviceFee = $serviceFee;
            $newInvoice->save();
            $tenantName = $tenantUser->FirstName . ' ' . $tenantUser->LastName;
            $dateTs = strtotime($paidDate);
            $invoiceDate = date('F Y', $dateTs);
            try {
                Mail::to($tenantEmail)->send(new Sendnvoice($leaseId, storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'), $tenantName, $invoiceDate, $newInvoice->hash));
            } catch (\Exception $e) {
                echo $e->getMessage();
                Log::error($e->getMessage());
            }


        } else {
            Log::error('Cant generate invoice');
        }
    }


    private function sendInvoiceReminder($leaseId, $tenantEmail, $paidDate, $invoiceId = 0)
    {

        Log::info('send invoice reminder');
        $lease = Leases::find($leaseId);
        if (empty($lease)) {
            return false;
        }

        $landlordUser = User::find($lease->landlord_user_id);
        if (empty($landlordUser)) {
            Log::error('For lease Id =' . $leaseId . '  we dont found landlord');
            return false;
        }

        $tenantUser = AdditionalOccupants::where('email', $tenantEmail)->first();
        if (empty($tenantUser)) {
            $tenantUser = new \stdClass();;
            $tenantUser->FirstName = 'User not registered';
            $tenantUser->LastName = $lease->tenant_email;
            $tenantUser->HomeAdress = 'User not registered';
            $tenantUser->PhoneNumber = 'User not registered';
            $tenantUser->email = $lease->tenant_email;
        } else {
            $tenantUser->HomeAdress = $tenantUser->address;
        }
        $newInvoice = Invoices::find($invoiceId);
        $newInvoice->touch();

        if (!empty($newInvoice)) {

            $tenantName = $tenantUser->FirstName . ' ' . $tenantUser->LastName;
            $dateTs = strtotime($paidDate);
            $invoiceDate = date('F Y', $dateTs);
            try {
                Mail::to($tenantEmail)->send(new Sendnvoice($leaseId, storage_path('app/pdf/invoice_' . $newInvoice->id . '.pdf'), $tenantName, $invoiceDate, $newInvoice->hash));
            } catch (\Exception $e) {
                echo $e->getMessage();
                Log::error($e->getMessage());
            }
        } else {
            Log::error('Cant generate invoice');
        }
    }


    private function calculateGas()
    {


        $gasInDollars = 0;
        $settings = Settings::where('key', 'gasPrice')->first();
        if (!empty($settings)) {
            if (!empty($settings->value)) {
                $gasInDollars = $settings->value;
            }
        }

        if (empty($gasInDollars)) {
            try {
                $GuzzleClent = new GuzzleClient();
                //get ETH price
                $urlWithPrice = 'https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD&api_key=f61e3b0dc333e0fad9c0de1f411d67691ba4d085e7d6e1dce6227563749a1b85';
                $priceResponse = $GuzzleClent->get($urlWithPrice);
                $body = $priceResponse->getBody();
                $stringBody = (string)$body;
                $result = json_decode($stringBody);
                $price = $result->USD;

                //get GASPrice

                $url = 'https://ethgasstation.info/json/ethgasAPI.json';
                $priceResponse = $GuzzleClent->get($url);
                $body = $priceResponse->getBody();
                $stringBody = (string)$body;
                $result = json_decode($stringBody);
                $GASprice = $result->average;


                $gasInDollars = ($GASprice * $price * 300) / 10000000;
            } catch (\Exception $e) {
                return 1;

            }
        }
        return round($gasInDollars, 2);

    }

}
