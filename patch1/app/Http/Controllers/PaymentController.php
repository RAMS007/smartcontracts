<?php

namespace App\Http\Controllers;

use App\Invoices;
use App\Leases;
use App\PaymentRequests;
use Illuminate\Http\Request;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Details;
use PayPal\Api\PaymentExecution;
use PHPUnit\TextUI\ResultPrinter;
use PayPal\Api\Payout;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Api\PayoutItem;
use PayPal\Api\Currency;
use Illuminate\Support\Facades\DB;


class PaymentController extends Controller
{
    public function ProcessWebHook(Request $request)
    {
        $sharedSecret = '53272eb9-b846-4385-a2a3-629539d1ab5c';
        $data = $request->all;
        $hashHeader = $request->header('X-CC-Webhook-Signature');
//        $generatedCode=hash_hmac('sha256',$data,$sharedSecret);
        Log::debug('Secret:' . $hashHeader);
//        Log::debug('generated code:' . $generatedCode);
        Log::debug('Get webhook:' . print_r($data, true));


        $userId = $data['userId'];
        $paymentRequest = PaymentRequests::where('user_id', $userId)->where('hostedPageCode', 'code')->first();
        if (empty($paymentRequest)) {
            Log::error('wrong webhook parametrs');
            //@todo sen alert message
        } else {
            $paymentRequest->state = 'confirmed';
            $paymentRequest->save();

        }

    }


    public function testConBase()
    {
        $configuration = Configuration::apiKey(env('COINBASEAPI_KEY'), env('COINBASEAPI_SECRET'));
        $client = Client::create($configuration);
        $accounts = $client->getAccounts();
        $data = $client->decodeLastResponse();

        $account = new Account([
            'name' => 'New Account'
        ]);
        $client->createAccount($account);
        $data = $client->decodeLastResponse();
        $paymentMethods = $client->getPaymentMethods();

        $data = $client->decodeLastResponse();

        /*   $data = $deposit->getRawData();
           $data = $client->decodeLastResponse();
           */
    }


    public function ProcessCallback(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        Log::debug(print_r($request->all(), true));
        if (!isset($request->code)) {
            abort(404);
        }

        $client = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => 'https://api.coinbase.com/oauth/',
            // You can set any number of default request options.
            'timeout' => 20.0,
        ]);

        $response = $client->request('POST', 'token', [
            'json' => [
                "grant_type" => "authorization_code",
                "code" => $request->code,
                "client_id" => env('COINBASE_APP_ID'),
                "client_secret" => env('COINBASE_APP_SECRET'),
                "redirect_uri" => env('COINBASE_APP_REDIRECT'),
            ]]);

        $body = $response->getBody()->getContents();
        $code = $response->getStatusCode();
        Log::debug('send request');
        Log::debug('code' . $code);
        Log::debug($body);
        $bodyresponse = json_decode($body, true);
        Log::debug(print_r($bodyresponse, true));
        if (isset($bodyresponse['access_token'])) {
            $user->CoinBaseAccessToken = $bodyresponse['access_token'];
            $user->CoinBaseRefreshToken = $bodyresponse['refresh_token'];
            $user->save();
        }


        return redirect('/dashboard');

    }


    /**
     * here we make invoice payments
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function PayInvoicePaypal(Request $request)
    {


        $invoice = Invoices::find($request->invoiceId);
        if (empty($invoice)) {
            return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setTotal($invoice->TotalAmount);
        $amount->setCurrency('USD');

        $transaction = new Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(env('PAYPAL_APP_REDIRECT'))
            ->setCancelUrl(env('PAYPAL_APP_CANCEL'));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);


        // After Step 3
        try {
            $payment->create($apiContext);
            //     echo $payment;
            //     echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
            $PaypalId = $payment->getId();
            $invoice->PaypalId = $PaypalId;
            $invoice->save();

            return response()->json(['error' => false, 'link' => $payment->getApprovalLink()]);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            //    echo $ex->getData();

            return response()->json(['error' => true, 'msg' => $ex->getData()]);
        }


    }


    public function PaypalPayouts($invoiceId)
    {

        $invoice = Invoices::find($invoiceId);
        if (empty($invoice)) {
            Log::error('Cant Found invoice');
            return false;
            //     return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }

       $sql="SELECT users.paypalAccount
FROM invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.id=?";
        $res = DB::select($sql,[$invoiceId]);
        if(isset($res[0]->paypalAccount)){

            $paypalAcount = $res[0]->paypalAccount;
            if(empty($paymentAmount)){

                Log::error('Paypal account empty');
                return false;
            }


        }else{
            Log::error('Cant get paypal account');
            return false;

        }

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );


        $payouts = new Payout();
        $senderBatchHeader = new PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
            ->setEmailSubject("You have a payment");

        $senderItem1 = new PayoutItem();
        $paymentAmount = $invoice->TotalAmount - $invoice->gasPrice - $invoice->serviceFee;
        $senderItem1->setRecipientType('Email')
            ->setNote('Thanks you.')
            ->setReceiver($paypalAcount)
            ->setSenderItemId("invoice_" . $invoiceId)
            ->setAmount(new Currency('{
                        "value":"' . $paymentAmount . '",
                        "currency":"USD"
                    }'));

        $payouts->setSenderBatchHeader($senderBatchHeader)
            ->addItem($senderItem1);


        $request = clone $payouts;

        try {
            $output = $payouts->create(null, $apiContext);
            $t = $output->getLinks();

            $t2 = $output->toArray();
            $t3 = $t[0]->toArray();
        } catch (Exception $ex) {
            Log::error('Cant create payouts ' . $ex->getMessage());
            //  ResultPrinter::printError("Created Batch Payout", "Payout", null, $request, $ex);
            return false;
            //   exit(1);
        }

        // ResultPrinter::printResult("Created Batch Payout", "Payout", $output->getBatchHeader()->getPayoutBatchId(), $request, $output);
        $payoutId = $output->getBatchHeader()->getPayoutBatchId();
        Log::debug('Create payout ' . $payoutId);
        $invoice->payoutId = $payoutId;
        $invoice->save();
        return true;
        //     return $output;


        /*
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $amount = new Amount();
                $amount->setTotal($invoice->TotalAmount);
                $amount->setCurrency('USD');

                $transaction = new Transaction();
                $transaction->setAmount($amount);

                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(env('PAYPAL_APP_REDIRECT'))
                    ->setCancelUrl(env('PAYPAL_APP_CANCEL'));

                $payment = new Payment();
                $payment->setIntent('sale')
                    ->setPayer($payer)
                    ->setTransactions(array($transaction))
                    ->setRedirectUrls($redirectUrls);


                // After Step 3
                try {
                    $payment->create($apiContext);
                    //     echo $payment;
                    //     echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
                    $PaypalId = $payment->getId();
                    $invoice->PaypalId = $PaypalId;
                    $invoice->save();

                    return response()->json(['error' => false, 'link' => $payment->getApprovalLink()]);
                } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    // This will print the detailed information on the exception.
                    //REALLY HELPFUL FOR DEBUGGING
                    //    echo $ex->getData();

                    return response()->json(['error' => true, 'msg' => $ex->getData()]);
                }

        */
    }


    public function PaypalCallback(Request $request)
    {

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );

        /*   $paymentId = 'PAY-7JF098896C7116747LQN4JCA';
           $token = 'EC-9G192826886694410';
           $PayerID = '4VAJLP89AYATS';
   */
        $paymentId = $request->paymentId;
        $PayerID = $request->PayerID;
        $payment = Payment::get($paymentId, $apiContext);
        $invoice = Invoices::where('PaypalId', $paymentId)->first();
        if (empty($invoice)) {
            return redirect('/paymentError?msg=Invoice not found');
        }
        $state = $payment->getState();
        if ($state == 'approved') {
            //Already payed
            $invoice->status = 'paid';
            $invoice->paid_at = time('Y-m-d H:i:s');
            $invoice->save();
            $this->PaypalPayouts($invoice->id);
            return redirect('/dashboard');
        } else {
            $execution = new PaymentExecution();
            $execution->setPayerId($PayerID);
            try {
                $result = $payment->execute($execution, $apiContext);
                $state = $result->getState();
                try {
                    $payment = Payment::get($paymentId, $apiContext);
                    $state = $payment->getState();
                    if ($state == 'approved') {
                        $invoice->status = 'paid';
                        $invoice->save();
                        return redirect('/dashboard');
                    }
                } catch (Exception $ex) {
                    return redirect('/paymentError?msg=' . $ex->getMessage());
                }
            } catch (Exception $ex) {
                return redirect('/paymentError?msg=' . $ex->getMessage());
            }
        }
    }

}
