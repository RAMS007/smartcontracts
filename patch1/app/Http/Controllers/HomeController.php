<?php

namespace App\Http\Controllers;

use App\Invoices;
use App\PaymentRequests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use SendGrid\Email;
use SendGrid\Content;
use SendGrid;
use SendGrid\Mail;
use Illuminate\Support\Facades\DB;
use PDF;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //     $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //      return view('home');
    }


    public function getProfile(Request $request, JWTAuth $JWTAuth)
    {
        /*
                $from = new Email("Example User", "test@example.com");
                $subject = "Sending with SendGrid is Fun";
                $to = new Email("Example User", "work123work123@gmail.com");
                $content = new Content("text/plain", "and easy to do anywhere, even with PHP");

        // Send message as html
        // $content = new SendGrid\Content("text/html", "<h1>Sending with SendGrid is Fun and easy to do anywhere, even with PHP</h1>");

                $mail = new Mail($from, $subject, $to, $content);

                $apiKey = env('SENDGRID_FULL');
                $sg = new SendGrid($apiKey);

                $response = $sg->client->mail()->send()->post($mail);
                echo $response->statusCode();
                print_r($response->headers());
                echo $response->body();


        */


        $user = HelperController::GetCurrentUser($request, $JWTAuth);


        if (empty($user)) {
            return response()->json(['status' => 'ok', 'id' => '', 'msg' => '', 'user' => ['id' => 0, 'role' => '', 'firstName' => 'John', 'lastName' => 'Doe', 'email' => 'test@email.com']]);
        } else {
            $Accounts = User::where('email',$user->email)->get();
            $otherAccountList=[];
            foreach ($Accounts as $account){
                $otherAccountList[]=['id'=>$account->id, 'role'=>$account->role];
            }
            if(count($otherAccountList)==1){
                $AllRoles=[];
            }else{
                $AllRoles=$otherAccountList;
            }

            return response()->json(['status' => 'ok', 'id' => Session::getId(), 'msg' => '', 'user' => $user, 'AllRoles'=>$AllRoles]);

        }
    }


    public function changeRole(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($user->id <> $request->user['id']) {
            return response()->json(['error'=>true,'msg'=>'Error with your request']);
            abort(403);
        } else {
            $user=User::where('email', $user->email)->where('role',$request->role)->first();
            if(!empty($user)){

                Auth::logout();
                Auth::login($user, true);

                return response()->json(['error'=>false,'msg'=>'Your role changed']);


            }else{
                return response()->json(['error'=>true,'msg'=>'This role dosnt exist for this user']);
            }
        }
    }


    public function saveProfile(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($user->id <> $request->user['id']) {
            abort(403);
        } else {
            $user->FirstName = $request->user['FirstName'];
            $user->LastName = $request->user['LastName'];
            $user->AreaCode = $request->user['AreaCode'];
            $user->PhoneNumber = $request->user['PhoneNumber'];
            $user->HomeAdress = $request->user['HomeAdress'];

            if (!empty($request->user['DOB'])) {

                $date = Carbon::parse($request->user['DOB']);
                $user->Dob = $date->addDay()->format('Y-m-d');;
            }


            $user->save();
            return response()->json(['error' => false, 'msg' => 'User saved']);
        }


    }

    public function saveProfilePaypal(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($user->id <> $request->user['id']) {
            abort(403);
        } else {
            $user->paypalAccount = $request->user['paypalAccount'];
            $user->save();
            return response()->json(['error' => false, 'msg' => 'User saved']);
        }


    }



    public function unlinkCoinbase(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($user->id <> $request->user['id']) {
            abort(403);
        } else {
            $user->CoinBaseToken = '';
            $user->save();
            return response()->json(['error' => false, 'msg' => 'User saved']);
        }


    }


    public function uploadBallance(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($request->user['id'] <> $user->id) {
            abort(403);
        }

        $Amount = $request->UploadAmount;
        if (empty($Amount) OR !is_numeric($Amount)) {
            return response()->json(['error' => true, 'msg' => 'Wrong request amount']);

        }

        $apiKey = 'e40f5854-e6f5-459d-9ac6-c7bfa689ab8d';

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://api.commerce.coinbase.com/',
            // You can set any number of default request options.
            'timeout' => 20.0,
        ]);


        $response = $client->request('POST', '/charges', [
            'json' => [

                "name" => "Upload ballance",
                "description" => "Upload account Ballance ",
                "local_price" => [
                    "amount" => $Amount,
                    "currency" => "USD"
                ],
                "pricing_type" => "fixed_price",
                "metadata" => [
                    "customer_id" => $user->id
                ]
            ],
            'headers' => [
                'Content-Type' => 'application/json',
                'X-CC-Api-Key' => $apiKey,
                'X-CC-Version' => '2018-03-22'
            ]]);


        $body = $response->getBody()->getContents();
        $code = $response->getStatusCode();
        if ($code == 201) {
            $bodyresponse = json_decode($body, true);
            $hostedPageCode = $code;
            $hostedPageUrl = $bodyresponse['data']['hosted_url'];
            PaymentRequests::create(
                [
                    'user_id' => $user->id,
                    'hostedPageCode' => $hostedPageCode,
                    'amount' => $Amount,
                    'currency' => 'USD',
                    'state' => 'new'
                ]
            );
            return response()->json(['error' => false, 'msg' => 'You will be redirected to payment page now', 'paymentUrl' => $hostedPageUrl]);
        } else {
            return response()->json(['error' => true, 'msg' => 'Error with response. Response code:' . $code]);
        }


        /*
                curl https://api.commerce.coinbase.com/charges \
             -X POST \
            -H 'Content-Type: application/json' \
            -H "X-CC-Api-Key: <Your API Key>" \
            -H "X-CC-Version: 2018-03-22"
            -d '{
                 "name": "The Sovereign Individual",
                 "description": "Mastering the Transition to the Information Age",
                 "local_price": {
                     "amount": "100.00",
                     "currency": "USD"
                 },
               "pricing_type": "fixed_price",
                 "metadata": {
                   "customer_id": "id_1005",
                   "customer_name": "Satoshi Nakamoto"
                 }
             }'
                */


    }


    public function uploadCustomLogo(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $file = $request->file('file');
        if (!empty($file) AND $file->isValid()) {
            $store = Storage::disk('uploads')->putFileAs(
                'customLogo', $request->file('file'), 'Logo_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension()
            );
            if ($store) {
                $user->CustomLogo = 'Logo_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension();
                $user->save();
                return response()->json(['error' => false, 'msg' => 'Saved']);
            } else {
                return response()->json(['error' => true, 'msg' => 'Cant  save file']);
            }

        } else {
            return response()->json(['error' => true, 'msg' => 'Cant upload  logo']);
        }


    }

    public function uploadProfilePicture(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $file = $request->file('file');
        if (!empty($file) AND $file->isValid()) {
            $store = Storage::disk('uploads')->putFileAs(
                'ProfilePicture', $request->file('file'), 'profile_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension()
            );
            if ($store) {
                $user->ProfilePicture = 'profile_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension();
                $user->save();
                return response()->json(['error' => false, 'msg' => 'Saved', 'fileName' => $user->ProfilePicture]);
            } else {
                return response()->json(['error' => true, 'msg' => 'Cant  save file']);
            }

        } else {
            return response()->json(['error' => true, 'msg' => 'Cant upload  logo']);
        }


    }


    public function GetPropertyList(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        if (empty($user)) {
            Log::error('User not found');
            Log::error(print_r($request->all(), true));
            return response()->json(['ApartmentList' => []]);
        }


        switch ($user->role) {
            case 'landlord':
                $sql = 'SELECT properties.id, properties.address1, properties.address2,properties.city, properties.state, properties.zip, properties.country, leases.id AS LeaseId, additional_occupants.FirstName, additional_occupants.LastName,additional_occupants.isLead
FROM properties
RIGHT JOIN leases ON leases.property_id = properties.id
LEFT JOIN additional_occupants ON additional_occupants.lease_id = leases.id
#LEFT JOIN users ON leases_tenants.tenant_user_id =users.id
WHERE properties.user_id=? AND (additional_occupants.isLead =1 OR additional_occupants.isLead IS NULL) AND properties.address1 IS NOT NULL   AND leases.deleted_at IS NULL ';
                $allProperties = DB::select($sql, [$user->id]);
                break;


            case 'tenant':
                $sql = 'SELECT properties.id, properties.address1, properties.address2,properties.city, properties.state, properties.zip, properties.country, leases.id AS LeaseId, additional_occupants.FirstName, additional_occupants.LastName,additional_occupants.isLead
FROM properties
RIGHT JOIN leases ON leases.property_id = properties.id
LEFT JOIN additional_occupants ON additional_occupants.lease_id = leases.id
#LEFT JOIN users ON leases_tenants.tenant_user_id =users.id
WHERE additional_occupants.email =? AND leases.deleted_at IS NULL';
                $allProperties = DB::select($sql, [$user->email]);
                break;


            case 'property_manager':
                $sql = 'SELECT properties.id, properties.address1, properties.address2,properties.city, properties.state, properties.zip, properties.country, leases.id AS LeaseId, leases.PropertyManager_FirstName AS FirstName, leases.PropertyManager_LastName AS LastName
                        FROM properties
                        RIGHT JOIN leases ON leases.property_id = properties.id
                        #LEFT JOIN users ON leases_tenants.tenant_user_id =users.id
                        WHERE leases.PropertyManager_email = ? AND leases.deleted_at IS NULL';
                $allProperties = DB::select($sql, [$user->email]);
                break;

            case 'broker':
                $sql = 'SELECT properties.id, properties.address1, properties.address2,properties.city, properties.state, properties.zip, properties.country, leases.id AS LeaseId, leases.RealEstate_FirstName AS FirstName, leases.RealEstate_LastName AS LastName
FROM properties
RIGHT JOIN leases ON leases.property_id = properties.id
#LEFT JOIN users ON leases_tenants.tenant_user_id =users.id
WHERE leases.RealEstate_email = ? AND leases.deleted_at IS NULL';
                $allProperties = DB::select($sql, [$user->email]);

                if (empty($allProperties)) {

                    $sql = 'SELECT properties.id, properties.address1, properties.address2,properties.city, properties.state, properties.zip, properties.country, leases.id AS LeaseId, leases.RealEstate_Additional_FirstName AS FirstName, leases.RealEstate_Additional_LastName AS LastName
FROM properties
RIGHT JOIN leases ON leases.property_id = properties.id
WHERE leases.RealEstate_Additional_email = ? AND leases.deleted_at IS NULL';
                    $allProperties = DB::select($sql, [$user->email]);


                }
                break;

            default:


                return response()->json(['ApartmentList' => []]);
                break;


        }


        $propertyArr = [];
        foreach ($allProperties as $property) {
            if (empty($property->FirstName) AND empty($property->LastName)) {
                $TenantName = 'No tenat\'s for this property';
                $status = 'PropertyAvaliable';
            } else {
                $TenantName = $property->FirstName . ' ' . $property->LastName;
                $invoice = Invoices::where('leaseId', $property->LeaseId)->orderBy('paidUntil', 'desc')->first();
                if (empty($invoice)) {
                    $status = 'InvoiceEmpty';
                } else {

                    $MoveDateCarbon = new Carbon($invoice->moveInDate);
                    $curDate = new Carbon();


                    if ($MoveDateCarbon->gt($curDate)) {
                        //this is future
                        $status = 'InvoiceEmpty';




                    }else{

                        if ($invoice->status == 'paid') {
                            $status = 'RentPaid';
                        } else {
                                $status = 'RentPassDue';
                        }

                    }



                }
            }
            $propertyArr[$property->state . ',' . $property->city . ',' . $property->address1][] = ['status' => $status, 'name' => $property->address2, 'tenantName' => $TenantName, 'propertyId' => $property->id];
        }

        $properties = [];
        foreach ($propertyArr as $adress => $v) {
            /*   $apartments=[];
               foreach($v as $apartment){

               }

               */
            $properties[] = ['Adress' => $adress, 'apartments' => $v];
        }


        /*
                $properties = [
                    ['Adress' => 'First Adress',
                        'apartments' => [
                            ['status' => 'InvoiceIssue', 'name' => 'Appartment1', 'tenantName' => 'Tenant1'],
                            ['status' => 'InvoiceIssue', 'name' => 'Appartment2', 'tenantName' => 'Tenant2'],
                        ]


                    ],
                    ['Adress' => 'Second Adress',
                        'apartments' => [
                            ['status' => 'InvoiceIssue', 'name' => 'Appartment3', 'tenantName' => 'Tenant3'],
                            ['status' => 'InvoiceIssue', 'name' => 'Appartment4', 'tenantName' => 'Tenant4'],
                        ]


                    ],


                ];
                */
        return response()->json(['ApartmentList' => $properties]);


    }



    public function test(){


        PDF::loadView('pdf.test')->save(storage_path('app/pdf/test.pdf'));
    }



}
