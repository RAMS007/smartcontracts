<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

Route::get('api/csrf', function() {
    header('Access-Control-Allow-Origin: *');
    return Session::token();
});



Route::get('testpayment', 'PaymentController@testConBase');

Route::get('CoinBaseOauthCallback', 'PaymentController@ProcessCallback');


Route::group(['prefix' => 'admin','middleware' => ['auth', 'chackAdmin']], function () {
    Route::get('/', 'AdminController@getDashboard');
    Route::get('/users', 'AdminController@getUsers');

    Route::get('/user/{userId}', 'AdminController@getUserById');
    Route::post('/user/{userId}', 'AdminController@saveUserById');

    Route::get('/invoices', 'AdminController@getInvoicesList');
    Route::get('/invoice/new', 'AdminController@newInvoice');
    Route::post('/invoice/new', 'AdminController@newInvoiceCreate');
    Route::post('/invoice/delete', 'AdminController@deleteInvoice');

    Route::get('/invoice/{invoiceId}', 'AdminController@getInvoiceById');
    Route::post('/invoice/{invoiceId}', 'AdminController@saveInvoiceById');
    Route::post('/invoices', 'AdminController@saveGasPrice');

    Route::get('/leases', 'AdminController@getLeasesList');
    Route::get('/lease/{leaseId}', 'AdminController@getLease');
    Route::post('/leases/delete', 'AdminController@deleteLease');

    Route::get('/payouts', 'AdminController@getPayoutsList');





});


//angular routes
Route::group(['prefix' => 'api'], function () {
    Route::get('profile', 'HomeController@getProfile');
    Route::post('profile/save', 'HomeController@saveProfile');
    Route::post('profile/savePaypal', 'HomeController@saveProfilePaypal');
    Route::post('profile/role', 'HomeController@changeRole');
    Route::post('profile/unlinkCoinbase', 'HomeController@unlinkCoinbase');
    Route::post('profile/uploadBallance', 'HomeController@uploadBallance');
    Route::post('profile/CustomLogo', 'HomeController@uploadCustomLogo');
    Route::post('profile/ProfilePicture', 'HomeController@uploadProfilePicture');


    Route::post('login', 'Auth\LoginController@loginAngular');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::post('register', 'Auth\RegisterController@registerAngular');

    Route::post('users/delete', 'AdminController@deleteUser');

    Route::post('newForm', 'LeaseController@newForm');
    Route::post('newForm/toUploaded', 'LeaseController@newFormtoUploaded');
    Route::post('newForm/fromTemplate', 'LeaseController@newFormFromTemplate');
    Route::get('getTemplates', 'LeaseController@getTemplates');
    Route::post('Forms/getOne', 'LeaseController@getFormById');

    Route::get('Forms', 'LeaseController@getForms');
    Route::post('Forms/delete', 'LeaseController@deleteForm');
    Route::post('Forms/invite', 'LeaseController@inviteUser');
    Route::post('Forms/reject', 'LeaseController@rejectForm');
    Route::post('Forms/approve', 'LeaseController@approveForm');
    Route::post('Forms/uploadCustom', 'LeaseController@uploadCustomLease');
    Route::post('Forms/saveWithComment', 'LeaseController@saveWithComment');


    Route::any('/coinbase/webhook', 'PaymentController@ProcessWebHook');

   // Route::get('/invoice', 'InvoiceController@generateInvoice');
    Route::get('/invoice', function(){
        \Illuminate\Support\Facades\Artisan::call('GenerateInvoice');

    });

     Route::get('/invoices/{PropertyId?}', 'InvoiceController@GetInvoicesForUser');
    Route::post('/invoices/pay/Paypal', 'PaymentController@PayInvoicePaypal');
    Route::post('/invoices/pay/Coinbase', 'InvoiceController@PayInvoiceCoinbase');
    Route::post('/invoices/2fa', 'InvoiceController@finish2Factor');
    Route::get('/PropertyList', 'HomeController@GetPropertyList');

    Route::get('/PaypalCallback', 'PaymentController@PaypalCallback');

    Route::post('/contactUs', 'AdminController@ContactUs');


});




Route::group(['middleware' => ['auth']], function () {

    Route::get('/invoice/{invoiceId}', 'InvoiceController@getInvoicePDF');

});





Route::get('/test', 'HomeController@test');


//handle aall other routes by angular
Route::any('{path?}', function () {
    return File::get(public_path() . '/angular/index.html');
})->where("path", ".+");
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
