<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;


    public $messageForm;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->messageForm=$message;
       }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
            ->view('emails.contactUs')
            ->subject('CointactUs form! ')
            ->from('support@example.com')
            //       ->to(['work123work123@gmail.com'])
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);

    }
}
