<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class WelcomeMessage extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.welcomeMessage')
            ->subject('Thank you for registration! ')
            ->from('support@example.com')
            //       ->to(['work123work123@gmail.com'])
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);

    }
}
