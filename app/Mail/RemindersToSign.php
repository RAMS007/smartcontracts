<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class RemindersToSign extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;
    public  $contractId;
    public $userName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contractId,$userName)
    {
        $this->contractId = $contractId;
        $this->userName =   $userName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.Reminders')
            ->subject('Reminder ')
            ->from('support@example.com')
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);
    }
}
