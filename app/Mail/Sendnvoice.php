<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class Sendnvoice extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;
    public  $leaseId;
    public $filename;
    public $tenantName;
    public $invoiceDate;
    public $hash;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($leaseId, $fileName, $tenantName,$invoiceDate, $hash)
    {
        $this->leaseId = $leaseId;
        $this->filename = $fileName;
        $this->tenantName =$tenantName;
        $this->invoiceDate = $invoiceDate;
        $this->hash = $hash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.invoice')
            ->subject('Invoice for contract #'.$this->leaseId)
            ->from('support@example.com')
            ->attach($this->filename)
            //       ->to(['work123work123@gmail.com'])
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);
    }
}
