<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class InviteManualy extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;

    public  $leaseId;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($leaseId)
    {
        $this->leaseId = $leaseId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.inviteManualy')
            ->subject('You are invited to lease #'.$this->leaseId)
            ->from('support@example.com')
            //       ->to(['work123work123@gmail.com'])
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);
    }
}
