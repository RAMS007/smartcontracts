<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class InviteTenant extends Mailable
{
    use Queueable, SerializesModels;
    use SendGrid;
    public  $leaseId;
    public $whoInvited;
    public $userRegistered;
    public $propertyAdress;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($leaseId, $whoInvited,$propertyAdress, $userRegistered=false)
    {
        $this->leaseId = $leaseId;
        $this->whoInvited = $whoInvited;

        switch($whoInvited->role){
            case 'tenant':
                $whoInvited->role ='Tenant';
                break;
            case 'broker':
                $whoInvited->role ='Real estate broker';

                break;

            case 'landlord':
                $whoInvited->role ='Landlord';

                break;

            case 'property_manager':

                $whoInvited->role ='Property manager';
                break;

        }
        $this->userRegistered = $userRegistered;
        $this->propertyAdress = $propertyAdress;



    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
/*
        return $this->from('support@example.com')
            ->view('emails.invite');
*/

        return $this
            ->view('emails.invite')
            ->subject('You are invited! ')
            ->from('support@example.com')
     //       ->to(['work123work123@gmail.com'])
            ->sendgrid([
                'personalizations' => [
                    [
                        'substitutions' => [
                            ':myname' => 'RAMS',
                        ],
                    ],
                ],
            ]);





    }
}
