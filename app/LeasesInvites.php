<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeasesInvites extends Model
{
    protected $guarded=['id'];
    protected $table='LeaseInvites';
}
