<?php

namespace App\Http\Controllers;

use App\Invoices;
use Illuminate\Http\Request;
use PDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Enum\CurrencyCode;
use Coinbase\Wallet\Resource\Transaction;
use Coinbase\Wallet\Value\Money;
use Illuminate\Support\Facades\DB;
use App\User;
use Coinbase\Wallet\Resource\Address;
use Coinbase\Wallet\Exception\TwoFactorRequiredException;
use Coinbase\Wallet\Enum\Param;


class InvoiceController extends Controller
{
    public function generateInvoice(Request $request)
    {

        $invoice = new \stdClass();
        $invoice->sender_info = 'Sender info';
        $invoice->receiver_info = 'receiver_info';
        $invoice->created_at = 'created_at';
        $invoice->reference = 'reference';
        $invoice->lines = [
            (object)['description' => 'description', 'amount' => 'amount', 'tax' => 0.2]];
        $invoice->total = 100;
        $invoice->tax = 2;
        $invoice->note = 'NOTE';
        $invoice->id = 1;

        try {
            PDF::loadView('pdf.invoice', ['invoice' => $invoice])->save(storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'));
        } catch (\Exception $e) {
            Log::error('PDF');
            Log::error($e->getMessage());
        }

    }

    public function getInvoicePDF(Request $request, $invoiceId)
    {
        $pathToFile = storage_path('app/pdf/invoice_' . $invoiceId . '.pdf');
        return response()->file($pathToFile);
    }


    public function GetInvoicesForUser($PropertyId=0)
    {
        $user = Auth::user();
        if (empty($user)) {
            abort(404);
        }

        if($PropertyId >0){


                $sql='SELECT invoices.*
FROM invoices
LEFT JOIN leases ON leases.id =invoices.leaseId
LEFT JOIN properties ON properties.id=leases.property_id
WHERE invoices.user_who_pay=? AND properties.id=?';
                $allInvoices=DB::select($sql,[$user->email, $PropertyId]);





        }else{
            if($user->role=='tenant'){
                $allInvoices = Invoices::where('user_who_pay', $user->email)->get();

            }else{
                $sql='SELECT invoices.*
FROM invoices
LEFT JOIN leases ON leases.id =invoices.leaseId
WHERE leases.landlord_user_id=?';
                $allInvoices=DB::select($sql,[$user->id]);
            }
        }




        return response()->json(['error' => false, 'invoices' => $allInvoices]);
    }


    public function PayInvoiceCoinbase(Request $request)
    {
        $user = Auth::user();
        if (empty($user)) {
            abort(404);
        }

        $invoice = $request->invoice;

        if (empty($invoice)) {
            return response()->json(['error' => true, 'msg' => 'Wrong request']);
        }


        $InvoiceDetails = Invoices::where('user_who_pay', $user->email)->where('id', $invoice['id'])->first();

        if (empty($InvoiceDetails)) {
            return response()->json(['error' => true, 'msg' => 'can\'t found this invoice']);
        }


        if (empty($user->CoinBaseRefreshToken)) {
            return response()->json(['error' => true, 'msg' => 'You need link coinbase account first']);
        }
        $sql = 'SELECT users.id
FROM 
invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.id=?';
        $userDetails = DB::select($sql, [$InvoiceDetails->id]);
        $landlord = User::find($userDetails[0]->id);
        if (empty($landlord)) {
            return response()->json(['error' => true, 'msg' => 'Cant found landlord account']);
        }
        if (empty($landlord->CoinBaseRefreshToken)) {
            return response()->json(['error' => true, 'msg' => 'Landlord need link coinbase account first']);
        }

        $configurationLandlord = Configuration::oauth($landlord->CoinBaseAccessToken, $landlord->CoinBaseRefreshToken);
        $clientLandlord = Client::create($configurationLandlord);
        $account = $clientLandlord->getPrimaryAccount();
        //   $accounts = $clientLandlord->getAccounts();
        //   $account = $clientLandlord->getAccount('ccbfd72e-6941-5f08-bc4c-3ddaf61cbe5d');


        $addresses = $clientLandlord->getAccountAddresses($account);
        $AdressArray = $clientLandlord->decodeLastResponse();
        if (empty($AdressArray['data'])) {

            $address = new Address([
                'name' => 'Adress for  rent'
            ]);
            $clientLandlord->createAccountAddress($account, $address);

        } else {
            $adress = $AdressArray['data'][0]['address'];
        }


        $configuration = Configuration::oauth($user->CoinBaseAccessToken, $user->CoinBaseRefreshToken);
        $client = Client::create($configuration);
        $account = $client->getPrimaryAccount();
        try {


            $transaction = Transaction::send([
                'toBitcoinAddress' => $adress,
                'amount' => new Money($InvoiceDetails->TotalAmount, CurrencyCode::USD),
                'description' => 'Pay invoice# ' . $InvoiceDetails->id,
                'fee' => '0.0001' // only required for transactions under BTC0.0001
            ]);


            $r = $client->createAccountTransaction($account, $transaction);

        } catch (TwoFactorRequiredException $e) {
            // show 2FA dialog to user and collect 2FA token
            $request->session()->put('AccountCoinbase', $account);
            $request->session()->put('TransactionCoinbase', $transaction);
            return response()->json(['error' => false, 'twoFactor' => true]);


        }

        Log::debug('Response create transaction');
        Log::debug(print_r($r, true));

        return response()->json(['error' => false, 'twoFactor' => false, 'msg' => 'Tranaction create']);
    }


    public function finish2Factor(Request $request)
    {

        $user = Auth::user();
        if (empty($user)) {
            abort(404);
        }

        if ($request->session()->has('AccountCoinbase')) {
            $account = $request->session()->get('AccountCoinbase');
        } else {
            return response()->json(['error' => true, 'msg' => 'Session error']);
        }

        if ($request->session()->has('TransactionCoinbase')) {
            $transaction = $request->session()->get('TransactionCoinbase');

        } else {
            return response()->json(['error' => true, 'msg' => 'Session error']);
        }


        if (empty($request->twoFactorToken)) {
            return response()->json(['error' => true, 'msg' => 'Token required']);
        }

        $configuration = Configuration::oauth($user->CoinBaseAccessToken, $user->CoinBaseRefreshToken);
        $client = Client::create($configuration);

        try {
            // retry call with token
            $result = $client->createAccountTransaction($account, $transaction, [
                Param::TWO_FACTOR_TOKEN => $request->twoFactorToken,
            ]);
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }

        return response()->json(['error' => false, 'msg' => 'Tranaction create']);

    }


}


