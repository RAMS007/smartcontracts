<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;
use App\User;

class HelperController extends Controller
{

    public static function GetCurrentUser(Request $request, JWTAuth $JWTAuth){
        $user = Auth::user();
        if (empty($user)) {
            if (isset($request->token)) {
                $JWTAuth->setToken($request->token);
                $payload = $JWTAuth->check(true);
                if ($payload) {
                    $userId = $payload->get('sub');
                    $user = User::find($userId);
                    if (empty($user)) {
                        abort(403);
                    }
                }
            } else {
                abort(404);
            }
        }
        return $user;
    }
}
