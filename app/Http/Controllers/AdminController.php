<?php

namespace App\Http\Controllers;

use App\InvoiceElements;
use App\Invoices;
use App\Leases;
use App\Mail\ContactUs;
use App\Settings;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Properties;
use App\AdditionalOccupants;


class AdminController extends Controller
{

    public function getDashboard()
    {
        return view('admin.dashboard');
    }

    public function getUsers()
    {

        $users = User::all();

        return view('admin.users', ['users' => $users]);
    }


    public function deleteUser(Request $request)
    {
        if (!isset($request->uid) or empty($request->uid)) {
            abort(404);
        }
        $user = User::find($request->uid);
        if (!empty($user)) {
            $user->delete();
            return response()->json(['error' => false, 'msg' => 'User deleted']);
        } else {
            return response()->json(['error' => true, 'msg' => 'User not found']);
        }


    }

    public function getUserById($userId)
    {
        if (!isset($userId) or empty($userId)) {
            abort(404);
        }
        $user = User::find($userId);
        if (empty($user)) {
            return redirect('/admin/users');
        } else {

            return view('admin.edituser', ['user' => $user]);

        }


    }

    public function saveUserById($userId, Request $request)
    {
        if (!isset($userId) or empty($userId)) {
            abort(404);
        }
        $user = User::find($userId);
        if (empty($user)) {
            return redirect('/admin/users');
        } else {
            $user->email = $request->email;
            $user->role = $request->role;
            $user->name = $request->name;
            $user->save();
            return redirect('/admin/users');

        }


    }


    public function getInvoicesList()
    {
        /*     $sql = "SELECT invoices.id, invoices.note, invoices.`status`, invoices.paidUntil, U1.email AS WhoGenerate, U2.email AS WhoPay
                     FROM invoices
                     LEFT JOIN users AS U1 ON invoices.user_generated =U1.id
                     LEFT JOIN users AS U2 ON invoices.user_who_pay =U2.id                ";
          */
        $sql = "  SELECT id, user_generated AS WhoGenerate, user_who_pay AS WhoPay, note, `status`,paidUntil,created_at, leaseId
                FROM invoices";
        $invoices = DB::select($sql);

       $settings = Settings::where('key','gasPrice')->first();
        if(!empty($settings)){
            $gasPrice =$settings->value;
        }else{
            $gasPrice = '';
        }

        return view('admin.invoices', ['invoices' => $invoices, 'gasPrice'=>$gasPrice]);
    }


    public function getPayoutsList()
    {

        $sql = "  SELECT invoices.id, invoices.leaseId, invoices.lateFees, invoices.gasPrice,invoices.serviceFee,invoices.TotalAmount,invoices.payoutId, invoices.paid_at,   invoices.payuot_paid_at,    users.paypalAccount
FROM invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.`status`='paid'";
        $invoices = DB::select($sql);
$payouts=[];
        foreach($invoices as $invoice){
            $paymentAmount = $invoice->TotalAmount - $invoice->gasPrice - $invoice->serviceFee;
            $payouts[]=[
                'id'=>$invoice->id,
                'payoutId'=>$invoice->payoutId,
                'paid_at'=>$invoice->paid_at,
                'payuot_paid_at'=>$invoice->payuot_paid_at,

                'paypalAccount'=>$invoice->paypalAccount,
                'paymentAmount'=>$paymentAmount

            ];
        }



        return view('admin.payouts', ['payouts' => $payouts]);
    }




    public function saveGasPrice (Request $request){

        if(isset($request->gasPrice)){
            $settings = Settings::where('key','gasPrice')->first();
            if(!empty($settings)){
                $settings->value =$request->gasPrice;
                $settings->save();
            }else{
                Settings::create([
                    'key'=>'gasPrice',
                    'value'=>$request->gasPrice
                ]);
            }
        }else{
            $settings = Settings::where('key','gasPrice')->first();
            if(!empty($settings)){
                $settings->value =null;
                $settings->save();
            }else{
                Settings::create([
                    'key'=>'gasPrice',
                    'value'=>null
                ]);
            }
        }

        return redirect('/admin/invoices');
    }

    public function getLeasesList()
    {
        $user = Auth::user();
        /*     $sql = "SELECT invoices.id, invoices.note, invoices.`status`, invoices.paidUntil, U1.email AS WhoGenerate, U2.email AS WhoPay
                     FROM invoices
                     LEFT JOIN users AS U1 ON invoices.user_generated =U1.id
                     LEFT JOIN users AS U2 ON invoices.user_who_pay =U2.id                ";
          */
        $sql = "  SELECT leases.id, leases.landlord_user_id, leases.`status`, properties.address1, properties.address2
                    FROM leases
                    LEFT JOIN properties ON properties.id =leases.property_id";
        $leases = DB::select($sql);
        return view('admin.leases', ['leases' => $leases]);
    }


    public function deleteLease(Request $request)
    {
        $user = Auth::user();
        $lease = Leases::find($request->id);
        if (!empty($lease)) {
            $lease->delete();
            return response()->json(['error' => false, 'msg' => 'Deleted']);
        } else {
            return response()->json(['error' => false, 'msg' => 'Already deleted']);
        }
    }

    public function getLease (Request $request, $leaseId){

        $user=Auth::user();
        $Lease = Leases::find($leaseId);
        if (empty($Lease)) {
            abort(404);
        }
        $property = Properties::find($Lease->property_id);
                $AllSigns = [];
                $landlord = User::find($Lease->landlord_user_id);
                $name = !empty($landlord) ? $landlord->FirstName . ' ' . $landlord->LastName : 'John Doe';
                $AllSigns [] = [
                    'role' => 'Landlord',
                    'Name' => $name,
                    'date' => $Lease->LLSigned_date,
                    'sign' => $Lease->LL_sign
                ];

                $PM = User::where('email', $Lease->PropertyManager_email)->first();
                $name = !empty($PM) ? $PM->FirstName . ' ' . $PM->LastName : 'John Doe';
                $AllSigns [] = [
                    'role' => 'Property manager',
                    'Name' => $name,
                    'date' => $Lease->PMSigned_date,
                    'sign' => $Lease->PM_sign
                ];

                $broker = User::where('email', $Lease->RealEstate_email)->first();
                $name = !empty($broker) ? $broker->FirstName . ' ' . $broker->LastName : 'John Doe';
                $AllSigns [] = [
                    'role' => 'Property manager',
                    'Name' => $name,
                    'date' => $Lease->BSigned_date,
                    'sign' => $Lease->B_sign
                ];

                $occupants=AdditionalOccupants::where('lease_id',$Lease->id)->get();
                foreach($occupants as $tenant){
                    if($tenant->isLead==1){
                        $AllSigns [] = [
                            'role' => 'Lead tenant',
                            'Name' => $tenant->FirstName.' '.$tenant->LastName,
                            'date' => $tenant->SignedDate,
                            'sign' => $tenant->sign
                        ];
                    }else{
                        $AllSigns [] = [
                            'role' => 'Tenant',
                            'Name' => $tenant->FirstName.' '.$tenant->LastName,
                            'date' => $tenant->SignedDate,
                            'sign' => $tenant->sign
                        ];
                    }


                }

                return view('admin.showForm',['form'=>$Lease,'property'=>$property, 'AllSigns'=>$AllSigns]);

    }




    public function newInvoice()
    {
        $user = Auth::user();
        return view('admin.newInvoice', ['user' => $user]);
    }


    public function newInvoiceCreate(Request $request)
    {

        $data = $request->all();
        $data = json_decode($request->data);
        if (empty($data)) {

            return response()->json(['error' => true, 'msg' => ' You must add at least 1 item']);
        }
        $form = $this->unserializeForm($request->form);

        $validator = Validator::make($form, [
            'WhoGenerate' => 'required|email',
            'InvoiceFor' => 'required|email',
            'note' => 'required|string',
            'paidUntil' => 'required|date',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => true, 'msg' => $validator->errors()->first()]);
        }


        try {
            $newInvoice = Invoices::create([
                'user_generated' => $form['WhoGenerate'],
                'user_who_pay' => $form['InvoiceFor'],
                'note' => $form['note'],
                'status' => 'new',
                'paidUntil' => $form['paidUntil']

            ]);

            if (!empty($newInvoice)) {
                $total = 0;
                $taxTotal = 0;
                foreach ($data As $element) {
                    InvoiceElements::create([
                        'invoice_id' => $newInvoice->id,
                        'description' => $element->description,
                        'amount' => $element->amount,
                        'tax' => $element->tax
                    ]);
                    $total += $element->amount;
                    $taxTotal += $element->tax;
                }

                //@todo create job delayed to generate invoice
                ////////////////////////////

                $invoice = new \stdClass();
                $invoice->sender_info = $form['WhoGenerate'];
                $invoice->receiver_info = $form['InvoiceFor'];
                $invoice->created_at = date('Y-md H:i:s');
                $invoice->reference = 'reference';
                $invoice->lines = $data;
                $invoice->total = $total;
                $invoice->tax = $taxTotal;
                $invoice->note = $form['note'];
                $invoice->id = $newInvoice->id;


                try {
                    PDF::loadView('pdf.invoice', ['invoice' => $invoice])->save(storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'));
                } catch (\Exception $e) {
                    Log::error('PDF');
                    Log::error($e->getMessage());
                    return response()->json(['error' => true, 'msg' => $e->getMessage()]);
                }


                //////////////////////////////
                return response()->json(['error' => false, 'msg' => 'Created']);

            } else {
                return response()->json(['error' => true, 'msg' => 'Cant create invoice']);
            }
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => $e->getMessage()]);
        }


        return response()->json(['error' => false]);


    }

    private function unserializeForm($str)
    {
        $strArray = explode("&", $str);
        foreach ($strArray as $item) {
            $array = explode("=", $item);
            $returndata[$array[0]] = urldecode($array[1]);
        }
        return $returndata;
    }


    public function deleteInvoice(Request $request)
    {
        $invoice = Invoices::find($request->id);
        $invoice->delete();

        $invoiceElements = InvoiceElements::where('invoice_id', $request->id)->delete();

        return response()->json(['error' => false, 'msg' => 'Deleted']);

    }


    public function getInvoiceById($InvoiceId)
    {
        $invoice = Invoices::find($InvoiceId);
        if (empty($invoice)) {
            abort(404);
        }

        return view('admin.editInvoice', ['invoice' => $invoice]);

    }


    public function saveInvoiceById($invoiceId, Request $request)
    {
        if (!isset($invoiceId) or empty($invoiceId)) {
            abort(404);
        }
        $invoice = Invoices::find($invoiceId);
        if (empty($invoice)) {
            return redirect('/admin/invoices');
        } else {

            $invoice->user_generated = $request->WhoGenerate;
            $invoice->user_who_pay = $request->InvoiceFor;
            $invoice->note = $request->note;
            $invoice->status = $request->status;
            $invoice->save();



            //@todo create job delayed to generate invoice
            ////////////////////////////

            $invoiceElements = InvoiceElements::where('invoice_id', $invoiceId)->get();
            $total = 0;
            $taxTotal = 0;
            foreach ($invoiceElements As $element) {
                $total += $element->amount;
                $taxTotal += $element->tax;
            }


            $invoice = new \stdClass();
            $invoice->sender_info = $request->WhoGenerate;
            $invoice->receiver_info =  $request->InvoiceFor;
            $invoice->created_at = date('Y-md H:i:s');
            $invoice->reference = 'reference';
            $invoice->lines = $invoiceElements;
            $invoice->total = $total;
            $invoice->tax = $taxTotal;
            $invoice->note = $request->note;
            $invoice->id = $invoiceId;


            try {
                PDF::loadView('pdf.invoice', ['invoice' => $invoice])->save(storage_path('app/pdf/invoice_' . $invoice->id . '.pdf'));
            } catch (\Exception $e) {
                Log::error('PDF');
                Log::error($e->getMessage());
                return response()->json(['error' => true, 'msg' => $e->getMessage()]);
            }


            //////////////////////////////




            return redirect('/admin/invoices');

        }


    }


    public function ContactUs (Request $request){

        $user=Auth::user();
        $requestMessage = $request->message;
        if(!isset($requestMessage['salutation'])){
            $requestMessage['salutation']='';
        }

        if(!isset($requestMessage['name'])){
            $requestMessage['name']=$user->name;
        }
        if(!isset($requestMessage['text']) OR empty($requestMessage['text'])){
            return response()->json(['error'=>true,'msg'=>'Please enter your message']);
        }

        try{
            $r = Mail::to('work123work123@gmail.com')->send(new ContactUs($requestMessage));
            return response()->json(['error'=>false,'msg'=>'We got your message']);
        }catch (\Exception $e){
            return response()->json(['error'=>true,'msg'=>$e->getMessage()]);

        }

    }






}
