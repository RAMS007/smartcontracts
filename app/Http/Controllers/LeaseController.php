<?php

namespace App\Http\Controllers;

use App\AdditionalOccupants;
use App\Comments;
use App\LandLord2BrokerRelations;
use App\Leases;
use App\Leases_tenants;
use App\LeasesInvites;
use App\Mail\InviteManualy;
use App\Mail\InviteTenant;
use App\Mail\PleaseCreateAccount;
use App\Properties;
use App\Tenant2BrokerRelations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Validator;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\AdditionalBrokers;
use App\Http\Controllers\HelperController;

use Illuminate\Support\Facades\Log;

date_default_timezone_set('Europe/Kiev');


class LeaseController extends Controller
{
    public function newForm(Request $request, JWTAuth $JWTAuth)
    {

        Log::debug(print_r($request->all(), true));
        $user = HelperController::GetCurrentUser($request, $JWTAuth);


        $dateFields=['moveInDate', 'MoveOutDate'];
        $requestData = $request->all();
        unset($requestData['token']);
        if (!isset($requestData['UploadLease']) OR $requestData['UploadLease'] == false) {
            //so we use  our templates


            unset($requestData['UploadLease']);


            foreach ($requestData as $key => $value) {

                if(array_search($key,$dateFields)===false){


                }else{
                    if ($this->isDate($value)) {
                        $requestData[$key] = $this->isDate($value);
                    }
                }


                if ($key == 'signature') {
                    switch ($user->role) {
                        case 'tenant':
                            $requestData['T_sign'] = isset($value['dataUrl']) ? $value['dataUrl'] : '';
                            $requestData['TSigned_date'] = date('Y-m-d');
                            break;
                        case 'broker':
                            $requestData['B_sign'] = isset($value['dataUrl']) ? $value['dataUrl'] : '';
                            $requestData['BSigned_date'] = date('Y-m-d');
                            break;

                        case 'landlord':
                            $requestData['LL_sign'] = isset($value['dataUrl']) ? $value['dataUrl'] : '';
                            $requestData['LLSigned_date'] = date('Y-m-d');
                            break;

                        case 'property_manager':
                            $requestData['PM_sign'] = isset($value['dataUrl']) ? $value['dataUrl'] : '';
                            $requestData['PMSigned_date'] = date('Y-m-d');
                            break;

                    }
                }
            }

            if (isset($request->property)) {
                $property = (array)$request->property;
            } else {
                $property = [];
            }

            if (empty($property)) {
                return response()->json(['error' => true, 'msg' => 'Please Fill Out All The Property Fields']);
            }
            $property['user_id'] = $user->id;

            if (isset($request->Id)) {
                //so we edit form
                $currentLease = Leases::where('id', $request->Id)->where('landlord_user_id', $user->id)->first();
                if (empty($currentLease)) {
                    //check maybe this is tenat edits

                    $invite = LeasesInvites::where('lease_id', $request->Id)->where('invited_email', $user->email)->first();
                    if (empty($invite)) {
                        return response()->json(['error' => true, 'msg' => 'Somthing wrong with your request ']);
                    } else {
                        $currentLease = Leases::where('id', $request->Id)->first();
                        // so write our changes into  tenants table
                        //          $leasesTenant = Leases_tenants::where('tenant_user_id', $user->id)->where('ParentLease_id', $request->Id)->first();
                        if (!empty($Trelationship)) {

                            if ($currentLease->tenant2brokerrelations_id > 0) {
                                Tenant2BrokerRelations::where('id', $currentLease->tenant2brokerrelations_id)->update($Trelationship);
                            } else {
                                $curentTenantRelation = Tenant2BrokerRelations::create($Trelationship);
                                $currentLease->tenant2brokerrelations_id = $curentTenantRelation->id;
                            }
                        }

                        $LeaseData = $requestData;
                        unset($LeaseData['signature']);
                        unset($LeaseData['property']);
                        unset($LeaseData['LLrelationship']);
                        unset($LeaseData['Trelationship']);
                        unset($LeaseData['tenant']);
                        unset($LeaseData['landlord_user_id']);
                        unset($LeaseData['tenant_email']);
                        unset($LeaseData['property_id']);
                        unset($LeaseData['tenant2brokerrelations_id']);
                        unset($LeaseData['landlord2brokerrelations_id']);
                        unset($LeaseData['status']);
                        //         if (empty($leasesTenant)) {

                        //              $LeaseData['tenant_user_id'] = $user->id;
                        //            $LeaseData['ParentLease_id'] = $request->Id;
                        //            Leases_tenants::create($LeaseData);


                        /*         } else {
                                     Leases_tenants::where('tenant_user_id', $user->id)->where('ParentLease_id', $request->Id)
                                         ->update($LeaseData);
                                     $currentLease->status = 'HasChangesByTenant';
                                     $currentLease->save();
                                 }*/
                    }
                } else {
                    Properties::where('id', $currentLease->property_id)
                        ->update(
                            $property
                        );

                    $LeaseData = $requestData;
                    $LeaseData['status'] = 'HasChangesByLandlord';
                    unset($LeaseData['signature']);
                    unset($LeaseData['property']);
                    unset($LeaseData['LLrelationship']);
                    unset($LeaseData['Trelationship']);
                    unset($LeaseData['tenant']);
                    Leases::where('id', $request->Id)->where('landlord_user_id', $user->id)->update(
                        $LeaseData
                    );
                    return response()->json(['error' => false, 'msg' => 'Updated']);
                }

            } else {
                try {
                    $CurrentProperty = Properties::create($property);
                } catch (\Exception $e) {
                    return response()->json(['error' => true, 'msg' => 'Cant  create Property.' . $e->getMessage()]);
                }


                $LeaseData = $requestData;
                $LeaseData['status'] = 'new';

                if ($user->role != 'landlord') {
                    if (isset($LeaseData['LandLord']['email'])) {
                        $landlord = User::where('email', $LeaseData['LandLord']['email'])->first();

                        if (empty($landlord)) {
                            $landLordData = [
                                'id' => 0,
                                'email' => $LeaseData['LandLord']['email'],
                                'FirstName' => isset($LeaseData['LandLord']['FirstName']) ? $LeaseData['LandLord']['FirstName'] : '',
                                'LastName' => isset($LeaseData['LandLord']['LastName']) ? $LeaseData['LandLord']['LastName'] : '',
                                'PhoneNumber' => isset($LeaseData['LandLord']['PhoneNumber']) ? $LeaseData['LandLord']['PhoneNumber'] : '',
                                'address' => isset($LeaseData['LandLord']['address']) ? $LeaseData['LandLord']['address'] : '',

                            ];

                        } else {
                            if ($landlord->role == 'landlord') {
                                $landLordData = [
                                    'id' => $landlord->id,
                                    'email' => $LeaseData['LandLord']['email'],
                                    'FirstName' => $LeaseData['LandLord']['FirstName'],
                                    'LastName' => $landlord->LastName,
                                    'PhoneNumber' => isset($LeaseData['LandLord']['PhoneNumber']) ? $LeaseData['LandLord']['PhoneNumber'] : '',
                                    'address' => isset($LeaseData['LandLord']['address']) ? $LeaseData['LandLord']['address'] : '',

                                ];
                            } else {
                                return response()->json(['error' => true, 'msg' => 'User with landrod email already registered but his role not landlord']);
                            }


                        }


                    } else {
                        return response()->json(['error' => true, 'msg' => 'Landlord email cant be empty']);
                    }


                }


                switch ($user->role) {
                    case 'tenant':
                        $LeaseData['landlord_user_id'] = $landLordData['id'];
                        $LeaseData['Landlord_email'] = $landLordData['email'];
                        $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                        $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                        $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                        $LeaseData['Landlord_address'] = $landLordData['address'];
                        break;
                    case 'broker':
                        $LeaseData['landlord_user_id'] = $landLordData['id'];
                        $LeaseData['Landlord_email'] = $landLordData['email'];
                        $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                        $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                        $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                        $LeaseData['Landlord_address'] = $landLordData['address'];

                        $LeaseData['RealEstate'] = 1;
                        $LeaseData['RealEstate_email'] = $user->email;
                        $LeaseData['RealEstate_FirstName'] = $user->FirstName;
                        $LeaseData['RealEstate_LastName'] = $user->LastName;
                        $LeaseData['RealEstate_PhoneNumber'] = $user->PhoneNumber;
                        $LeaseData['RealEstate_address'] = $user->HomeAdress;

                        $count = User::where('email', $landLordData['email'])->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Landlord';
                            //      Mail::to($landLordData['email'])->send(new PleaseCreateAccount($u));


                        }


                        break;

                    case 'landlord':
                        $LeaseData['landlord_user_id'] = $user->id;
                        $LeaseData['Landlord_email'] = $user->email;
                        $LeaseData['Landlord_FirstName'] = $user->FirstName;
                        $LeaseData['Landlord_LastName'] = $user->LastName;
                        $LeaseData['Landlord_PhoneNumber'] = $user->PhoneNumber;
                        $LeaseData['Landlord_address'] = $user->HomeAdress;

                        break;

                    case 'property_manager':

                        $LeaseData['landlord_user_id'] = $landLordData['id'];
                        $LeaseData['Landlord_email'] = $landLordData['email'];
                        $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                        $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                        $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                        $LeaseData['Landlord_address'] = $landLordData['address'];

                        $LeaseData['PropertyManager'] = 1;
                        $LeaseData['PropertyManager_email'] = $user->email;
                        $LeaseData['PropertyManager_FirstName'] = $user->FirstName;
                        $LeaseData['PropertyManager_LastName'] = $user->LastName;
                        $LeaseData['PropertyManager_PhoneNumber'] = $user->PhoneNumber;
                        $LeaseData['PropertyManager_address'] = $user->HomeAdress;

                        $count = User::where('email', $landLordData['email'])->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Landlord';
                            //                     Mail::to($landLordData['email'])->send(new PleaseCreateAccount($u));
                        }

                        break;

                }


                $LeaseData['tenant_email'] = '';
                $LeaseData['property_id'] = $CurrentProperty->id;
                $LeaseData['created_by'] = $user->id;
                unset($LeaseData['signature']);
                unset($LeaseData['property']);
                unset($LeaseData['LLrelationship']);
                unset($LeaseData['Trelationship']);
                unset($LeaseData['tenant']);
                unset($LeaseData['LandLord']);
                unset($LeaseData['AdditionalOccupantsArray']);
                unset($LeaseData['AdditionalBrokersArray']);


                try {
                    $lease = Leases::create(
                        $LeaseData
                    );
                } catch (\Exception $e) {
                    return response()->json(['error' => true, 'msg' => 'Cant  create Lease.' . $e->getMessage()]);
                }


                if (!empty($lease)) {

                    if (!empty($lease->PropertyManager_email)) {


                        $count = User::where('email', $lease->PropertyManager_email)->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Property manager';
                            $u->email = $lease->PropertyManager_email;
                            //   Mail::to($lease->PropertyManager_email)->send(new PleaseCreateAccount($u));
                        } else {
                            $u = false;
                        }

                        $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                        Mail::to($lease->PropertyManager_email)->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

                    }
                    if (!empty($lease->RealEstate_email)) {

                        $count = User::where('email', $lease->RealEstate_email)->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Real estate broker';
                            $u->email = $lease->RealEstate_email;
                            //      Mail::to($lease->RealEstate_email)->send(new PleaseCreateAccount($u));
                        } else {
                            $u = false;
                        }

                        $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                        Mail::to($lease->RealEstate_email)->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

                    }


                    $countOccupants = 0;
                    foreach ($request->AdditionalOccupantsArray as $occupant) {


                        try {
                            AdditionalOccupants::create([
                                'lease_id' => $lease->id,
                                'FirstName' => isset($occupant['AdditionalOccupants_FirstName']) ? $occupant['AdditionalOccupants_FirstName'] : '',
                                'LastName' => isset($occupant['AdditionalOccupants_LastName']) ? $occupant['AdditionalOccupants_LastName'] : '',
                                'email' => isset($occupant['AdditionalOccupants_email']) ? $occupant['AdditionalOccupants_email'] : '',
                                'PhoneNumber' => isset($occupant['AdditionalOccupants_PhoneNumber']) ? $occupant['AdditionalOccupants_PhoneNumber'] : '',
                                'address' => isset($occupant['AdditionalOccupants_address']) ? $occupant['AdditionalOccupants_address'] : '',
                                'isLead' => isset($occupant['isLead']) ? $occupant['isLead'] : 0
                            ]);

                            $count = User::where('email', $occupant['AdditionalOccupants_email'])->count();
                            if ($count == 0) {
                                $u = new \stdClass();
                                $u->role = 'tenant';
                                //                 Mail::to($occupant['AdditionalOccupants_email'])->send(new PleaseCreateAccount($u));
                            }

                        } catch (\Exception $e) {
                            return response()->json(['error' => true, 'msg' => 'Cant  create Tenant.' . $e->getMessage()]);
                        }


                        $countOccupants++;
                        if (isset($occupant['AdditionalOccupants_email'])) {
                            $count = User::where('email', $occupant['AdditionalOccupants_email'])->count();
                            if ($count == 0) {
                                $u = new \stdClass();
                                $u->role = 'Tenant';
                                $u->email = $occupant['AdditionalOccupants_email'];
                            } else {
                                $u = false;
                            }

                            $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                            Mail::to($occupant['AdditionalOccupants_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));
                        }

                    }
                    if (isset($request->AdditionalBrokersArray)) {
                        foreach ($request->AdditionalBrokersArray as $broker) {

                            try {


                                AdditionalBrokers::create([
                                    'lease_id' => $lease->id,
                                    'FirstName' => isset($broker['RealEstate_Additional_FirstName']) ? $broker['RealEstate_Additional_FirstName'] : '',
                                    'LastName' => isset($broker['RealEstate_Additional_LastName']) ? $broker['RealEstate_Additional_LastName'] : '',
                                    'Jurisdiction' => isset($broker['RealEstate_Additional_Jurisdiction']) ? $broker['RealEstate_Additional_Jurisdiction'] : '',
                                    'email' => isset($broker['RealEstate_Additional_email']) ? $broker['RealEstate_Additional_email'] : '',
                                    'PhoneNumber' => isset($broker['RealEstate_Additional_PhoneNumber']) ? $broker['RealEstate_Additional_PhoneNumber'] : '',
                                    'AreaCode' => '',
                                    'Address' => isset($broker['RealEstate_Additional_address']) ? $broker['RealEstate_Additional_address'] : '',
                                    'relationToLandlord' => isset($broker['relationToLandlord']) ? $broker['relationToLandlord'] : '',
                                    'relationToTenant' => isset($broker['relationToTenant']) ? $broker['relationToTenant'] : '',

                                    'isLead' => isset($broker['isLead']) ? $broker['isLead'] : 0
                                ]);

                            } catch (\Exception $e) {
                                return response()->json(['error' => true, 'msg' => 'Cant  create Broker.' . $e->getMessage()]);
                            }


                            if (isset($broker['RealEstate_Additional_email'])) {
                                $count = User::where('email', $broker['RealEstate_Additional_email'])->count();
                                if ($count == 0) {
                                    $u = new \stdClass();
                                    $u->role = 'Real estate broker ';
                                    $u->email = $broker['RealEstate_Additional_email'];
                                } else {
                                    $u = false;
                                }


                                $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                                Mail::to($broker['RealEstate_Additional_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

                            }


                        }

                    }
                    $lease->AdditionalOccupants_number = $countOccupants;
                    $lease->save();

                }

            }
            return response()->json(['error' => false, 'msg' => 'Saved']);


        } else {
            $requestData = json_decode($request->form, true);
            // we use custom template
            $file = $request->file('file');
            if (!empty($file) AND $file->isValid()) {
                if (!in_array($request->file('file')->getClientOriginalExtension(), ['doc', 'docx', 'pdf'])) {
                    return response()->json(['error' => true, 'msg' => 'Only DOC and PDF files allowed']);
                }

                if (isset($requestData['Id']) AND !empty($requestData['Id']) AND ($requestData['Id'] != 'undefined')) {
                    $lease = Leases::find($requestData['Id']);
                    if (empty($lease)) {
                        return response()->json(['error' => true, 'msg' => 'Cant  found form Id']);
                    } else {
                        $store = Storage::disk('uploads')->putFileAs(
                            'customLease', $request->file('file'), 'Lease_' . $request->user()->id . '_' . $requestData['Id'] . '.' . $request->file('file')->getClientOriginalExtension()
                        );
                        if ($store) {
                            $lease->customLease_file = 'Lease_' . $request->user()->id . '_' . $requestData['Id'] . '.' . $request->file('file')->getClientOriginalExtension();
                            $lease->IsCustom = 1;
                            $lease->save();
                            return response()->json(['error' => false, 'msg' => 'Saved']);
                        } else {
                            return response()->json(['error' => true, 'msg' => 'Cant  save file']);
                        }
                    }
                } else {
                    /*       if (isset($requestData['property'])) {
                               $property = (array)$requestData['property'];
                           } else {
                               $property = [];
                           }
                           if (empty($property)) {
                               return response()->json(['error' => true, 'msg' => 'Please fill property fields']);
                           }
       */

                    $property['user_id'] = $user->id;

                    try {

                        $CurrentProperty = Properties::create($property);

                        $lease = Leases::create([
                            'landlord_user_id' => 0,
                            'created_by' => $user->id,
                            'tenant_email' => '',
                            'property_id' => $CurrentProperty->id
                        ]);
                    } catch (QueryException $e) {
                        $t = $e->getMessage();
                        return response()->json(['error' => true, 'msg' => $t]);
                    }


                    if (empty($lease)) {
                        return response()->json(['error' => true, 'msg' => 'Cant  create Lease']);
                    } else {
                        $store = Storage::disk('uploads')->putFileAs(
                            'customLease', $request->file('file'), 'Lease_' . $request->user()->id . '_' . $lease->id . '.' . $request->file('file')->getClientOriginalExtension()
                        );
                        if ($store) {
                            $lease->customLease_file = 'Lease_' . $request->user()->id . '_' . $lease->id . '.' . $request->file('file')->getClientOriginalExtension();
                            $lease->IsCustom = 1;
                            $lease->save();
                            return response()->json(['error' => false, 'msg' => 'Saved', 'leaseId' => $lease->id]);
                        } else {
                            return response()->json(['error' => true, 'msg' => 'Cant  save file']);
                        }
                    }

                }
            } else {
                return response()->json(['error' => true, 'msg' => 'Uploaded file invalid']);
            }
        }


    }


    public function newFormFromTemplate(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if (isset($request->property)) {
            $property = (array)$request->property;
        } else {
            $property = [];
        }

        if (empty($property)) {
            return response()->json(['error' => true, 'msg' => 'Please Fill Out All The Property Fields']);
        }
        $property['user_id'] = $user->id;
        try {
            $CurrentProperty = Properties::create($property);
        } catch (QueryException $e) {
            $t = $e->getMessage();
            return response()->json(['error' => true, 'msg' => $t]);
        }

        $LeaseData = Leases::find($request->TemplateId);
        if (empty($LeaseData)) {
            return response()->json(['error' => true, 'msg' => 'Wrong template Id']);
        }
        $LeaseData = $LeaseData->toArray();
        $LeaseData['status'] = 'new';


        if ($user->role != 'landlord') {
            if (isset($LeaseData['LandLord']['email'])) {
                $landlord = User::where('email', $LeaseData['LandLord']['email'])->first();

                if (empty($landlord)) {
                    $landLordData = [
                        'id' => 0,
                        'email' => $LeaseData['LandLord']['email'],
                        'FirstName' => isset($LeaseData['LandLord']['FirstName']) ? $LeaseData['LandLord']['FirstName'] : '',
                        'LastName' => isset($LeaseData['LandLord']['LastName']) ? $LeaseData['LandLord']['LastName'] : '',
                        'PhoneNumber' => isset($LeaseData['LandLord']['PhoneNumber']) ? $LeaseData['LandLord']['PhoneNumber'] : '',
                        'address' => isset($LeaseData['LandLord']['address']) ? $LeaseData['LandLord']['address'] : '',

                    ];

                } else {
                    if ($landlord->role == 'landlord') {
                        $landLordData = [
                            'id' => $landlord->id,
                            'email' => $LeaseData['LandLord']['email'],
                            'FirstName' => $LeaseData['LandLord']['FirstName'],
                            'LastName' => $landlord->LastName,
                            'PhoneNumber' => isset($LeaseData['LandLord']['PhoneNumber']) ? $LeaseData['LandLord']['PhoneNumber'] : '',
                            'address' => isset($LeaseData['LandLord']['address']) ? $LeaseData['LandLord']['address'] : '',
                        ];
                    } else {
                        return response()->json(['error' => true, 'msg' => 'User with landrod email already registered but his role not landlord']);
                    }


                }


            } else {
                return response()->json(['error' => true, 'msg' => 'Landlord email cant be empty']);
            }


        }

        switch ($user->role) {
            case 'tenant':
                $LeaseData['landlord_user_id'] = $landLordData['id'];
                $LeaseData['Landlord_email'] = $landLordData['email'];
                $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                $LeaseData['Landlord_address'] = $landLordData['address'];
                break;
            case 'broker':
                $LeaseData['landlord_user_id'] = $landLordData['id'];
                $LeaseData['Landlord_email'] = $landLordData['email'];
                $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                $LeaseData['Landlord_address'] = $landLordData['address'];

                $LeaseData['RealEstate'] = 1;
                $LeaseData['RealEstate_email'] = $user->email;
                $LeaseData['RealEstate_FirstName'] = $user->FirstName;
                $LeaseData['RealEstate_LastName'] = $user->LastName;
                $LeaseData['RealEstate_PhoneNumber'] = $user->PhoneNumber;
                $LeaseData['RealEstate_address'] = $user->HomeAdress;

                $count = User::where('email', $landLordData['email'])->count();
                if ($count == 0) {
                    $u = new \stdClass();
                    $u->role = 'Landlord';
                    //      Mail::to($landLordData['email'])->send(new PleaseCreateAccount($u));


                }


                break;

            case 'landlord':
                $LeaseData['landlord_user_id'] = $user->id;
                $LeaseData['Landlord_email'] = $user->email;
                $LeaseData['Landlord_FirstName'] = $user->FirstName;
                $LeaseData['Landlord_LastName'] = $user->LastName;
                $LeaseData['Landlord_PhoneNumber'] = $user->PhoneNumber;
                $LeaseData['Landlord_address'] = $user->HomeAdress;

                break;

            case 'property_manager':

                $LeaseData['landlord_user_id'] = $landLordData['id'];
                $LeaseData['Landlord_email'] = $landLordData['email'];
                $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                $LeaseData['Landlord_address'] = $landLordData['address'];

                $LeaseData['PropertyManager'] = 1;
                $LeaseData['PropertyManager_email'] = $user->email;
                $LeaseData['PropertyManager_FirstName'] = $user->FirstName;
                $LeaseData['PropertyManager_LastName'] = $user->LastName;
                $LeaseData['PropertyManager_PhoneNumber'] = $user->PhoneNumber;
                $LeaseData['PropertyManager_address'] = $user->HomeAdress;

                $count = User::where('email', $landLordData['email'])->count();
                if ($count == 0) {
                    $u = new \stdClass();
                    $u->role = 'Landlord';
                    //                     Mail::to($landLordData['email'])->send(new PleaseCreateAccount($u));
                }

                break;

        }


        $LeaseData['tenant_email'] = '';
        $LeaseData['property_id'] = $CurrentProperty->id;
        $LeaseData['created_by'] = $user->id;
        unset($LeaseData['signature']);
        unset($LeaseData['property']);
        unset($LeaseData['LLrelationship']);
        unset($LeaseData['Trelationship']);
        unset($LeaseData['tenant']);
        unset($LeaseData['LandLord']);
        unset($LeaseData['AdditionalOccupantsArray']);
        unset($LeaseData['AdditionalBrokersArray']);


        try {
            $lease = Leases::create(
                $LeaseData
            );
        } catch (\Exception $e) {
            return response()->json(['error' => true, 'msg' => 'Cant  create Lease.' . $e->getMessage()]);
        }


        if (!empty($lease)) {

            if (!empty($lease->PropertyManager_email)) {


                $count = User::where('email', $lease->PropertyManager_email)->count();
                if ($count == 0) {
                    $u = new \stdClass();
                    $u->role = 'Property manager';
                    $u->email = $lease->PropertyManager_email;
                    //   Mail::to($lease->PropertyManager_email)->send(new PleaseCreateAccount($u));
                } else {
                    $u = false;
                }

                $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                Mail::to($lease->PropertyManager_email)->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

            }
            if (!empty($lease->RealEstate_email)) {

                $count = User::where('email', $lease->RealEstate_email)->count();
                if ($count == 0) {
                    $u = new \stdClass();
                    $u->role = 'Real estate broker';
                    $u->email = $lease->RealEstate_email;
                    //      Mail::to($lease->RealEstate_email)->send(new PleaseCreateAccount($u));
                } else {
                    $u = false;
                }

                $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                Mail::to($lease->RealEstate_email)->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

            }


            $countOccupants = 0;
            foreach ($request->AdditionalOccupantsArray as $occupant) {


                try {
                    AdditionalOccupants::create([
                        'lease_id' => $lease->id,
                        'FirstName' => isset($occupant['AdditionalOccupants_FirstName']) ? $occupant['AdditionalOccupants_FirstName'] : '',
                        'LastName' => isset($occupant['AdditionalOccupants_LastName']) ? $occupant['AdditionalOccupants_LastName'] : '',
                        'email' => isset($occupant['AdditionalOccupants_email']) ? $occupant['AdditionalOccupants_email'] : '',
                        'PhoneNumber' => isset($occupant['AdditionalOccupants_PhoneNumber']) ? $occupant['AdditionalOccupants_PhoneNumber'] : '',
                        'address' => isset($occupant['AdditionalOccupants_address']) ? $occupant['AdditionalOccupants_address'] : '',
                        'isLead' => isset($occupant['isLead']) ? $occupant['isLead'] : 0
                    ]);

                    $count = User::where('email', $occupant['AdditionalOccupants_email'])->count();
                    if ($count == 0) {
                        $u = new \stdClass();
                        $u->role = 'tenant';
                        //                 Mail::to($occupant['AdditionalOccupants_email'])->send(new PleaseCreateAccount($u));
                    }

                } catch (\Exception $e) {
                    return response()->json(['error' => true, 'msg' => 'Cant  create Tenant.' . $e->getMessage()]);
                }


                $countOccupants++;
                if (isset($occupant['AdditionalOccupants_email'])) {
                    $count = User::where('email', $occupant['AdditionalOccupants_email'])->count();
                    if ($count == 0) {
                        $u = new \stdClass();
                        $u->role = 'Tenant';
                        $u->email = $occupant['AdditionalOccupants_email'];
                    } else {
                        $u = false;
                    }

                    $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                    Mail::to($occupant['AdditionalOccupants_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));
                }

            }
            if (isset($request->AdditionalBrokersArray)) {
                foreach ($request->AdditionalBrokersArray as $broker) {

                    try {


                        AdditionalBrokers::create([
                            'lease_id' => $lease->id,
                            'FirstName' => isset($broker['RealEstate_Additional_FirstName']) ? $broker['RealEstate_Additional_FirstName'] : '',
                            'LastName' => isset($broker['RealEstate_Additional_LastName']) ? $broker['RealEstate_Additional_LastName'] : '',
                            'Jurisdiction' => isset($broker['RealEstate_Additional_Jurisdiction']) ? $broker['RealEstate_Additional_Jurisdiction'] : '',
                            'email' => isset($broker['RealEstate_Additional_email']) ? $broker['RealEstate_Additional_email'] : '',
                            'PhoneNumber' => isset($broker['RealEstate_Additional_PhoneNumber']) ? $broker['RealEstate_Additional_PhoneNumber'] : '',
                            'AreaCode' => '',
                            'Address' => isset($broker['RealEstate_Additional_address']) ? $broker['RealEstate_Additional_address'] : '',
                            'relationToLandlord' => isset($broker['relationToLandlord']) ? $broker['relationToLandlord'] : '',
                            'relationToTenant' => isset($broker['relationToTenant']) ? $broker['relationToTenant'] : '',

                            'isLead' => isset($broker['isLead']) ? $broker['isLead'] : 0
                        ]);

                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'msg' => 'Cant  create Broker.' . $e->getMessage()]);
                    }


                    if (isset($broker['RealEstate_Additional_email'])) {
                        $count = User::where('email', $broker['RealEstate_Additional_email'])->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Real estate broker ';
                            $u->email = $broker['RealEstate_Additional_email'];
                        } else {
                            $u = false;
                        }


                        $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                        Mail::to($broker['RealEstate_Additional_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

                    }


                }

            }
            $lease->AdditionalOccupants_number = $countOccupants;
            $lease->save();

        }


        /*
        $LeaseData['landlord_user_id'] = $user->id;
        $LeaseData['tenant_email'] = '';
        $LeaseData['property_id'] = $CurrentProperty->id;
        $LeaseData['created_by'] = $user->id;
        try {
            Leases::create(
                $LeaseData
            );
        } catch (QueryException $e) {
            $t = $e->getMessage();
            return response()->json(['error' => true, 'msg' => $t]);
        }
*/
        return response()->json(['error' => false, 'msg' => 'Created']);


    }


    public function getForms(Request $request, JWTAuth $JWTAuth)
    {

        $user = HelperController::GetCurrentUser($request, $JWTAuth);


        switch ($user->role) {
            case 'tenant':
                $sql = "SELECT leases.id AS Id, leases.IsCustom, leases.customLease_file AS LL_customLease_file, properties.*, additional_occupants.email AS tenant_email,
 leases.T_sign, leases.LL_sign,leases.B_sign, leases.B_A_sign, leases.PM_sign, leases.created_by,
  leases.PropertyManager, leases.RealEstate, leases.RealEstate_Additional, leases.deleted_at
FROM
 leases
LEFT JOIN properties ON 
 properties.id =leases.property_id
LEFT JOIN additional_occupants ON
				 additional_occupants.lease_id=leases.id
WHERE additional_occupants.email=?


                ";
                $Forms = DB::select($sql, [$user->email]);


                break;

            case 'landlord':

                $sql = "SELECT leases.id AS Id, leases.IsCustom, leases.customLease_file AS LL_customLease_file, properties.* , additional_occupants.email AS tenant_email,
                 leases.T_sign, leases.LL_sign,leases.B_sign, leases.B_A_sign, leases.PM_sign, leases.created_by,
                 
                leases.PropertyManager, leases.RealEstate, leases.RealEstate_Additional, leases.deleted_at
                 
FROM
 leases
LEFT JOIN properties ON 
 properties.id =leases.property_id
LEFT JOIN additional_occupants ON
				 additional_occupants.lease_id=leases.id
WHERE leases.landlord_user_id =? AND (additional_occupants.isLead =1 OR additional_occupants.isLead IS NULL)


                                    ";

                $Forms = DB::select($sql, [$user->id]);


                break;

            case 'broker':

                $sql = "
                   SELECT leases.id AS Id, leases.IsCustom, leases.customLease_file AS LL_customLease_file, properties.*,
                 leases.T_sign, leases.LL_sign,leases.B_sign, leases.B_A_sign, leases.PM_sign, leases.created_by,
                 
                leases.PropertyManager, leases.RealEstate, leases.RealEstate_Additional, leases.deleted_at
                    FROM
                     leases
                    LEFT JOIN properties ON 
                     properties.id =leases.property_id
                 
                    LEFT JOIN LeaseInvites ON
                     LeaseInvites.lease_id =leases.id
                    WHERE leases.RealEstate_email=?  OR  leases.RealEstate_Additional_email=?
                    
                                    ";

                $Forms = DB::select($sql, [$user->email, $user->email]);


                break;

            case 'property_manager':

                $sql = "
                
               SELECT leases.id AS Id, leases.IsCustom, leases.customLease_file AS LL_customLease_file, properties.*, 
                 leases.T_sign, leases.LL_sign,leases.B_sign, leases.B_A_sign, leases.PM_sign, leases.created_by,
                 
                leases.PropertyManager, leases.RealEstate, leases.RealEstate_Additional, leases.deleted_at
                 FROM
                     leases
                    LEFT JOIN properties ON 
                     properties.id =leases.property_id
                 
                    LEFT JOIN LeaseInvites ON
                     LeaseInvites.lease_id =leases.id
                    WHERE leases.PropertyManager_email=?
                    
                                    ";

                $Forms = DB::select($sql, [$user->email]);


                break;


        }


        foreach ($Forms as $curForm) {
            if(!empty($curForm->deleted_at)){
                $curForm->terminated=1;
            }else{
                $curForm->terminated=0;
            }
            $invetedArr = [];
            $ocupants = AdditionalOccupants::where('lease_id', $curForm->Id)->get(['email']);
            $formAutor = User::find($curForm->created_by);
            if (empty($formAutor)) {
                $autorRole = '';
            } else {
                $invetedArr[] = $formAutor->email;
                $autorRole = $formAutor->role;
            }
            foreach ($ocupants as $ocupant) {
                $invetedArr[] = $ocupant->email;
            }
            $Lease = Leases::find($curForm->Id);

            if (!empty($Lease->Landlord_email)) {
                if ($autorRole != 'landlord') {
                    $invetedArr[] = $Lease->Landlord_email;
                }

            }


            if (!empty($Lease->PropertyManager_email)) {
                if ($autorRole != 'property_manager') {
                    $invetedArr[] = $Lease->PropertyManager_email;
                }
            }

            if (!empty($Lease->RealEstate_email)) {
                if ($autorRole != 'broker') {
                    $invetedArr[] = $Lease->RealEstate_email;
                }
            }

            $brokers =  AdditionalBrokers::where('lease_id', $curForm->Id)->get(['email']);
            foreach ($brokers as $broker) {
                $invetedArr[] = $broker->email;
            }



            $curForm->Parties = 0;
            if (!empty($curForm->tenant_email)) {
                $curForm->Parties = 1;
            }

            if (!isset($curForm->state)) {
                $curForm->state = ' ';
            }
            if (!isset($curForm->city)) {
                $curForm->city = ' ';
            }
            if (!isset($curForm->address1)) {
                $curForm->address1 = ' ';
            }
            $curForm->address = $curForm->address1 . ', ' . $curForm->city . ', ' . $curForm->zip . ',' . $this->countryDecode($curForm->country);
            $curForm->status = 'new';

            $curForm->AllSigns = 1;

            if ($curForm->T_sign == null) {
                $curForm->T_sign = 0;
                $curForm->AllSigns = 0;
            } else {
                $curForm->T_sign = 1;
            }

            if ($curForm->LL_sign == null) {
                $curForm->LL_sign = 0;
                $curForm->AllSigns = 0;
            } else {
                $curForm->LL_sign = 1;
            }

            if ($curForm->RealEstate == 1) {
                if ($curForm->B_sign == null) {
                    $curForm->B_sign = 0;
                    $curForm->AllSigns = 0;
                } else {
                    $curForm->B_sign = 1;
                }
            } else {
                $curForm->B_sign = -1;
            }


            if ($curForm->RealEstate_Additional == 1) {

                if ($curForm->B_A_sign == null) {
                    $curForm->B_A_sign = 0;
                    $curForm->AllSigns = 0;
                } else {
                    $curForm->B_A_sign = 1;
                }
            } else {
                $curForm->B_A_sign = -1;
            }


            if ($curForm->PropertyManager == 1) {
                if ($curForm->PM_sign == null) {
                    $curForm->PM_sign = 0;
                    $curForm->AllSigns = 0;
                } else {
                    $curForm->PM_sign = 1;
                }
            } else {
                $curForm->PM_sign = -1;
            }


            $curForm->InvitedList = join("<br>", $invetedArr);
            /*
                        switch ($curForm->status) {

                            //      case 'HasChangesByTenant':
                            case 'comments':
                                // 'rejected'
                                $curForm->Parties = 2;

                                break;

                            case 'approved':
                                $curForm->Parties = 3;

                                break;
                        }
                        */


        }


        return response()->json(['error' => false, 'Forms' => $Forms]);


    }

    public function deleteForm(Request $request, JWTAuth $JWTAuth)
    {
        if (!isset($request->formId)) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->formId);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {
            if ($Lease->landlord_user_id == $user->id) {
                $Lease->deleted_at=date('Y-m-d H:i:s');
                $Lease->save();
                return response()->json(['error' => false, 'msg' => 'Terminated']);
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant terminate this lease']);
            }
        }
    }

    public function terminateForm(Request $request, JWTAuth $JWTAuth)
    {
        if (!isset($request->formId)) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->formId);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {
            if ($Lease->landlord_user_id == $user->id) {
                $Lease->delete();
                return response()->json(['error' => false, 'msg' => 'deleted']);
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant delete this lease']);
            }
        }
    }

    public function rejectForm(Request $request, JWTAuth $JWTAuth)
    {
        if (!isset($request->formId)) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->formId);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {
            $invites = LeasesInvites::where('lease_id', $request->formId)->where('invited_email', $user->email)->first();
            if (!empty($invites)) {
                $invites->invitedStatus = 'rejected';
                $invites->save();
                return response()->json(['error' => false, 'msg' => 'rejected']);
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant reject this lease']);
            }
        }
    }

    public function approveForm(Request $request, JWTAuth $JWTAuth)
    {
        if (!isset($request->form['id'])) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        //      if ($user->role <> 'landlord') {

        if (empty($request->form['signature']['dataUrl'])) {
            return response()->json(['error' => true, 'msg' => 'You must sign document']);
        }
        //           if (empty($request->form['LLSigned_date'])) {
        $signDate = date('y-m-d');
        //          } else {
        //              $signDate = $request->form['LLSigned_date'];
        //          }

        //   }

        $Lease = Leases::find($request->form['id']);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {

            switch ($user->role) {
                case'landlord':

                    if ($Lease->Landlord_email == $user->email) {
                        $Lease->LL_sign = $request->form['signature']['dataUrl'];
                        $Lease->LLSigned_date = $signDate;
                        $Lease->save();
                        return response()->json(['error' => false, 'msg' => 'approved']);
                    } else {
                        return response()->json(['error' => true, 'msg' => 'You cant approve this lease']);
                    }


                    break;

                case 'tenant':

                    $tenant = AdditionalOccupants::where('lease_id', $request->form['id'])->where('email', $user->email)->first();
                    if (!empty($tenant)) {
                        //            $invites->invitedStatus = 'approved';
                        //           $invites->save();

                        $tenant->sign = $request->form['signature']['dataUrl'];
                        $tenant->SignedDate = $signDate;
                        $tenant->save();
                        return response()->json(['error' => false, 'msg' => 'approved']);
                    } else {
                        return response()->json(['error' => true, 'msg' => 'You cant approve this lease']);
                    }
                    break;

                case 'broker':
                    if ($Lease->RealEstate_email == $user->email || $Lease->RealEstate_Additional_email == $user->email) {

                        if ($Lease->RealEstate_email == $user->email) {
                            $Lease->B_sign = $request->form['signature']['dataUrl'];
                            $Lease->BSigned_date = $signDate;
                            $Lease->save();


                        } else {
                            $Lease->B_A_sign = $request->form['signature']['dataUrl'];
                            $Lease->B_ASigned_date = $signDate;
                            $Lease->save();

                        }


                        return response()->json(['error' => false, 'msg' => 'signed']);

                    } else {
                        return response()->json(['error' => true, 'msg' => 'You cant sign this lease']);
                    }

                    break;

                case 'property_manager':
                    if ($Lease->PropertyManager_email == $user->email) {
                        $Lease->PM_sign = $request->form['signature']['dataUrl'];
                        $Lease->PMSigned_date = $signDate;
                        $Lease->save();
                        return response()->json(['error' => false, 'msg' => 'signed']);
                    } else {
                        return response()->json(['error' => true, 'msg' => 'You cant sign this lease']);
                    }


                    break;


            }


        }
    }


    public function inviteUser(Request $request, JWTAuth $JWTAuth)
    {
        if (!isset($request->formId) AND !isset($request->email)) {
            return response()->json(['error' => true, 'msg' => 'Wrong data']);
        }

        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->formId);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found lease']);
        } else {


            $AdditionalOccupants = AdditionalOccupants::where('email', $request->email)->where('lease_id', $Lease->id)->first();

            if (!empty($AdditionalOccupants)) {
                try {
                    Mail::to($request->email)->send(new InviteManualy($request->formId));
                    return response()->json(['error' => false, 'msg' => 'We send invite to tenant']);
                } catch (\Exception $e) {
                    return response()->json(['error' => true, 'msg' => $e->getMessage()]);

                }


            } else {

                if ($Lease->PropertyManager_email == $request->email) {

                    try {
                        Mail::to($request->email)->send(new InviteManualy($request->formId));
                        return response()->json(['error' => false, 'msg' => 'We send invite to PM']);
                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'msg' => $e->getMessage()]);

                    }

                } else if ($Lease->RealEstate_email == $request->email) {

                    try {
                        Mail::to($request->email)->send(new InviteManualy($request->formId));
                        return response()->json(['error' => false, 'msg' => 'We send invite to Broker']);
                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'msg' => $e->getMessage()]);

                    }


                } else if ($Lease->RealEstate_Additional_email == $request->email) {

                    try {
                        Mail::to($request->email)->send(new InviteManualy($request->formId));
                        return response()->json(['error' => false, 'msg' => 'We send invite to  Additional Broker']);
                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'msg' => $e->getMessage()]);

                    }


                } else {

                    return response()->json(['error' => true, 'msg' => 'Email that you giv not added to selected lease']);


                }


            }


            /*
            if ($Lease->landlord_user_id == $user->id) {
                //@todo send email
                //   $Lease->tenant_email = $request->email;
                //     $Lease->save();

                $invites = LeasesInvites::where('lease_id', $request->formId)->where('invited_email', $request->email)->first();
                if (empty($invites)) {
                    LeasesInvites::create(
                        [
                            'lease_id' => $request->formId,
                            'invited_email' => $request->email,
                            'invitedStatus' => 'new',
                        ]
                    );
                    return response()->json(['error' => false, 'msg' => 'Invite sended ']);
                } else {
                    return response()->json(['error' => true, 'msg' => 'this user already invited ']);
                }
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant send invite for this lease']);
            }

            */
        }

    }


    public function getFormById(Request $request, JWTAuth $JWTAuth)
    {

        if (!isset($request->formId)) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->formId);
        $AllTenants = [];
        $leadTenant = [];
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {
            $signedByMe = false;
            switch ($user->role) {
                case 'landlord':

                    if ($Lease->landlord_user_id == $user->id) {

                        $form = $Lease->toArray();
                        $property = Properties::find($Lease->property_id);
                        $form['property'] = $property;
                        $form['status'] = 'new';
                        $form['TenantComment'] = '';
                        if (!empty($Lease->LLSigned_date)) {
                            $signedByMe = true;
                        }
                    } else {
                        $form = [];
                    }

                    break;

                case'tenant':

                    $occupant = AdditionalOccupants::where('lease_id', $request->formId)->where('email', $user->email)->first();
                    if (!empty($occupant)) {

                        $form = $Lease->toArray();
                        $property = Properties::find($Lease->property_id);
                        $form['property'] = $property;
                        $form['status'] = 'status1';
                        $form['TenantComment'] = '';
                        $occupants = AdditionalOccupants::where('lease_id', $Lease->id)->where('email', $user->email)->first();

                        if (!empty($occupants) AND !empty($occupants->sign)) {
                            $signedByMe = true;
                        }


                    } else {
                        $form = [];
                    }

                    break;


                case 'broker':

                    if ($Lease->RealEstate_email == $user->email || $Lease->RealEstate_Additional_email == $user->email) {

                        $form = $Lease->toArray();
                        $property = Properties::find($Lease->property_id);
                        $form['property'] = $property;
                        $form['status'] = 'new';
                        $form['TenantComment'] = '';
                        if (!empty($Lease->B_sign)) {
                            $signedByMe = true;
                        }


                    } else {

                        $broker = AdditionalBrokers::where('lease_id', $request->formId)->where('email', $user->email)->first();
                        if (!empty($broker)) {

                            $form = $Lease->toArray();
                            $property = Properties::find($Lease->property_id);
                            $form['property'] = $property;
                            $form['status'] = 'status1';
                            $form['TenantComment'] = '';
                            $occupants = AdditionalOccupants::where('lease_id', $Lease->id)->where('email', $user->email)->first();

                            if (!empty($occupants) AND !empty($occupants->sign)) {
                                $signedByMe = true;
                            }


                        } else {
                            $form = [];
                        }





                    }


                    break;


                case 'property_manager':

                    if ($Lease->PropertyManager_email == $user->email) {

                        $form = $Lease->toArray();
                        $property = Properties::find($Lease->property_id);
                        $form['property'] = $property;
                        $form['status'] = 'new';
                        $form['TenantComment'] = '';


                        if (!empty($Lease->PM_sign)) {
                            $signedByMe = true;
                        }

                    } else {
                        $form = [];
                    }


                    break;


            }


            if (empty($form)) {
                return response()->json(['error' => true, 'msg' => 'You cant see this lease']);
            } else {

                $AllSigns = [];
                /*   $landlord = User::find($Lease->landlord_user_id);

                   if (empty($Lease->Landlord_FirstName)) {
                       $Fn = !empty($landlord) ? $landlord->FirstName : 'John';
                       $Ln = !empty($landlord) ? $landlord->LastName : ' ';
                   } else {
                       $Fn = $Lease->Landlord_FirstName;
                       $Ln = '';
                   }
   */
                $Fn = $Lease->Landlord_FirstName;
                $Ln = $Lease->Landlord_LastName;


                $name = $Fn . ' ' . $Ln;
                $AllSigns [] = [
                    'role' => 'Landlord',
                    'Name' => $name,
                    'date' => $Lease->LLSigned_date,
                    'sign' => $Lease->LL_sign
                ];

                /*     $PM = User::where('email', $Lease->PropertyManager_email)->first();
                     if (!empty($PM)) {

                         if (empty($Lease->PropertyManager_FirstName)) {
                             $Fn = $PM->FirstName;
                             $Ln = $PM->LastName;
                         } else {
                             $Fn = $Lease->PropertyManager_FirstName;
                             $Ln = '';
                         }

                         $name = $Fn . ' ' . $Ln;
                         //       $name = !empty($PM) ? $PM->FirstName . ' ' . $PM->LastName : 'John Doe';
                         $AllSigns [] = [
                             'role' => 'Property Manager',
                             'Name' => $name,
                             'date' => $Lease->PMSigned_date,
                             'sign' => $Lease->PM_sign
                         ];
                     } else {
                         if (!empty($Lease->PropertyManager_email)) {

                             $Fn = $Lease->PropertyManager_FirstName;
                             $Ln = $Lease->PropertyManager_LastName;
                             $name = $Fn . ' ' . $Ln;
                             $AllSigns [] = [
                                 'role' => 'Property Manager',
                                 'Name' => $name,
                                 'date' => $Lease->PMSigned_date,
                                 'sign' => $Lease->PM_sign
                             ];

                         }

                     }

                     */
                if (!empty($Lease->PropertyManager_email)) {
                    $Fn = $Lease->PropertyManager_FirstName;
                    $Ln = $Lease->PropertyManager_LastName;
                    $name = $Fn . ' ' . $Ln;
                    $AllSigns [] = [
                        'role' => 'Property Manager',
                        'Name' => $name,
                        'date' => $Lease->PMSigned_date,
                        'sign' => $Lease->PM_sign
                    ];
                }


                /*      $broker = User::where('email', $Lease->RealEstate_email)->first();
                      if (!empty($broker)) {

                          if (empty($Lease->RealEstate_FirstName)) {
                              $Fn = $broker->FirstName;
                              $Ln = $broker->LastName;
                          } else {
                              $Fn = $Lease->RealEstate_FirstName;
                              $Ln = $Lease->RealEstate_LastName;
                          }

                          $name = $Fn . ' ' . $Ln;
                          //      $name = !empty($broker) ? $broker->FirstName . ' ' . $broker->LastName : 'John Doe';
                          $AllSigns [] = [
                              'role' => 'Broker',
                              'Name' => $name,
                              'date' => $Lease->BSigned_date,
                              'sign' => $Lease->B_sign
                          ];
                      } else {
                          if (!empty($Lease->RealEstate_email)) {

                              $Fn = $Lease->RealEstate_FirstName;
                              $Ln = $Lease->RealEstate_LastName;
                              $name = $Fn . ' ' . $Ln;
                              $AllSigns [] = [
                                  'role' => 'Broker',
                                  'Name' => $name,
                                  'date' => $Lease->BSigned_date,
                                  'sign' => $Lease->B_sign
                              ];

                          }

                      }
                      */

                if (!empty($Lease->RealEstate_email)) {

                    $Fn = $Lease->RealEstate_FirstName;
                    $Ln = $Lease->RealEstate_LastName;
                    $name = $Fn . ' ' . $Ln;
                    $AllSigns [] = [
                        'role' => 'Broker',
                        'Name' => $name,
                        'date' => $Lease->BSigned_date,
                        'sign' => $Lease->B_sign
                    ];

                }

                $AdditionalBrokers = AdditionalBrokers::where('lease_id', $Lease->id)->get();

                foreach ($AdditionalBrokers as $broker) {

                        $AllSigns [] = [
                            'role' => 'Additional broker',
                            'Name' => $broker->FirstName . ' ' . $broker->LastName,
                            'date' => $broker->SignedDate,
                            'sign' => $broker->sign
                        ];
                 }




                $occupants = AdditionalOccupants::where('lease_id', $Lease->id)->get();


                foreach ($occupants as $tenant) {
                    if ($tenant->isLead == 1) {
                        $leadTenant = $tenant;
                        $AllSigns [] = [
                            'role' => 'Lead Tenant',
                            'Name' => $tenant->FirstName . ' ' . $tenant->LastName,
                            'date' => $tenant->SignedDate,
                            'sign' => $tenant->sign
                        ];
                    } else {
                        $AllTenants[] = $tenant;
                        $AllSigns [] = [
                            'role' => 'Tenant',
                            'Name' => $tenant->FirstName . ' ' . $tenant->LastName,
                            'date' => $tenant->SignedDate,
                            'sign' => $tenant->sign
                        ];
                    }


                }


                $form['AllSigns'] = $AllSigns;


                if (!empty($Lease->created_by)) {

                    $userInitiator = User::find($Lease->created_by);

                    $roleFormated = '';
                    switch ($userInitiator->role) {

                        case 'property_manager':
                            $roleFormated = 'Property Manager';
                            break;

                        case 'broker':
                            $roleFormated = 'Real Estate Broker';
                            break;

                        case 'landlord':
                            $roleFormated = 'Landlord';
                            break;

                        case 'admin':
                            $roleFormated = 'Admin';

                            break;

                    }


                    if (!empty($userInitiator)) {

                        $FormInitiator = [
                            'role' => $roleFormated,
                            'FullName' => $userInitiator->name,
                            'email' => $userInitiator->email,
                            'PhoneNumber' => $userInitiator->PhoneNumber
                        ];
                    } else {

                        $FormInitiator = [
                            'role' => 'N/a',
                            'FullName' => '',
                            'email' => '',
                            'PhoneNumber' => ''
                        ];


                    }


                } else {

                    $FormInitiator = [
                        'role' => 'N/a',
                        'FullName' => '',
                        'email' => '',
                        'PhoneNumber' => ''
                    ];

                }

                $form['property']['country'] = $this->countryDecode($form['property']['country']);

                $sql = "SELECT users.name, users.FirstName, users.LastName, comments.`Comment`, comments.created_at, comments.user_id
FROM comments
LEFT JOIN users ON comments.user_id =users.id
WHERE comments.lease_id =?";
                $Comments = DB::Select($sql, [$Lease->id]);
                $allComments = [];
                $userCommented = false;
                foreach ($Comments as $comment) {

                    if ($comment->user_id == $user->id) {
                        $userCommented = true;
                    }
                    $date = Carbon::parse($comment->created_at);
                    $allComments[] = [

                        'Name' => $comment->name,
                        'date' => $date->diffForHumans(),
                        'text' => $comment->Comment,


                    ];

                }
                //       $comment = Comments::where('lease_id',$Lease->id)->get();

                /*

                                if(isset($comment->Comment)){
                                    $form['TenantComment']=$comment->Comment;
                                }else{
                                    $form['TenantComment']='';
                                }
                */
                return response()->json(['error' => false, 'msg' => 'loaded', 'form' => $form, 'FormInitiator' => $FormInitiator, 'leadTenant' => $leadTenant, 'AllBrokers'=>$AdditionalBrokers, 'AllTenants' => $AllTenants, 'AllComments' => $allComments, 'userCommented' => $userCommented, 'signedByMe' => $signedByMe]);
            }


        }

    }


    private function isDate($value)
    {
        if (!$value) {
            return false;
        }
        if (!is_string($value)) {
            return false;
        }

        try {
            $v = new \DateTime($value);
            $ts = strtotime($value);
            $dated = date('Y-m-d', $ts);
            return $dated;
         //   return true;
        } catch (\Exception $e) {
            return false;
        }
    }


    public function uploadCustomLease(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        $file = $request->file('file');
        if ($file->isValid()) {
            $store = Storage::disk('uploads')->putFileAs(
                'customLease', $request->file('file'), 'Lease_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension()
            );

            $invites = LeasesInvites::where('lease_id', $request->LeaseId)->where('invited_email', $user->email)->first();


            if ($store) {
                if (!empty($invites)) {
                    $invites->customLease_file = 'Lease_' . $request->user()->id . '.' . $request->file('file')->getClientOriginalExtension();
                    $invites->save();
                    return response()->json(['error' => false, 'msg' => 'Inserted ']);
                } else {
                    return response()->json(['error' => true, 'msg' => 'User not found']);
                }


            } else {
                return response()->json(['error' => true, 'msg' => 'Cant insert record']);
            }
        }
    }


    public function getTemplates(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        if ($user->role == 'landlord') {
            $sql = "SELECT leases.id,  properties.address, properties.unit, leases.created_at
FROM
 leases
LEFT JOIN properties ON 
 properties.id =leases.property_id
WHERE leases.landlord_user_id =? AND saveASTemplate=1
                                    ";

            $Forms = DB::select($sql, [$user->id]);
        } else {
            return response()->json(['error' => false, 'AvaliableTemplates' => []]);
        }


        foreach ($Forms as $curForm) {
            $curForm->text = $curForm->address . ' ' . $curForm->unit . '@ ' . $curForm->created_at;

        }

        return response()->json(['error' => false, 'AvaliableTemplates' => $Forms]);

    }


    public function saveWithComment(Request $request, JWTAuth $JWTAuth)
    {

        if (!isset($request->form['id'])) {
            abort(404);
        }
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $Lease = Leases::find($request->form['id']);
        if (empty($Lease)) {
            return response()->json(['error' => true, 'msg' => 'Not found']);
        } else {

            if ($this->isUserAttachedToContract($user->email, $Lease)) {

                $comment = Comments::where('lease_id', $Lease->id)->where('user_id', $user->id)->first();
                if (empty($comment)) {
                    $comment = Comments::create([
                        'lease_id' => $Lease->id,
                        'user_id' => $user->id,
                        'Comment' => $request->form['TenantComment']
                    ]);

                } else {
                    $comment->Comment = $request->form['TenantComment'];
                    $comment->save();

                }


                return response()->json(['error' => false, 'msg' => 'Comment Saved']);
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant approve this lease']);
            }


            /*
            $occupant = AdditionalOccupants::where('lease_id', $request->form['id'])->where('email', $user->email)->first();

            if (!empty($occupant)) {
                $occupant->comment = $request->form['TenantComment'];

                $occupant->save();
                if (isset($request->form['signature']['dataUrl'])) {
                    if (empty($request->form['LLSigned_date'])) {
                        $signDate = date('y-m-d');
                    } else {
                        $signDate = $request->form['LLSigned_date'];
                    }

                    switch ($user->role) {

                        case 'tenant':
                            $occupant->sign = $request->form['signature']['dataUrl'];
                            $occupant->SignedDate = $signDate;
                            $occupant->save();

                            break;

                        case 'broker':
                            if ($Lease->RealEstate_email == $user->email || $Lease->RealEstate_Additional_email == $user->email) {

                                if ($Lease->RealEstate_email == $user->email) {
                                    $Lease->B_sign = $request->form['signature']['dataUrl'];
                                    $Lease->BSigned_date = $signDate;
                                    $Lease->save();


                                } else {
                                    $Lease->B_A_sign = $request->form['signature']['dataUrl'];
                                    $Lease->B_ASigned_date = $signDate;
                                    $Lease->save();

                                }

                            } else {
                                return response()->json(['error' => true, 'msg' => 'You cant sign this lease']);
                            }

                            break;

                        case 'property_manager':
                            if ($Lease->PropertyManager_email == $user->email) {
                                $Lease->PM_sign = $request->form['LL_sign'];
                                $Lease->PMSigned_date = $signDate;
                                $Lease->save();
                            } else {
                                return response()->json(['error' => true, 'msg' => 'You cant sign this lease']);
                            }


                            break;
                    }


                }


                return response()->json(['error' => false, 'msg' => 'approved']);
            } else {
                return response()->json(['error' => true, 'msg' => 'You cant approve this lease']);
            }
            */
        }

    }


    public function newFormtoUploaded(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);
        $requestData = $request->all();


        $validator = Validator::make($requestData, [
            'moveInDate' => 'required|date',
            'monthAmount' => 'required|numeric',
            'LateFee' => 'required|numeric',
            'property.address1' => 'required',

            'property.city' => 'required',
            'property.country' => 'required',
            'property.state' => 'required',
            'property.zip' => 'required',

        ]);

        if ($validator->fails()) {

            $msg = $validator->errors()->all();
            return response()->json(['error' => true, 'msg' => $msg]);
        }


        if (isset($requestData['property'])) {
            $property = (array)$requestData['property'];
        } else {
            $property = [];
        }


        if (empty($property)) {
            return response()->json(['error' => true, 'msg' => 'Please Fill Out All The Property Fields']);
        }
        $lease = Leases::where('id', $requestData['leaseId'])->first();
        if (empty($lease)) {
            return response()->json(['error' => true, 'msg' => 'Cant  create Lease']);
        } else {


            if ($user->role != 'landlord') {
                if (isset($request->form['LandLord']['email'])) {
                    $landlord = User::where('email', $request->form['LandLord']['email'])->first();

                    if (empty($landlord)) {
                        $landLordData = [
                            'id' => 0,
                            'email' => $request->form['LandLord']['email'],
                            'FirstName' => isset($request->form['LandLord']['FirstName']) ? $request->form['LandLord']['FirstName'] : '',
                            'LastName' => isset($request->form['LandLord']['LastName']) ? $request->form['LandLord']['LastName'] : '',
                            'PhoneNumber' => isset($request->form['LandLord']['PhoneNumber']) ? $request->form['LandLord']['PhoneNumber'] : '',
                            'address'=>isset($request->form['LandLord']['address']) ? $request->form['LandLord']['address'] : '',

                        ];

                    } else {
                        if ($landlord->role == 'landlord') {
                            $landLordData = [
                                'id' => $landlord->id,
                                'email' => $request->form['LandLord']['email'],
                                'FirstName' => $request->form['LandLord']['FirstName'],
                                'LastName' => $landlord->LastName,
                                'PhoneNumber' => isset($request->form['LandLord']['PhoneNumber']) ? $request->form['LandLord']['PhoneNumber'] : '',
                                'address'=>isset($request->form['LandLord']['address']) ? $request->form['LandLord']['address'] : '',

                            ];
                        } else {
                            return response()->json(['error' => true, 'msg' => 'User with landrod email already registered but his role not landlord']);
                        }


                    }


                } else {
                    return response()->json(['error' => true, 'msg' => 'Landlord email cant be empty']);
                }


            }


            switch ($user->role) {
                case 'tenant':
                    $LeaseData['landlord_user_id'] = $landLordData['id'];
                    $LeaseData['Landlord_email'] = $landLordData['email'];
                    $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                    $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                    $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                    $LeaseData['Landlord_address'] = $landLordData['address'];
                    break;
                case 'broker':
                    $LeaseData['landlord_user_id'] = $landLordData['id'];
                    $LeaseData['Landlord_email'] = $landLordData['email'];
                    $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                    $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                    $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                    $LeaseData['Landlord_address'] = $landLordData['address'];


                    break;

                case 'landlord':
                    $LeaseData['landlord_user_id'] = $user->id;
                    $LeaseData['Landlord_email'] = $user->email;
                    $LeaseData['Landlord_FirstName'] = $user->FirstName;
                    $LeaseData['Landlord_LastName'] = $user->LastName;
                    $LeaseData['Landlord_PhoneNumber'] = $user->PhoneNumber;
                    $LeaseData['Landlord_address'] = $user->HomeAdress;

                    break;

                case 'property_manager':

                    $LeaseData['landlord_user_id'] = $landLordData['id'];
                    $LeaseData['Landlord_email'] = $landLordData['email'];
                    $LeaseData['Landlord_FirstName'] = $landLordData['FirstName'];
                    $LeaseData['Landlord_LastName'] = $landLordData['LastName'];
                    $LeaseData['Landlord_PhoneNumber'] = $landLordData['PhoneNumber'];
                    $LeaseData['Landlord_address'] = $landLordData['address'];

                    break;

            }


            try {
                Properties::where('id', $lease->property_id)
                    ->update(
                        $property
                    );
                $CurrentProperty = Properties::find($lease->property_id);
                $moveInDate = new Carbon($request->moveInDate);

                $lease->moveInDate = $moveInDate->toDateString();
                $lease->Lease_monthAmount = $request->monthAmount;
                $lease->Lease_payFees = $request->LateFee;


                $lease->landlord_user_id = $LeaseData['landlord_user_id'];
                $lease->Landlord_email = $LeaseData['Landlord_email'];
                $lease->Landlord_FirstName = $LeaseData['Landlord_FirstName'];
                $lease->Landlord_LastName = $LeaseData['Landlord_LastName'];
                $lease->Landlord_PhoneNumber = $LeaseData['Landlord_PhoneNumber'];
                $lease->Landlord_address = $LeaseData['Landlord_address'];


                $lease->PropertyManager = isset($request->form['PropertyManager']) ? $request->form['PropertyManager'] : null;
                $lease->PropertyManager_FirstName = isset($request->form['PropertyManager_FirstName']) ? $request->form['PropertyManager_FirstName'] : null;
                $lease->PropertyManager_LastName = isset($request->form['PropertyManager_LastName']) ? $request->form['PropertyManager_LastName'] : null;
                $lease->PropertyManager_email = isset($request->form['PropertyManager_email']) ? $request->form['PropertyManager_email'] : null;
                $lease->PropertyManager_PhoneNumber = isset($request->form['PropertyManager_PhoneNumber']) ? $request->form['PropertyManager_PhoneNumber'] : null;
                $lease->PropertyManager_address = isset($request->form['PropertyManager_address']) ? $request->form['PropertyManager_address'] : null;

                $lease->RealEstate = isset($request->form['RealEstate']) ? $request->form['RealEstate'] : null;
                $lease->RealEstate_Jurisdiction = isset($request->form['RealEstate_Jurisdiction']) ? $request->form['RealEstate_Jurisdiction'] : null;
                $lease->RealEstate_FirstName = isset($request->form['RealEstate_FirstName']) ? $request->form['RealEstate_FirstName'] : null;
                $lease->RealEstate_LastName = isset($request->form['RealEstate_LastName']) ? $request->form['RealEstate_LastName'] : null;
                $lease->RealEstate_email = isset($request->form['RealEstate_email']) ? $request->form['RealEstate_email'] : null;
                $lease->RealEstate_PhoneNumber = isset($request->form['RealEstate_PhoneNumber']) ? $request->form['RealEstate_PhoneNumber'] : null;
                $lease->RealEstate_address = isset($request->form['RealEstate_address']) ? $request->form['RealEstate_address'] : null;
                $lease->RealEstate_LLrelationship = isset($request->form['RealEstate_LLrelationship']) ? $request->form['RealEstate_LLrelationship'] : null;
                $lease->RealEstate_Trelationship = isset($request->form['RealEstate_Trelationship']) ? $request->form['RealEstate_Trelationship'] : null;
                $lease->RealEstate_Additional = isset($request->form['RealEstate_Additional']) ? $request->form['RealEstate_Additional'] : null;


                $lease->save();


                /*

                                $leadTenant=[];

                foreach($request->form->AdditionalOccupantsArray as $occupants){
                    if(empty($leadTenant)){
                        $leadTenant= $occupants;
                    }

                    if($occupants->isLead=='true'){
                        $leadTenant= $occupants;
                    }


                    $LeaseData['tenant_user_id'] = $user->id;
                    $LeaseData['ParentLease_id'] = $request->Id;
                    Leases_tenants::create($LeaseData);

                }
                */
                foreach ($request->form['AdditionalOccupantsArray'] as $occupant) {
                    AdditionalOccupants::create([
                        'lease_id' => $lease->id,
                        'FirstName' => isset($occupant['AdditionalOccupants_FirstName']) ? $occupant['AdditionalOccupants_FirstName'] : '',
                        'LastName' => isset($occupant['AdditionalOccupants_LastName']) ? $occupant['AdditionalOccupants_LastName'] : '',
                        'email' => isset($occupant['AdditionalOccupants_email']) ? $occupant['AdditionalOccupants_email'] : '',
                        'PhoneNumber' => isset($occupant['AdditionalOccupants_PhoneNumber']) ? $occupant['AdditionalOccupants_PhoneNumber'] : '',
                        'address' => isset($occupant['AdditionalOccupants_address']) ? $occupant['AdditionalOccupants_address'] : '',
                        'isLead' => isset($occupant['isLead']) ? $occupant['isLead'] : 0
                    ]);
                    //                $countOccupants++;
                    if (isset($occupant['AdditionalOccupants_email'])) {
                        $count = User::where('email', $occupant['AdditionalOccupants_email'])->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Tenant';
                            $u->email = $occupant['AdditionalOccupants_email'];
                        } else {
                            $u = false;
                        }
                        $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                        Mail::to($occupant['AdditionalOccupants_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));
                    }

                }


                foreach ($request->form['AdditionalBrokersArray'] as $broker) {

                    try {


                        AdditionalBrokers::create([
                            'lease_id' => $lease->id,
                            'FirstName' => isset($broker['RealEstate_Additional_FirstName']) ? $broker['RealEstate_Additional_FirstName'] : '',
                            'LastName' => isset($broker['RealEstate_Additional_LastName']) ? $broker['RealEstate_Additional_LastName'] : '',
                            'Jurisdiction' => isset($broker['RealEstate_Additional_Jurisdiction']) ? $broker['RealEstate_Additional_Jurisdiction'] : '',
                            'email' => isset($broker['RealEstate_Additional_email']) ? $broker['RealEstate_Additional_email'] : '',
                            'PhoneNumber' => isset($broker['RealEstate_Additional_PhoneNumber']) ? $broker['RealEstate_Additional_PhoneNumber'] : '',
                            'AreaCode' => '',
                            'Address' => isset($broker['RealEstate_Additional_address']) ? $broker['RealEstate_Additional_address'] : '',
                            'relationToLandlord' => isset($broker['relationToLandlord']) ? $broker['relationToLandlord'] : '',
                            'relationToTenant' => isset($broker['relationToTenant']) ? $broker['relationToTenant'] : '',

                            'isLead' => isset($broker['isLead']) ? $broker['isLead'] : 0
                        ]);

                    } catch (\Exception $e) {
                        return response()->json(['error' => true, 'msg' => 'Cant  create Broker.' . $e->getMessage()]);
                    }


                    if (isset($broker['RealEstate_Additional_email'])) {
                        $count = User::where('email', $broker['RealEstate_Additional_email'])->count();
                        if ($count == 0) {
                            $u = new \stdClass();
                            $u->role = 'Real estate broker ';
                            $u->email = $broker['RealEstate_Additional_email'];
                        } else {
                            $u = false;
                        }


                        $propertyAdress = $CurrentProperty->address1 . ', ' . $CurrentProperty->city . ', ' . $CurrentProperty->zip . ',' . $this->countryDecode($CurrentProperty->country);
                        Mail::to($broker['RealEstate_Additional_email'])->send(new InviteTenant($lease->id, $user, $propertyAdress, $u));

                    }


                }







                return response()->json(['error' => false, 'msg' => 'Saved', 'leaseId' => $lease->id]);
            } catch (QueryException $e) {
                $t = $e->getMessage();
                return response()->json(['error' => true, 'msg' => $t]);
            }
        }
    }


    private function countryDecode($code)
    {
        $countryList = array(
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua and Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas the',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia and Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island (Bouvetoya)',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory (Chagos Archipelago)',
            'VG' => 'British Virgin Islands',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros the',
            'CD' => 'Congo',
            'CG' => 'Congo the',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote d\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FO' => 'Faroe Islands',
            'FK' => 'Falkland Islands (Malvinas)',
            'FJ' => 'Fiji the Fiji Islands',
            'FI' => 'Finland',
            'FR' => 'France, French Republic',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia the',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island and McDonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KP' => 'Korea',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyz Republic',
            'LA' => 'Lao',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'AN' => 'Netherlands Antilles',
            'NL' => 'Netherlands the',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn Islands',
            'PL' => 'Poland',
            'PT' => 'Portugal, Portuguese Republic',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts and Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre and Miquelon',
            'VC' => 'Saint Vincent and the Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome and Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia (Slovak Republic)',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia, Somali Republic',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia and the South Sandwich Islands',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard & Jan Mayen Islands',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland, Swiss Confederation',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad and Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks and Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States of America',
            'UM' => 'United States Minor Outlying Islands',
            'VI' => 'United States Virgin Islands',
            'UY' => 'Uruguay, Eastern Republic of',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Vietnam',
            'WF' => 'Wallis and Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe'
        );

        if (!isset($countryList[$code])) {
            return $code;
        } else {
            return $countryList[$code];
        }

    }


    private function isUserAttachedToContract($userEmail, $lease)
    {

        //check if tenant
        $additionalOccupant = AdditionalOccupants::where('lease_id', $lease->id)->where('email', $userEmail)->count();
        if ($additionalOccupant > 0) {
            return 'tenant';
        }
        //check if landlord

        if ($lease->Landlord_email == $userEmail) {
            return 'landlord';
        }


        //check if broker

        if ($lease->RealEstate_email == $userEmail || $lease->RealEstate_Additional_email == $userEmail) {
            return 'broker';
        }
        $additionalBrokers = AdditionalBrokers::where('lease_id', $lease->id)->where('email', $userEmail)->count();
        if ($additionalBrokers > 0) {
            return 'broker';
        }

        //check if PM
        if ($lease->PropertyManager_email == $userEmail) {
            return 'landlord';
        }

        //not found
        return false;
    }
}
