<?php

namespace App\Http\Controllers;

use App\Invoices;
use App\Leases;
use App\PaymentRequests;
use App\StripePayments;
use Illuminate\Http\Request;
use Coinbase\Wallet\Client;
use Coinbase\Wallet\Configuration;
use Coinbase\Wallet\Resource\Account;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client as GuzzleClient;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Payment;
use PayPal\Api\Details;
use PayPal\Api\PaymentExecution;
use PHPUnit\TextUI\ResultPrinter;
use PayPal\Api\Payout;
use PayPal\Api\PayoutSenderBatchHeader;
use PayPal\Api\PayoutItem;
use PayPal\Api\Currency;
use Illuminate\Support\Facades\DB;
use Stripe\Customer;
use Stripe\Stripe;
use Stripe\Account As StripeAccount;
use Tymon\JWTAuth\JWTAuth;


class PaymentController extends Controller
{
    public function ProcessWebHook(Request $request)
    {
        $sharedSecret = '53272eb9-b846-4385-a2a3-629539d1ab5c';
        $data = $request->all;
        $hashHeader = $request->header('X-CC-Webhook-Signature');
//        $generatedCode=hash_hmac('sha256',$data,$sharedSecret);
        Log::debug('Secret:' . $hashHeader);
//        Log::debug('generated code:' . $generatedCode);
        Log::debug('Get webhook:' . print_r($data, true));


        $userId = $data['userId'];
        $paymentRequest = PaymentRequests::where('user_id', $userId)->where('hostedPageCode', 'code')->first();
        if (empty($paymentRequest)) {
            Log::error('wrong webhook parametrs');
            //@todo sen alert message
        } else {
            $paymentRequest->state = 'confirmed';
            $paymentRequest->save();

        }

    }


    public function testConBase()
    {
        $configuration = Configuration::apiKey(env('COINBASEAPI_KEY'), env('COINBASEAPI_SECRET'));
        $client = Client::create($configuration);
        $accounts = $client->getAccounts();
        $data = $client->decodeLastResponse();

        $account = new Account([
            'name' => 'New Account'
        ]);
        $client->createAccount($account);
        $data = $client->decodeLastResponse();
        $paymentMethods = $client->getPaymentMethods();

        $data = $client->decodeLastResponse();

        /*   $data = $deposit->getRawData();
           $data = $client->decodeLastResponse();
           */
    }


    public function ProcessCallback(Request $request, JWTAuth $JWTAuth)
    {
        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        Log::debug(print_r($request->all(), true));
        if (!isset($request->code)) {
            abort(404);
        }

        $client = new GuzzleClient([
            // Base URI is used with relative requests
            'base_uri' => 'https://api.coinbase.com/oauth/',
            // You can set any number of default request options.
            'timeout' => 20.0,
        ]);

        $response = $client->request('POST', 'token', [
            'json' => [
                "grant_type" => "authorization_code",
                "code" => $request->code,
                "client_id" => env('COINBASE_APP_ID'),
                "client_secret" => env('COINBASE_APP_SECRET'),
                "redirect_uri" => env('COINBASE_APP_REDIRECT'),
            ]]);

        $body = $response->getBody()->getContents();
        $code = $response->getStatusCode();
        Log::debug('send request');
        Log::debug('code' . $code);
        Log::debug($body);
        $bodyresponse = json_decode($body, true);
        Log::debug(print_r($bodyresponse, true));
        if (isset($bodyresponse['access_token'])) {
            $user->CoinBaseAccessToken = $bodyresponse['access_token'];
            $user->CoinBaseRefreshToken = $bodyresponse['refresh_token'];
            $user->save();
        }


        return redirect('/dashboard');

    }


    /**
     * here we make invoice payments
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function PayInvoicePaypal(Request $request)
    {


        $invoice = Invoices::find($request->invoiceId);
        if (empty($invoice)) {
            return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );

        if (env('PAYPAL_MODE') == 'live') {
            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                    'log.LogEnabled' => true,
                    'log.FileName' => '/var/www/smartcontracts/PayPal.log',
                    'log.LogLevel' => 'INFO'
                ));

        }


        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setTotal($invoice->TotalAmount);
        $amount->setCurrency('USD');

        $transaction = new Transaction();
        $transaction->setAmount($amount);

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(env('PAYPAL_APP_REDIRECT'))
            ->setCancelUrl(env('PAYPAL_APP_CANCEL'));

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);


        // After Step 3
        try {
            $payment->create($apiContext);
            //     echo $payment;
            //     echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
            $PaypalId = $payment->getId();
            $invoice->PaypalId = $PaypalId;
            $invoice->save();

            return response()->json(['error' => false, 'link' => $payment->getApprovalLink()]);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // This will print the detailed information on the exception.
            //REALLY HELPFUL FOR DEBUGGING
            //    echo $ex->getData();

            return response()->json(['error' => true, 'msg' => $ex->getData()]);
        }


    }


    public function acceptStripeTOS()
    {


        Stripe::setApiKey(env('STRIPE_API_KEY'));

        $t = \Stripe\Account::update(
            "acct_1EQyWUDJUG94dAd5",
            [
                'tos_acceptance' => [
                    'date' => time(),
                    'ip' => $_SERVER['REMOTE_ADDR'] // Assumes you're not using a proxy
                ],
            ]
        );


    }


    public function PayInvoiceStripe(Request $request, JWTAuth $JWTAuth)
    {

        $invoice = Invoices::find($request->invoiceId);
        if (empty($invoice)) {
            return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }

        Stripe::setApiKey(env('STRIPE_API_KEY'));


        $user = HelperController::GetCurrentUser($request, $JWTAuth);

        if (empty($user->stripeToken)) {
            return response()->json(['error' => true, 'msg' => 'You need add payment card first. Please go to account settings']);

        }
/*
           $acct = StripeAccount::create([
                    'country' => 'US',
                    'type' => 'custom',
                    "requested_capabilities" => ["card_payments"],
               "account_token"=>1
                ]);
*/

        //acct Id =acct_1EQyWUDJUG94dAd5

        /*         $acct = StripeAccount::create([
                     'country' => 'CH',
                     'type' => 'custom'
                 ]);
    /*   // id = acct_1EQygkKRA3Mz8FRV
                 */








        // acct_1ETLxeFr1LA3PYZg   this is my
        $charge = \Stripe\Charge::create([
            "amount" => $invoice->TotalAmount * 100.00,
            "currency" => "usd",
         //   "source" => "tok_visa",
            "transfer_data" => [
                "destination" => "acct_1EQyWUDJUG94dAd5",
            ],
            "customer" => $user->stripeToken, // obtained with Stripe.js
            "description" => "Charge for jenny.rosen@example.com"
        ]);


        $t = 1;

        $chargeId = $charge->id;
        $balance_transaction = $charge->balance_transaction;
        $outcome = json_encode($charge->outcome);
        $ispaid = $charge->paid;
        $paymentDetails = json_encode($charge->payment_method_details);
        $receipt = $charge->receipt_url;
        $transferStatus = $charge->status;


        StripePayments::create([
            'invoice_id' => $invoice->id,
            'chargeId' => $chargeId,
            'balance_transaction' => $balance_transaction,
            'outcome' => $outcome,
            'isPaid' => $ispaid,
            'paymentDetails' => $paymentDetails,
            'receipt' => $receipt,
            'status' => $transferStatus,
        ]);

        $invoice->StripeId = $chargeId;
        $invoice->save();


$customer = Customer::retrieve($user->stripeToken);
        $customerSource = $customer->default_source;


        $payout = \Stripe\Payout::create([
            'amount' => 100.00,
            'currency' => 'usd',
            'destination' => $customerSource
        ]);



        $this->stripePayouts($invoice->id);

        return response()->json(['error' => false, 'msg' => $transferStatus]);

    }


    public function PaypalPayouts($invoiceId)
    {

        $invoice = Invoices::find($invoiceId);

        Log::debug('PaypalPayouts ' . $invoiceId);
        if (empty($invoice)) {
            Log::error('Cant Found invoice');
            return false;
            //     return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }

        $sql = "SELECT users.paypalAccount
FROM invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.id=?";
        $res = DB::select($sql, [$invoiceId]);
        if (isset($res[0]->paypalAccount)) {

            $paypalAcount = $res[0]->paypalAccount;
            if (empty($paypalAcount)) {

                Log::error('Paypal account empty');
                return false;
            }


        } else {
            Log::error('Cant get paypal account');
            return false;

        }

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );


        if (env('PAYPAL_MODE') == 'live') {
            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                    'log.LogEnabled' => true,
                    'log.FileName' => '/var/www/smartcontracts/PayPal.log',
                    'log.LogLevel' => 'INFO'
                ));

        }

        $payouts = new Payout();
        $senderBatchHeader = new PayoutSenderBatchHeader();
        $senderBatchHeader->setSenderBatchId(uniqid())
            ->setEmailSubject("You have a payment");

        $senderItem1 = new PayoutItem();
        $paymentAmount = $invoice->TotalAmount - $invoice->gasPrice - $invoice->serviceFee;
        $senderItem1->setRecipientType('Email')
            ->setNote('Thanks you.')
            ->setReceiver($paypalAcount)
            ->setSenderItemId("invoice_" . $invoiceId)
            ->setAmount(new Currency('{
                        "value":"' . $paymentAmount . '",
                        "currency":"USD"
                    }'));

        $payouts->setSenderBatchHeader($senderBatchHeader)
            ->addItem($senderItem1);


        $request = clone $payouts;

        try {
            $output = $payouts->create(null, $apiContext);
            $t = $output->getLinks();

            $t2 = $output->toArray();
            $t3 = $t[0]->toArray();
        } catch (Exception $ex) {
            Log::error('Cant create payouts ' . $ex->getMessage());
            //  ResultPrinter::printError("Created Batch Payout", "Payout", null, $request, $ex);
            return false;
            //   exit(1);
        }

        // ResultPrinter::printResult("Created Batch Payout", "Payout", $output->getBatchHeader()->getPayoutBatchId(), $request, $output);
        $payoutId = $output->getBatchHeader()->getPayoutBatchId();
        Log::debug('Create payout ' . $payoutId);
        $invoice->payoutId = $payoutId;
        $invoice->payuot_paid_at = date('Y-m-d H:i:s');
        $invoice->save();
        return true;
        //     return $output;


        /*
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $amount = new Amount();
                $amount->setTotal($invoice->TotalAmount);
                $amount->setCurrency('USD');

                $transaction = new Transaction();
                $transaction->setAmount($amount);

                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(env('PAYPAL_APP_REDIRECT'))
                    ->setCancelUrl(env('PAYPAL_APP_CANCEL'));

                $payment = new Payment();
                $payment->setIntent('sale')
                    ->setPayer($payer)
                    ->setTransactions(array($transaction))
                    ->setRedirectUrls($redirectUrls);


                // After Step 3
                try {
                    $payment->create($apiContext);
                    //     echo $payment;
                    //     echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
                    $PaypalId = $payment->getId();
                    $invoice->PaypalId = $PaypalId;
                    $invoice->save();

                    return response()->json(['error' => false, 'link' => $payment->getApprovalLink()]);
                } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    // This will print the detailed information on the exception.
                    //REALLY HELPFUL FOR DEBUGGING
                    //    echo $ex->getData();

                    return response()->json(['error' => true, 'msg' => $ex->getData()]);
                }

        */
    }


    public function stripePayouts($invoiceId)
    {

        $invoice = Invoices::find($invoiceId);

        Log::debug('stripePayouts ' . $invoiceId);
        if (empty($invoice)) {
            Log::error('Cant Found invoice');
            return false;
            //     return response()->json(['error' => true, 'msg' => 'Cant Found invoice']);
        }

        $sql = "SELECT users.stripeToken
FROM invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.id=?";
        $res = DB::select($sql, [$invoiceId]);
        if (isset($res[0]->stripeToken)) {
            $stripeToken = $res[0]->stripeToken;
            if (empty($stripeToken)) {
                Log::error('stripe account empty');
                return false;
            }
        } else {
            Log::error('Cant get stripe account');
            return false;
        }


        Stripe::setApiKey(env('STRIPE_API_KEY'));


        $paymentAmount = $invoice->TotalAmount - $invoice->gasPrice - $invoice->serviceFee;

        $payout = \Stripe\Payout::create([
            'amount' => $paymentAmount,
            'currency' => 'usd',
            'destination' => $stripeToken
        ]);


        // ResultPrinter::printResult("Created Batch Payout", "Payout", $output->getBatchHeader()->getPayoutBatchId(), $request, $output);
        $payoutId = $payout->id;
        Log::debug('Create payout ' . $payoutId);
        $invoice->payoutId = $payoutId;
        $invoice->payuot_paid_at = date('Y-m-d H:i:s');
        $invoice->save();
        return true;
        //     return $output;


        /*
                $payer = new Payer();
                $payer->setPaymentMethod('paypal');

                $amount = new Amount();
                $amount->setTotal($invoice->TotalAmount);
                $amount->setCurrency('USD');

                $transaction = new Transaction();
                $transaction->setAmount($amount);

                $redirectUrls = new RedirectUrls();
                $redirectUrls->setReturnUrl(env('PAYPAL_APP_REDIRECT'))
                    ->setCancelUrl(env('PAYPAL_APP_CANCEL'));

                $payment = new Payment();
                $payment->setIntent('sale')
                    ->setPayer($payer)
                    ->setTransactions(array($transaction))
                    ->setRedirectUrls($redirectUrls);


                // After Step 3
                try {
                    $payment->create($apiContext);
                    //     echo $payment;
                    //     echo "\n\nRedirect user to approval_url: " . $payment->getApprovalLink() . "\n";
                    $PaypalId = $payment->getId();
                    $invoice->PaypalId = $PaypalId;
                    $invoice->save();

                    return response()->json(['error' => false, 'link' => $payment->getApprovalLink()]);
                } catch (\PayPal\Exception\PayPalConnectionException $ex) {
                    // This will print the detailed information on the exception.
                    //REALLY HELPFUL FOR DEBUGGING
                    //    echo $ex->getData();

                    return response()->json(['error' => true, 'msg' => $ex->getData()]);
                }

        */
    }


    public function PaypalCallback(Request $request)
    {

        Log::debug('Get paypal callback');
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                env('PAYPAL_CLIENTID'),     // ClientID
                env('PAYPAL_CLIENTSECRET')      // ClientSecret
            )
        );

        if (env('PAYPAL_MODE') == 'live') {
            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                    'log.LogEnabled' => true,
                    'log.FileName' => '/var/www/smartcontracts/PayPal.log',
                    'log.LogLevel' => 'INFO'
                ));
        }

        /*   $paymentId = 'PAY-7JF098896C7116747LQN4JCA';
           $token = 'EC-9G192826886694410';
           $PayerID = '4VAJLP89AYATS';
   */
        $paymentId = $request->paymentId;
        $PayerID = $request->PayerID;
        $payment = Payment::get($paymentId, $apiContext);
        $invoice = Invoices::where('PaypalId', $paymentId)->first();
        if (empty($invoice)) {
            return redirect('/paymentError?msg=Invoice not found');
        }
        $state = $payment->getState();
        Log::debug('Get paypal callback state ' . $state);
        if ($state == 'approved') {
            //Already payed
            $invoice->status = 'paid';
            $invoice->paid_at = date('Y-m-d H:i:s');
            $invoice->save();
            $this->PaypalPayouts($invoice->id);
            return redirect('/dashboard');
        } else {
            $execution = new PaymentExecution();
            $execution->setPayerId($PayerID);
            try {
                $result = $payment->execute($execution, $apiContext);
                $state = $result->getState();
                try {
                    $payment = Payment::get($paymentId, $apiContext);
                    $state = $payment->getState();
                    Log::debug('Get paypal callback state ' . $state);

                    if ($state == 'approved') {
                        $invoice->status = 'paid';
                        $invoice->save();
                        $this->PaypalPayouts($invoice->id);

                        return redirect('/dashboard');
                    }
                } catch (Exception $ex) {
                    return redirect('/paymentError?msg=' . $ex->getMessage());
                }
            } catch (Exception $ex) {
                return redirect('/paymentError?msg=' . $ex->getMessage());
            }
        }
    }


    public function makeAdminPayout(Request $request)
    {


        $invoice = Invoices::find($request->id);

        if (empty($invoice)) {
            return response()->json(['error' => true, 'msg' => 'Cant found Invoice ID']);
        }
        if (!empty($invoice->payoutId)) {
            return response()->json(['error' => true, 'msg' => 'Payout already done']);
        }


        $sql = "SELECT users.paypalAccount
FROM invoices
LEFT JOIN leases ON leases.id = invoices.leaseId
LEFT JOIN users ON users.id =leases.landlord_user_id
WHERE invoices.id=?";
        $res = DB::select($sql, [$invoice->id]);
        if (isset($res[0]->paypalAccount)) {
            $paypalAcount = $res[0]->paypalAccount;
            if (empty($paypalAcount)) {
                return response()->json(['error' => true, 'msg' => 'Paypal account not linked']);
            }

        } else {
            return response()->json(['error' => true, 'msg' => 'Cant get paypal account']);

        }


        $payout = $this->PaypalPayouts($invoice->id);
        if ($payout) {
            return response()->json(['error' => false, 'msg' => 'Payout  done']);
        } else {
            return response()->json(['error' => true, 'msg' => 'Somthing wrong. We cant make payout right now']);
        }

    }


}
