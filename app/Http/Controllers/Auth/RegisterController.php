<?php

namespace App\Http\Controllers\Auth;

use App\Mail\WelcomeMessage;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
 //       $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'role' => $data['role'],
            'password' => Hash::make($data['password']),
        ]);
    }


    public function registerAngular(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        $user = User::where('email', $request->email)->where('role',$request->role)->count();
        if ($user > 0) {
            return response()->json(['status' => 'fail', 'id' => 0, 'msg' => 'This email already taken', 'user' => ['id' => 0, 'role' => '']]);
        }
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg = '';
            foreach ($errors->all() as $message) {
                $msg .= $message . "\r\n";
            }
            return response()->json(['status' => 'fail', 'id' => 0, 'msg' => $msg, 'user' => ['id' => 0, 'role' => '']]);
        }
            $params['name'] = $request->name;
            $params['email'] = $request->email;
            $params['role'] = $request->role;
            $params['password'] = $request->password;
            $user = $this->create($params);
            Auth::login($user);
            $userCreated = true;


        Mail::to($request->email)->send(new WelcomeMessage($user));
        return response()->json(['userCreated' => $userCreated]);

    }



    public function registerMobile(Request $request, JWTAuth $JWTAuth){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        $user = User::where('email', $request->email)->count();
        if ($user > 0) {
            return response()->json(['status' => 'fail', 'id' => 0, 'msg' => 'This email already taken', 'user' => ['id' => 0, 'role' => '']]);
        }
        if ($validator->fails()) {
            $errors = $validator->errors();
            $msg = '';
            foreach ($errors->all() as $message) {
                $msg .= $message . "\r\n";
            }
            return response()->json(['status' => 'fail', 'id' => 0, 'msg' => $msg, 'user' => ['id' => 0, 'role' => '']]);
        }
        $params['name'] = $request->name;
        $params['email'] = $request->email;
        $params['role'] = $request->role;
        $params['password'] = $request->password;
        $user = $this->create($params);

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 'ok',
            'token' => $token,
            'user' => $user
        ], 201);
    }

}
