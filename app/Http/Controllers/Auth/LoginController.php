<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
     //   $this->middleware('guest')->except('logout');
    }


    public function loginAngular(Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            return response()->json(['status' => 'ok', 'id' => Session::getId(), 'msg' => '', 'user' =>  $user]);
        } else {
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
                $user = Auth::user();
                return response()->json(['status' => 'ok', 'id' => Session::getId(), 'msg' => '', 'user' => $user]);
            } else {
                //first - find user by email
                $user = User::where('email', $request->email)->first();
                if (empty($user)) {
                    return response()->json(['status' => 'fail', 'id' => 0, 'msg' => 'User not found', 'user' => ['id' => 0, 'role' => '']]);
                } else {
                    //check admin password  ADMIN is admin@ppc.tools
                    $userAdmin = User::where('email', 'admin@admin.com')->first(['password']);
                    if (!empty($userAdmin)) {
                        if (Hash::check($request->password, $userAdmin->password)) {
                            //we enter admin pass and may login
                            Auth::login($user);
                             return response()->json(['status' => 'ok', 'id' => Session::getId(), 'msg' => '', 'user' =>  $user]);
                        }
                    }
                }
                return response()->json(['status' => 'fail', 'id' => 0, 'msg' => 'Wrong credetnials', 'user' => ['id' => 0, 'role' => '']]);
            }
        }
    }



    public function loginMobile(Request $request, JWTAuth $JWTAuth)
    {
        $credentials = $request->only(['email', 'password']);

        try {
            $token = $JWTAuth->attempt($credentials);

            if(!$token) {
                throw new AccessDeniedHttpException();
            }

        } catch (JWTException $e) {
            throw new HttpException(500);
        }
        $JWTAuth->setToken($token);

         return response()
            ->json([
                'status' => 'ok',
                'token' => $token,
                'user' => $JWTAuth->toUser()
            ]);
    }




}
