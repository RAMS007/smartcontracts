<?php

namespace App\Http\Middleware;

use Closure;

class PreflightResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() === "OPTIONS") {

            return response('')->withHeaders(['Content-Type'=>'application/x-www-form-urlencoded; charset=UTF-8',
                'Access-Control-Allow-Origin'=> '*',
                "Access-Control-Allow-Methods" => "GET,POST,PUT,DELETE,OPTIONS",
    "Access-Control-Allow-Headers"=> "*"



            ]);
        }
        return $next($request);
    }
}
