<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdditionalOccupants extends Model
{
    protected $guarded=['id'];
}
