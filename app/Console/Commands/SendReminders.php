<?php

namespace App\Console\Commands;

use App\Invoices;
use App\Reminders;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Mail\RemindersToSign;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendReminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Start send reminders');
        $threeDayDate = date('Y-m-d', time() - 60 * 60 * 24 * 3);
        $now = Carbon::now();
        //find  peoples who still not signing
        //landlords
        $sql = "SELECT Landlord_email AS email, leases.id, Landlord_FirstName AS userName, landlord_user_id, users.name,users.FirstName, users.LastName
FROM leases
LEFT JOIN users ON leases.landlord_user_id =users.id
WHERE 
LL_sign = '' OR LL_sign IS NULL AND Landlord_email IS NOT NULL AND leases.created_at < '" . $threeDayDate . "'
 AND Landlord_email !='' AND leases.deleted_at IS NULL
";
        $res = DB::select($sql);
        Log::info('We have ' . count($res) . ' unsigned contracts by landlord');
        foreach ($res as $r) {
            $reminder = Reminders::where('email', $r->email)->first();
            if (empty($r->userName)) {
                $userName = $r->FirstName . ' ' . $r->LastName;
                if (empty($userName)) {
                    $userName = 'user';
                }
            } else {
                $userName = $r->userName;
            }
            if (empty($reminder)) {
                try {
                    Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    Log::error($e->getMessage());
                }

                Reminders::create([
                    'lease_id' => $r->id,
                    'email' => $r->email,
                    'reminderSended' => date('Y-m-d')
                ]);
            } else {
                $ReminderCarbon = Carbon::parse($reminder->reminderSended);
                $diffInDays = $ReminderCarbon->diffInDays($now);
                if ($diffInDays >= 3) {
                    try {
                        Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        Log::error($e->getMessage());
                    }
                    $reminder->reminderSended = date('Y-m-d');
                }
            }

        }
        //tenants
        $sql = "SELECT lease_id AS id, FirstName, LastName, email
FROM additional_occupants
LEFT JOIN leases ON leases.id =additional_occupants.lease_id
WHERE 
additional_occupants.sign = '' OR additional_occupants.sign IS NULL AND leases.created_at < '" . $threeDayDate . "'
AND leases.deleted_at IS NULL AND email !=''
";

        $res = DB::select($sql);
        Log::info('We have ' . count($res) . ' unsigned contracts by tenants');
        foreach ($res as $r) {
            $reminder = Reminders::where('email', $r->email)->first();
            $userName = $r->FirstName . ' ' . $r->LastName;
            if (empty($userName)) {
                $userName = 'user';
            }
            if (empty($reminder)) {
                try {
                    Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    Log::error($e->getMessage());
                }
                Reminders::create([
                    'lease_id' => $r->id,
                    'email' => $r->email,
                    'reminderSended' => date('Y-m-d')
                ]);
            } else {
                $ReminderCarbon = Carbon::parse($reminder->reminderSended);
                $diffInDays = $ReminderCarbon->diffInDays($now);
                if ($diffInDays >= 3) {
                    try {
                        Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        Log::error($e->getMessage());
                    }
                    $reminder->reminderSended = date('Y-m-d');
                }
            }
        }

        //PMs
        $sql = "SELECT PropertyManager_email AS email, leases.id, PropertyManager_FirstName AS FirstName   , PropertyManager_LastName AS LastName
FROM leases
WHERE 
PM_sign = '' OR PM_sign IS NULL AND PropertyManager_email IS NOT NULL AND leases.created_at < '" . $threeDayDate . "'
AND leases.deleted_at IS NULL AND PropertyManager_email !=''
";
        $res = DB::select($sql);
        Log::info('We have ' . count($res) . ' unsigned contracts by PM');
        foreach ($res as $r) {
            $reminder = Reminders::where('email', $r->email)->first();

            $userName = $r->FirstName . ' ' . $r->LastName;
            if (empty($userName)) {
                $userName = 'user';
            }
            if (empty($reminder)) {
                try {
                    Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    Log::error($e->getMessage());
                }
                Reminders::create([
                    'lease_id' => $r->id,
                    'email' => $r->email,
                    'reminderSended' => date('Y-m-d')
                ]);
            } else {
                $ReminderCarbon = Carbon::parse($reminder->reminderSended);
                $diffInDays = $ReminderCarbon->diffInDays($now);
                if ($diffInDays >= 3) {
                    try {
                        Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        Log::error($e->getMessage());
                    }
                    $reminder->reminderSended = date('Y-m-d');
                }
            }
        }


        //brokers

        $sql = "SELECT RealEstate_email AS email, leases.id, RealEstate_FirstName AS FirstName   , RealEstate_LastName AS LastName
FROM leases
WHERE 
B_sign = '' OR B_sign IS NULL AND RealEstate_email IS NOT NULL AND leases.created_at < '" . $threeDayDate . "'
AND leases.deleted_at IS NULL AND RealEstate_email !=''
";
        $res = DB::select($sql);
        Log::info('We have ' . count($res) . ' unsigned contracts by brokers');
        foreach ($res as $r) {
            $reminder = Reminders::where('email', $r->email)->first();

            $userName = $r->FirstName . ' ' . $r->LastName;
            if (empty($userName)) {
                $userName = 'user';
            }
            if (empty($reminder)) {
                try {
                    Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    Log::error($e->getMessage());
                }
                Reminders::create([
                    'lease_id' => $r->id,
                    'email' => $r->email,
                    'reminderSended' => date('Y-m-d')
                ]);
            } else {
                $ReminderCarbon = Carbon::parse($reminder->reminderSended);
                $diffInDays = $ReminderCarbon->diffInDays($now);
                if ($diffInDays >= 3) {
                    try {
                        Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        Log::error($e->getMessage());
                    }
                    $reminder->reminderSended = date('Y-m-d');
                }
            }
        }


        $sql = "SELECT RealEstate_Additional_email AS email, leases.id, RealEstate_Additional_FirstName AS FirstName   , RealEstate_Additional_LastName AS LastName
FROM leases
WHERE 
B_A_sign = '' OR B_A_sign IS NULL AND RealEstate_Additional_email IS NOT NULL AND leases.created_at < '" . $threeDayDate . "'
AND leases.deleted_at IS NULL  AND RealEstate_Additional_email !=''
";
        $res = DB::select($sql);
        Log::info('We have ' . count($res) . ' unsigned contracts by additional brokers');
        foreach ($res as $r) {
            $reminder = Reminders::where('email', $r->email)->first();

            $userName = $r->FirstName . ' ' . $r->LastName;
            if (empty($userName)) {
                $userName = 'user';
            }
            if (empty($reminder)) {
                try {
                    Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                } catch (\Exception $e) {
                    echo $e->getMessage();
                    Log::error($e->getMessage());
                }
                Reminders::create([
                    'lease_id' => $r->id,
                    'email' => $r->email,
                    'reminderSended' => date('Y-m-d')
                ]);
            } else {
                $ReminderCarbon = Carbon::parse($reminder->reminderSended);
                $diffInDays = $ReminderCarbon->diffInDays($now);
                if ($diffInDays >= 3) {
                    try {
                        Mail::to($r->email)->send(new RemindersToSign($r->id, $userName));
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                        Log::error($e->getMessage());
                    }
                    $reminder->reminderSended = date('Y-m-d');
                }
            }
        }

        //now send reminders  for non paid invoices
$AllInvoices = Invoices::where('status','new')->where('user_who_pay','<>','')->get();





        Log::info('finish send reminders');
    }
}
