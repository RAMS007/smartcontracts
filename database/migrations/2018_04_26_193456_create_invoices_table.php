<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_generated');
            $table->string('user_who_pay');
            $table->unsignedInteger('leaseId');
            $table->text('note');
            $table->enum('status',['new', 'paid', 'notPaid'])->default('new');
            $table->date('paidUntil');
            $table->decimal('lateFees',10,2 )->default(0);
            $table->decimal('TotalAmount',10,2 )->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
