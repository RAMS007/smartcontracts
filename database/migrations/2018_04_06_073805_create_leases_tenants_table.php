<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasesTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases_tenants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('tenant_user_id');
            $table->unsignedInteger('ParentLease_id');

            $table->date('dated')->nullable();
            $table->date('moveInDate')->nullable();
            $table->date('MoveOutDate')->nullable();

            $table->boolean('AdditionalOccupants')->nullable();
            $table->unsignedTinyInteger('AdditionalOccupants_number')->nullable();
            $table->string('AdditionalOccupants_FirstName')->nullable();
            $table->string('AdditionalOccupants_LastName')->nullable();
            $table->string('AdditionalOccupants_email')->nullable();
            $table->string('AdditionalOccupants_AreaCode')->nullable();
            $table->string('AdditionalOccupants_PhoneNumber')->nullable();
            $table->string('Emergency_FirstName')->nullable();
            $table->string('Emergency_LastName')->nullable();
            $table->string('Emergency_relationship')->nullable();
            $table->string('Emergency_email')->nullable();
            $table->string('Emergency_AreaCode')->nullable();
            $table->string('Emergency_PhoneNumber')->nullable();
            $table->boolean('PropertyManager')->nullable();
            $table->string('PropertyManager_FirstName')->nullable();
            $table->string('PropertyManager_LastName')->nullable();
            $table->string('PropertyManager_email')->nullable();
            $table->string('PropertyManager_address')->nullable();
            $table->string('PropertyManager_AreaCode')->nullable();
            $table->string('PropertyManager_PhoneNumber')->nullable();
            $table->boolean('RealEstate')->nullable();
            $table->enum('RealEstate_Jurisdiction', ["State", "County", "Canton", "Country", "City", "Municipality", "Province"])->nullable();
            $table->string('RealEstate_FirstName')->nullable();
            $table->string('RealEstate_LastName')->nullable();
            $table->string('RealEstate_email')->nullable();
            $table->string('RealEstate_AreaCode')->nullable();
            $table->string('RealEstate_PhoneNumber')->nullable();
            $table->string('RealEstate_address')->nullable();
            $table->boolean('RealEstate_LLrelationship')->nullable();
            $table->boolean('RealEstate_Trelationship')->nullable();
            $table->boolean('RealEstate_Additional')->nullable();
            $table->enum('RealEstate_Additional_Jurisdiction', ["State", "County", "Canton", "Country", "City", "Municipality", "Province"])->nullable();
            $table->string('RealEstate_Additional_FirstName')->nullable();
            $table->string('RealEstate_Additional_LastName')->nullable();
            $table->string('RealEstate_Additional_email')->nullable();
            $table->string('RealEstate_Additional_AreaCode')->nullable();
            $table->string('RealEstate_Additional_PhoneNumber')->nullable();
            $table->string('RealEstate_Additional_address')->nullable();
            $table->string('Lease_state')->nullable();
            $table->decimal('Lease_monthAmount', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_payday')->nullable();
            $table->string('Lease_payBy')->nullable();
            $table->unsignedTinyInteger('Lease_startDay')->nullable();
            $table->unsignedTinyInteger('Lease_payday2')->nullable();
            $table->unsignedTinyInteger('Lease_payday3')->nullable();
            $table->decimal('Lease_payFees', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_payday4')->nullable();
            $table->decimal('Lease_insufficientFunds', 10, 2)->nullable();
            $table->decimal('Lease_SecurityDeposit', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_Posession_day')->nullable();
            $table->unsignedTinyInteger('Lease_OccupantsAllowed')->nullable();
            $table->unsignedSmallInteger('Lease_VisitorsPeriodAllowed')->nullable();
            $table->enum('Lease_Service_Electricity', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Telephone', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_TV', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Heat', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_HotWater', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Water', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Garbage', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_SnowRemoval', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Landscaping', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Included_Stove', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Refrigerator', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Dishwasher', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_WachingMachine', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Dryer', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_GarbageDisposal', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Microwawe', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Toaster', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_AirConditioner', ['Yes', 'No'])->nullable();
            $table->boolean('Lease_Pets_allowed')->nullable();
            $table->unsignedTinyInteger('Lease_Pets_maxAllowed')->nullable();
            $table->string('Lease_Pets_Allowedtypes')->nullable();
            $table->decimal('Lease_Pets_SecurityDeposit', 10, 2)->nullable();
            $table->boolean('Lease_Pets_deposit_type')->nullable();
            $table->unsignedTinyInteger('Lease_Abandons_days')->nullable();
            $table->string('Lease_Notice_address')->nullable();
            $table->string('Lease_Notice_city')->nullable();
            $table->string('Lease_Notice_state')->nullable();
            $table->string('Lease_Notice_Zip')->nullable();
            $table->boolean('Lease_Parking_allowed')->nullable();
            $table->string('Lease_Parking_space')->nullable();
            $table->boolean('Lease_Parking_fees')->nullable();
            $table->decimal('Lease_Parking_fees_amount', 10, 2)->nullable();
            $table->enum('Lease_Parking_fees_period', ['month', 'oneTime'])->nullable();
            $table->text('Lease_AdditionalTerms')->nullable();

            $table->date('LLSigned_date')->nullable();
            $table->text('LL_sign')->nullable();
            $table->date('TSigned_date')->nullable();
            $table->text('T_sign')->nullable();
            $table->date('BSigned_date')->nullable();
            $table->text('B_sign')->nullable();




            $table->foreign('tenant_user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('ParentLease_id')->references('id')->on('leases')->onDelete('cascade');;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases_tenants');
    }
}
