<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalPropertyManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_property_managers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('lease_id');
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('email')->nullable();
            $table->string('AreaCode')->nullable();
            $table->string('PhoneNumber')->nullable();
            $table->string('Address')->nullable();
            $table->text('sign')->nullable();
            $table->date('SignedDate')->nullable();
            $table->boolean('isLead')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_property_managers');
    }
}
