<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('FirstName')->nullable();
            $table->string('LastName')->nullable();
            $table->string('AreaCode')->nullable();
            $table->string('PhoneNumber')->nullable();
            $table->text('HomeAdress')->nullable();

            $table->enum('role',['tenant','landlord','admin', 'broker', 'property_manager'])->default('landlord');
            $table->decimal('ballance',15,2)->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('CustomLogo')->nullable();
            $table->string('CoinBaseAccessToken')->nullable();
            $table->string('CoinBaseRefreshToken')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
