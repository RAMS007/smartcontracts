<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('landlord_user_id');
            $table->string('tenant_email')->nullable();
            $table->unsignedInteger('property_id');
            //          $table->unsignedInteger('tenant2brokerrelations_id')->nullable();
            //          $table->unsignedInteger('landlord2brokerrelations_id');
            $table->enum('status', ['new', 'approved', 'HasChangesByTenant', 'HasChangesByLandlord', 'rejected'])->default('new');

            $table->date('dated')->nullable();
            $table->date('moveInDate')->nullable();
            $table->date('MoveOutDate')->nullable();

            $table->boolean('AdditionalOccupants')->nullable();
            $table->unsignedTinyInteger('AdditionalOccupants_number')->nullable();
            $table->string('AdditionalOccupants_FirstName')->nullable();
            $table->string('AdditionalOccupants_LastName')->nullable();
            $table->string('AdditionalOccupants_email')->nullable();
            $table->string('AdditionalOccupants_AreaCode')->nullable();
            $table->string('AdditionalOccupants_PhoneNumber')->nullable();
            $table->string('Emergency_FirstName')->nullable();
            $table->string('Emergency_LastName')->nullable();
            $table->string('Emergency_relationship')->nullable();
            $table->string('Emergency_email')->nullable();
            $table->string('Emergency_AreaCode')->nullable();
            $table->string('Emergency_PhoneNumber')->nullable();
            $table->boolean('PropertyManager')->nullable();
            $table->string('PropertyManager_FirstName')->nullable();
            $table->string('PropertyManager_LastName')->nullable();
            $table->string('PropertyManager_email')->nullable();
            $table->string('PropertyManager_address')->nullable();
            $table->string('PropertyManager_AreaCode')->nullable();
            $table->string('PropertyManager_PhoneNumber')->nullable();
            $table->boolean('RealEstate')->nullable();
            $table->enum('RealEstate_Jurisdiction', ["State", "County", "Canton", "Country", "City", "Municipality", "Province"])->nullable();
            $table->string('RealEstate_FirstName')->nullable();
            $table->string('RealEstate_LastName')->nullable();
            $table->string('RealEstate_email')->nullable();
            $table->string('RealEstate_AreaCode')->nullable();
            $table->string('RealEstate_PhoneNumber')->nullable();
            $table->string('RealEstate_address')->nullable();
            $table->boolean('RealEstate_LLrelationship')->nullable();
            $table->boolean('RealEstate_Trelationship')->nullable();
            $table->boolean('RealEstate_Additional')->nullable();
            $table->enum('RealEstate_Additional_Jurisdiction', ["State", "County", "Canton", "Country", "City", "Municipality", "Province"])->nullable();
            $table->string('RealEstate_Additional_FirstName')->nullable();
            $table->string('RealEstate_Additional_LastName')->nullable();
            $table->string('RealEstate_Additional_email')->nullable();
            $table->string('RealEstate_Additional_AreaCode')->nullable();
            $table->string('RealEstate_Additional_PhoneNumber')->nullable();
            $table->string('RealEstate_Additional_address')->nullable();
            $table->string('Lease_state')->nullable();
            $table->decimal('Lease_monthAmount', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_payday')->nullable();
            $table->string('Lease_payBy')->nullable();
            $table->unsignedTinyInteger('Lease_startDay')->nullable();
            $table->unsignedTinyInteger('Lease_payday2')->nullable();
            $table->unsignedTinyInteger('Lease_payday3')->nullable();
            $table->decimal('Lease_payFees', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_payday4')->nullable();
            $table->decimal('Lease_insufficientFunds', 10, 2)->nullable();
            $table->decimal('Lease_SecurityDeposit', 10, 2)->nullable();
            $table->unsignedTinyInteger('Lease_Posession_day')->nullable();
            $table->unsignedTinyInteger('Lease_OccupantsAllowed')->nullable();
            $table->unsignedSmallInteger('Lease_VisitorsPeriodAllowed')->nullable();
            $table->enum('Lease_Service_Electricity', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Telephone', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_TV', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Heat', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_HotWater', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Water', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Garbage', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_SnowRemoval', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Service_Landscaping', ['Landlord', 'Tenant'])->nullable();
            $table->enum('Lease_Included_Stove', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Refrigerator', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Dishwasher', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_WachingMachine', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Dryer', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_GarbageDisposal', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Microwawe', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_Toaster', ['Yes', 'No'])->nullable();
            $table->enum('Lease_Included_AirConditioner', ['Yes', 'No'])->nullable();
            $table->boolean('Lease_Pets_allowed')->nullable();
            $table->unsignedTinyInteger('Lease_Pets_maxAllowed')->nullable();
            $table->string('Lease_Pets_Allowedtypes')->nullable();
            $table->decimal('Lease_Pets_SecurityDeposit', 10, 2)->nullable();
            $table->string('Lease_Pets_deposit_type')->nullable();
            $table->unsignedTinyInteger('Lease_Abandons_days')->nullable();
            $table->string('Lease_Notice_address')->nullable();
            $table->string('Lease_Notice_city')->nullable();
            $table->string('Lease_Notice_state')->nullable();
            $table->string('Lease_Notice_Zip')->nullable();
            $table->boolean('Lease_Parking_allowed')->nullable();
            $table->string('Lease_Parking_space')->nullable();
            $table->boolean('Lease_Parking_fees')->nullable();
            $table->decimal('Lease_Parking_fees_amount', 10, 2)->nullable();
            $table->enum('Lease_Parking_fees_period', ['month', 'oneTime'])->nullable();
            $table->text('Lease_AdditionalTerms')->nullable();

            $table->date('LLSigned_date')->nullable();
            $table->text('LL_sign')->nullable();
            $table->date('TSigned_date')->nullable();
            $table->text('T_sign')->nullable();
            $table->date('BSigned_date')->nullable();
            $table->text('B_sign')->nullable();
            $table->date('B_ASigned_date')->nullable();
            $table->text('B_A_sign')->nullable();
            $table->date('PMSigned_date')->nullable();
            $table->text('PM_sign')->nullable();


///////////////////////////custom////////////////////////////////////////////////
            $table->boolean('GoverningLawEnabled')->nullable();
            $table->boolean('GoverningLawEdit')->nullable();
            $table->text('GoverningLawCustom')->nullable();
            $table->boolean('LeasePaymentsEnabled')->nullable();
            $table->boolean('LeasePaymentsEdit')->nullable();
            $table->text('LeasePaymentsCustom')->nullable();
            $table->boolean('LateChargesEnabled')->nullable();
            $table->boolean('LateChargesEdit')->nullable();
            $table->text('LateChargesCustom')->nullable();
            $table->boolean('InsufficientFundsEnabled')->nullable();
            $table->boolean('InsufficientFundsEdit')->nullable();
            $table->text('InsufficientFundsCustom')->nullable();
            $table->boolean('SecurityDepositEnabled')->nullable();
            $table->boolean('SecurityDepositEdit')->nullable();
            $table->text('SecurityDepositCustom')->nullable();
            $table->boolean('DefaultsEnabled')->nullable();
            $table->boolean('DefaultsEdit')->nullable();
            $table->text('DefaultsCustom')->nullable();
            $table->boolean('QuietEnjoymentEnabled')->nullable();
            $table->boolean('QuietEnjoymentEdit')->nullable();
            $table->text('QuietEnjoymentCustom')->nullable();
            $table->boolean('PosessionEnabled')->nullable();
            $table->boolean('PosessionEdit')->nullable();
            $table->text('PosessionCustom')->nullable();
            $table->boolean('PremisessEnabled')->nullable();
            $table->boolean('PremisessEdit')->nullable();
            $table->text('PremisessCustom')->nullable();
            $table->boolean('OccupantsEnabled')->nullable();
            $table->boolean('OccupantsEdit')->nullable();
            $table->text('OccupantsCustom')->nullable();
            $table->boolean('VisitorsEnabled')->nullable();
            $table->boolean('VisitorsEdit')->nullable();
            $table->text('VisitorsCustom')->nullable();
            $table->boolean('ConditionOfPremissesEnabled')->nullable();
            $table->boolean('ConditionOfPremissesEdit')->nullable();
            $table->text('ConditionOfPremissesCustom')->nullable();
            $table->boolean('AssigmentEnabled')->nullable();
            $table->boolean('AssigmentEdit')->nullable();
            $table->text('AssigmentCustom')->nullable();
            $table->boolean('DangerousMaterialsEnabled')->nullable();
            $table->boolean('DangerousMaterialsEdit')->nullable();
            $table->text('DangerousMaterialsCustom')->nullable();
            $table->boolean('UtilitesEnabled')->nullable();
            $table->boolean('UtilitesEdit')->nullable();
            $table->text('UtilitesCustom')->nullable();
            $table->boolean('ApplianceIncludedEnabled')->nullable();
            $table->boolean('ApplianceIncludedEdit')->nullable();
            $table->text('ApplianceIncludedCustom')->nullable();
            $table->boolean('PetsEnabled')->nullable();
            $table->boolean('PetsEdit')->nullable();
            $table->text('PetsCustom')->nullable();
            $table->boolean('AlterationsEnabled')->nullable();
            $table->boolean('AlterationsEdit')->nullable();
            $table->text('AlterationsCustom')->nullable();
            $table->boolean('DamagesEnabled')->nullable();
            $table->boolean('DamagesEdit')->nullable();
            $table->text('DamagesCustom')->nullable();
            $table->boolean('MaintanceAndRepairEnabled')->nullable();
            $table->boolean('MaintanceAndRepairEdit')->nullable();
            $table->text('MaintanceAndRepairCustom')->nullable();
            $table->boolean('RightOfInspectionEnabled')->nullable();
            $table->boolean('RightOfInspectionEdit')->nullable();
            $table->text('RightOfInspectionCustom')->nullable();
            $table->boolean('AbandonmentEnabled')->nullable();
            $table->boolean('AbandonmentEdit')->nullable();
            $table->text('AbandonmentCustom')->nullable();
            $table->boolean('ExtendedAbsensesEnabled')->nullable();
            $table->boolean('ExtendedAbsensesEdit')->nullable();
            $table->text('ExtendedAbsensesCustom')->nullable();
            $table->boolean('SecurityEnabled')->nullable();
            $table->boolean('SecurityEdit')->nullable();
            $table->text('SecurityCustom')->nullable();
            $table->boolean('SeverabilityEnabled')->nullable();
            $table->boolean('SeverabilityEdit')->nullable();
            $table->text('SeverabilityCustom')->nullable();
            $table->boolean('InsuranceEnabled')->nullable();
            $table->boolean('InsuranceEdit')->nullable();
            $table->text('InsuranceCustom')->nullable();
            $table->boolean('BindingEffectEnabled')->nullable();
            $table->boolean('BindingEffectEdit')->nullable();
            $table->text('BindingEffectCustom')->nullable();
            $table->boolean('EntireAgrementEnabled')->nullable();
            $table->boolean('EntireAgrementEdit')->nullable();
            $table->text('EntireAgrementCustom')->nullable();
            $table->boolean('NoticeEnabled')->nullable();
            $table->boolean('NoticeEdit')->nullable();
            $table->text('NoticeCustom')->nullable();
            $table->boolean('CumulativeRightsEnabled')->nullable();
            $table->boolean('CumulativeRightsEdit')->nullable();
            $table->text('CumulativeRightsCustom')->nullable();
            $table->boolean('WaiverEnabled')->nullable();
            $table->boolean('WaiverEdit')->nullable();
            $table->text('WaiverCustom')->nullable();
            $table->boolean('IndemnificationEnabled')->nullable();
            $table->boolean('IndemnificationEdit')->nullable();
            $table->text('IndemnificationCustom')->nullable();
            $table->boolean('AttorneyFeesEnabled')->nullable();
            $table->boolean('AttorneyFeesEdit')->nullable();
            $table->text('AttorneyFeesCustom')->nullable();
            $table->boolean('DisplaySignsEnabled')->nullable();
            $table->boolean('DisplaySignsEdit')->nullable();
            $table->text('DisplaySignsCustom')->nullable();
            $table->boolean('NoiceEnabled')->nullable();
            $table->boolean('NoiceEdit')->nullable();
            $table->text('NoiceCustom')->nullable();
            $table->boolean('ParkingEnabled')->nullable();
            $table->boolean('ParkingEdit')->nullable();
            $table->text('ParkingCustom')->nullable();
            $table->boolean('BalconesEnabled')->nullable();
            $table->boolean('BalconesEdit')->nullable();
            $table->text('BalconesCustom')->nullable();
            $table->boolean('DwellingEnabled')->nullable();
            $table->boolean('DwellingEdit')->nullable();
            $table->text('DwellingCustom')->nullable();
            $table->boolean('saveASTemplate')->nullable();


            ////////////////////end custom///////////
            $table->foreign('landlord_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
//            $table->foreign('tenant2brokerrelations_id')->references('id')->on('tenant2_broker_relations')->onDelete('cascade');
//            $table->foreign('landlord2brokerrelations_id')->references('id')->on('land_lord2_broker_relations')->onDelete('cascade');
            $table->string('customLease_file')->nullable();
            $table->boolean('IsCustom')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases');
    }
}
