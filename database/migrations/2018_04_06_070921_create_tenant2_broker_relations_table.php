<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenant2BrokerRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return false;
        Schema::create('tenant2_broker_relations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->boolean('noBusinessRelationship')->default(0);
            $table->string('broker')->nullable();
            $table->string('company_adress')->nullable();
            $table->string('company_phone')->nullable();
            $table->string('company_fax')->nullable();
            $table->string('license')->nullable();
            $table->string('derect_phone')->nullable();
            $table->string('cell_phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();

            $table->boolean('BrokerIsTenantAgent')->default(0);
            $table->boolean('BrokerIsDualAgent')->default(0);
            $table->boolean('LicenseIsTenantAgentWithDesignated')->default(0);
            $table->boolean('LicenseIsTenantAgentWithoutDesignated')->default(0);
            $table->boolean('LicenseIsDual')->default(0);
            $table->boolean('TransactionLicense')->default(0);

            $table->timestamps();
            $table->index('user_id');
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant2_broker_relations');
    }
}
